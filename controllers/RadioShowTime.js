'use strict';

var url = require('url');


var RadioShowTime = require('./RadioShowTimeService');


module.exports.radioShowTime.count = function radioShowTime.count (req, res, next) {
  RadioShowTime.radioShowTime.count(req.swagger.params, res, next);
};

module.exports.radioShowTime.create = function radioShowTime.create (req, res, next) {
  RadioShowTime.radioShowTime.create(req.swagger.params, res, next);
};

module.exports.radioShowTime.createChangeStream__get_RadioShowTimes_changeStream = function radioShowTime.createChangeStream__get_RadioShowTimes_changeStream (req, res, next) {
  RadioShowTime.radioShowTime.createChangeStream__get_RadioShowTimes_changeStream(req.swagger.params, res, next);
};

module.exports.radioShowTime.createChangeStream__post_RadioShowTimes_changeStream = function radioShowTime.createChangeStream__post_RadioShowTimes_changeStream (req, res, next) {
  RadioShowTime.radioShowTime.createChangeStream__post_RadioShowTimes_changeStream(req.swagger.params, res, next);
};

module.exports.radioShowTime.deleteById = function radioShowTime.deleteById (req, res, next) {
  RadioShowTime.radioShowTime.deleteById(req.swagger.params, res, next);
};

module.exports.radioShowTime.exists__get_RadioShowTimes_{id}_exists = function radioShowTime.exists__get_RadioShowTimes_{id}_exists (req, res, next) {
  RadioShowTime.radioShowTime.exists__get_RadioShowTimes_{id}_exists(req.swagger.params, res, next);
};

module.exports.radioShowTime.exists__head_RadioShowTimes_{id} = function radioShowTime.exists__head_RadioShowTimes_{id} (req, res, next) {
  RadioShowTime.radioShowTime.exists__head_RadioShowTimes_{id}(req.swagger.params, res, next);
};

module.exports.radioShowTime.find = function radioShowTime.find (req, res, next) {
  RadioShowTime.radioShowTime.find(req.swagger.params, res, next);
};

module.exports.radioShowTime.findById = function radioShowTime.findById (req, res, next) {
  RadioShowTime.radioShowTime.findById(req.swagger.params, res, next);
};

module.exports.radioShowTime.findOne = function radioShowTime.findOne (req, res, next) {
  RadioShowTime.radioShowTime.findOne(req.swagger.params, res, next);
};

module.exports.radioShowTime.prototype.__get__radioShow = function radioShowTime.prototype.__get__radioShow (req, res, next) {
  RadioShowTime.radioShowTime.prototype.__get__radioShow(req.swagger.params, res, next);
};

module.exports.radioShowTime.prototype.updateAttributes__patch_RadioShowTimes_{id} = function radioShowTime.prototype.updateAttributes__patch_RadioShowTimes_{id} (req, res, next) {
  RadioShowTime.radioShowTime.prototype.updateAttributes__patch_RadioShowTimes_{id}(req.swagger.params, res, next);
};

module.exports.radioShowTime.prototype.updateAttributes__put_RadioShowTimes_{id} = function radioShowTime.prototype.updateAttributes__put_RadioShowTimes_{id} (req, res, next) {
  RadioShowTime.radioShowTime.prototype.updateAttributes__put_RadioShowTimes_{id}(req.swagger.params, res, next);
};

module.exports.radioShowTime.replaceById = function radioShowTime.replaceById (req, res, next) {
  RadioShowTime.radioShowTime.replaceById(req.swagger.params, res, next);
};

module.exports.radioShowTime.replaceOrCreate = function radioShowTime.replaceOrCreate (req, res, next) {
  RadioShowTime.radioShowTime.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.radioShowTime.updateAll = function radioShowTime.updateAll (req, res, next) {
  RadioShowTime.radioShowTime.updateAll(req.swagger.params, res, next);
};

module.exports.radioShowTime.upsertWithWhere = function radioShowTime.upsertWithWhere (req, res, next) {
  RadioShowTime.radioShowTime.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.radioShowTime.upsert__patch_RadioShowTimes = function radioShowTime.upsert__patch_RadioShowTimes (req, res, next) {
  RadioShowTime.radioShowTime.upsert__patch_RadioShowTimes(req.swagger.params, res, next);
};

module.exports.radioShowTime.upsert__put_RadioShowTimes = function radioShowTime.upsert__put_RadioShowTimes (req, res, next) {
  RadioShowTime.radioShowTime.upsert__put_RadioShowTimes(req.swagger.params, res, next);
};
