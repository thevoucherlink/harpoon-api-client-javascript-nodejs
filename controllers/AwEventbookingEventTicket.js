'use strict';

var url = require('url');


var AwEventbookingEventTicket = require('./AwEventbookingEventTicketService');


module.exports.awEventbookingEventTicket.count = function awEventbookingEventTicket.count (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.count(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.create = function awEventbookingEventTicket.create (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.create(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.createChangeStream__get_AwEventbookingEventTickets_changeStream = function awEventbookingEventTicket.createChangeStream__get_AwEventbookingEventTickets_changeStream (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.createChangeStream__get_AwEventbookingEventTickets_changeStream(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.createChangeStream__post_AwEventbookingEventTickets_changeStream = function awEventbookingEventTicket.createChangeStream__post_AwEventbookingEventTickets_changeStream (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.createChangeStream__post_AwEventbookingEventTickets_changeStream(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.deleteById = function awEventbookingEventTicket.deleteById (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.deleteById(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.exists__get_AwEventbookingEventTickets_{id}_exists = function awEventbookingEventTicket.exists__get_AwEventbookingEventTickets_{id}_exists (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.exists__get_AwEventbookingEventTickets_{id}_exists(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.exists__head_AwEventbookingEventTickets_{id} = function awEventbookingEventTicket.exists__head_AwEventbookingEventTickets_{id} (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.exists__head_AwEventbookingEventTickets_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.find = function awEventbookingEventTicket.find (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.find(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.findById = function awEventbookingEventTicket.findById (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.findById(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.findOne = function awEventbookingEventTicket.findOne (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.findOne(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__count__attributes = function awEventbookingEventTicket.prototype.__count__attributes (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__count__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__count__awEventbookingTicket = function awEventbookingEventTicket.prototype.__count__awEventbookingTicket (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__count__awEventbookingTicket(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__count__tickets = function awEventbookingEventTicket.prototype.__count__tickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__count__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__create__attributes = function awEventbookingEventTicket.prototype.__create__attributes (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__create__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__create__awEventbookingTicket = function awEventbookingEventTicket.prototype.__create__awEventbookingTicket (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__create__awEventbookingTicket(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__create__tickets = function awEventbookingEventTicket.prototype.__create__tickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__create__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__delete__attributes = function awEventbookingEventTicket.prototype.__delete__attributes (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__delete__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__delete__awEventbookingTicket = function awEventbookingEventTicket.prototype.__delete__awEventbookingTicket (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__delete__awEventbookingTicket(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__delete__tickets = function awEventbookingEventTicket.prototype.__delete__tickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__delete__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__destroyById__attributes = function awEventbookingEventTicket.prototype.__destroyById__attributes (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__destroyById__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__destroyById__awEventbookingTicket = function awEventbookingEventTicket.prototype.__destroyById__awEventbookingTicket (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__destroyById__awEventbookingTicket(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__destroyById__tickets = function awEventbookingEventTicket.prototype.__destroyById__tickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__destroyById__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__findById__attributes = function awEventbookingEventTicket.prototype.__findById__attributes (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__findById__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__findById__awEventbookingTicket = function awEventbookingEventTicket.prototype.__findById__awEventbookingTicket (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__findById__awEventbookingTicket(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__findById__tickets = function awEventbookingEventTicket.prototype.__findById__tickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__findById__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__get__attributes = function awEventbookingEventTicket.prototype.__get__attributes (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__get__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__get__awEventbookingTicket = function awEventbookingEventTicket.prototype.__get__awEventbookingTicket (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__get__awEventbookingTicket(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__get__tickets = function awEventbookingEventTicket.prototype.__get__tickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__get__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__updateById__attributes = function awEventbookingEventTicket.prototype.__updateById__attributes (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__updateById__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__updateById__awEventbookingTicket = function awEventbookingEventTicket.prototype.__updateById__awEventbookingTicket (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__updateById__awEventbookingTicket(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.__updateById__tickets = function awEventbookingEventTicket.prototype.__updateById__tickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.__updateById__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.updateAttributes__patch_AwEventbookingEventTickets_{id} = function awEventbookingEventTicket.prototype.updateAttributes__patch_AwEventbookingEventTickets_{id} (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.updateAttributes__patch_AwEventbookingEventTickets_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.prototype.updateAttributes__put_AwEventbookingEventTickets_{id} = function awEventbookingEventTicket.prototype.updateAttributes__put_AwEventbookingEventTickets_{id} (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.prototype.updateAttributes__put_AwEventbookingEventTickets_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.replaceById = function awEventbookingEventTicket.replaceById (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.replaceById(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.replaceOrCreate = function awEventbookingEventTicket.replaceOrCreate (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.updateAll = function awEventbookingEventTicket.updateAll (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.updateAll(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.upsertWithWhere = function awEventbookingEventTicket.upsertWithWhere (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.upsert__patch_AwEventbookingEventTickets = function awEventbookingEventTicket.upsert__patch_AwEventbookingEventTickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.upsert__patch_AwEventbookingEventTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEventTicket.upsert__put_AwEventbookingEventTickets = function awEventbookingEventTicket.upsert__put_AwEventbookingEventTickets (req, res, next) {
  AwEventbookingEventTicket.awEventbookingEventTicket.upsert__put_AwEventbookingEventTickets(req.swagger.params, res, next);
};
