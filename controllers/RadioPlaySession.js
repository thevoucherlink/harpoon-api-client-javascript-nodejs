'use strict';

var url = require('url');


var RadioPlaySession = require('./RadioPlaySessionService');


module.exports.radioPlaySession.count = function radioPlaySession.count (req, res, next) {
  RadioPlaySession.radioPlaySession.count(req.swagger.params, res, next);
};

module.exports.radioPlaySession.create = function radioPlaySession.create (req, res, next) {
  RadioPlaySession.radioPlaySession.create(req.swagger.params, res, next);
};

module.exports.radioPlaySession.createChangeStream__get_RadioPlaySessions_changeStream = function radioPlaySession.createChangeStream__get_RadioPlaySessions_changeStream (req, res, next) {
  RadioPlaySession.radioPlaySession.createChangeStream__get_RadioPlaySessions_changeStream(req.swagger.params, res, next);
};

module.exports.radioPlaySession.createChangeStream__post_RadioPlaySessions_changeStream = function radioPlaySession.createChangeStream__post_RadioPlaySessions_changeStream (req, res, next) {
  RadioPlaySession.radioPlaySession.createChangeStream__post_RadioPlaySessions_changeStream(req.swagger.params, res, next);
};

module.exports.radioPlaySession.deleteById = function radioPlaySession.deleteById (req, res, next) {
  RadioPlaySession.radioPlaySession.deleteById(req.swagger.params, res, next);
};

module.exports.radioPlaySession.exists__get_RadioPlaySessions_{id}_exists = function radioPlaySession.exists__get_RadioPlaySessions_{id}_exists (req, res, next) {
  RadioPlaySession.radioPlaySession.exists__get_RadioPlaySessions_{id}_exists(req.swagger.params, res, next);
};

module.exports.radioPlaySession.exists__head_RadioPlaySessions_{id} = function radioPlaySession.exists__head_RadioPlaySessions_{id} (req, res, next) {
  RadioPlaySession.radioPlaySession.exists__head_RadioPlaySessions_{id}(req.swagger.params, res, next);
};

module.exports.radioPlaySession.find = function radioPlaySession.find (req, res, next) {
  RadioPlaySession.radioPlaySession.find(req.swagger.params, res, next);
};

module.exports.radioPlaySession.findById = function radioPlaySession.findById (req, res, next) {
  RadioPlaySession.radioPlaySession.findById(req.swagger.params, res, next);
};

module.exports.radioPlaySession.findOne = function radioPlaySession.findOne (req, res, next) {
  RadioPlaySession.radioPlaySession.findOne(req.swagger.params, res, next);
};

module.exports.radioPlaySession.prototype.updateAttributes__patch_RadioPlaySessions_{id} = function radioPlaySession.prototype.updateAttributes__patch_RadioPlaySessions_{id} (req, res, next) {
  RadioPlaySession.radioPlaySession.prototype.updateAttributes__patch_RadioPlaySessions_{id}(req.swagger.params, res, next);
};

module.exports.radioPlaySession.prototype.updateAttributes__put_RadioPlaySessions_{id} = function radioPlaySession.prototype.updateAttributes__put_RadioPlaySessions_{id} (req, res, next) {
  RadioPlaySession.radioPlaySession.prototype.updateAttributes__put_RadioPlaySessions_{id}(req.swagger.params, res, next);
};

module.exports.radioPlaySession.replaceById = function radioPlaySession.replaceById (req, res, next) {
  RadioPlaySession.radioPlaySession.replaceById(req.swagger.params, res, next);
};

module.exports.radioPlaySession.replaceOrCreate = function radioPlaySession.replaceOrCreate (req, res, next) {
  RadioPlaySession.radioPlaySession.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.radioPlaySession.updateAll = function radioPlaySession.updateAll (req, res, next) {
  RadioPlaySession.radioPlaySession.updateAll(req.swagger.params, res, next);
};

module.exports.radioPlaySession.upsertWithWhere = function radioPlaySession.upsertWithWhere (req, res, next) {
  RadioPlaySession.radioPlaySession.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.radioPlaySession.upsert__patch_RadioPlaySessions = function radioPlaySession.upsert__patch_RadioPlaySessions (req, res, next) {
  RadioPlaySession.radioPlaySession.upsert__patch_RadioPlaySessions(req.swagger.params, res, next);
};

module.exports.radioPlaySession.upsert__put_RadioPlaySessions = function radioPlaySession.upsert__put_RadioPlaySessions (req, res, next) {
  RadioPlaySession.radioPlaySession.upsert__put_RadioPlaySessions(req.swagger.params, res, next);
};
