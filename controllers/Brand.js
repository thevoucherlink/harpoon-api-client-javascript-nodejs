'use strict';

var url = require('url');


var Brand = require('./BrandService');


module.exports.brand.allFeeds = function brand.allFeeds (req, res, next) {
  Brand.brand.allFeeds(req.swagger.params, res, next);
};

module.exports.brand.competitions = function brand.competitions (req, res, next) {
  Brand.brand.competitions(req.swagger.params, res, next);
};

module.exports.brand.coupons = function brand.coupons (req, res, next) {
  Brand.brand.coupons(req.swagger.params, res, next);
};

module.exports.brand.dealGroups = function brand.dealGroups (req, res, next) {
  Brand.brand.dealGroups(req.swagger.params, res, next);
};

module.exports.brand.dealPaids = function brand.dealPaids (req, res, next) {
  Brand.brand.dealPaids(req.swagger.params, res, next);
};

module.exports.brand.deals = function brand.deals (req, res, next) {
  Brand.brand.deals(req.swagger.params, res, next);
};

module.exports.brand.events = function brand.events (req, res, next) {
  Brand.brand.events(req.swagger.params, res, next);
};

module.exports.brand.feeds = function brand.feeds (req, res, next) {
  Brand.brand.feeds(req.swagger.params, res, next);
};

module.exports.brand.find = function brand.find (req, res, next) {
  Brand.brand.find(req.swagger.params, res, next);
};

module.exports.brand.findById = function brand.findById (req, res, next) {
  Brand.brand.findById(req.swagger.params, res, next);
};

module.exports.brand.findOne = function brand.findOne (req, res, next) {
  Brand.brand.findOne(req.swagger.params, res, next);
};

module.exports.brand.followers = function brand.followers (req, res, next) {
  Brand.brand.followers(req.swagger.params, res, next);
};

module.exports.brand.followersCreate = function brand.followersCreate (req, res, next) {
  Brand.brand.followersCreate(req.swagger.params, res, next);
};

module.exports.brand.followersDelete = function brand.followersDelete (req, res, next) {
  Brand.brand.followersDelete(req.swagger.params, res, next);
};

module.exports.brand.offers = function brand.offers (req, res, next) {
  Brand.brand.offers(req.swagger.params, res, next);
};

module.exports.brand.replaceById = function brand.replaceById (req, res, next) {
  Brand.brand.replaceById(req.swagger.params, res, next);
};

module.exports.brand.replaceOrCreate = function brand.replaceOrCreate (req, res, next) {
  Brand.brand.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.brand.upsertWithWhere = function brand.upsertWithWhere (req, res, next) {
  Brand.brand.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.brand.venues = function brand.venues (req, res, next) {
  Brand.brand.venues(req.swagger.params, res, next);
};
