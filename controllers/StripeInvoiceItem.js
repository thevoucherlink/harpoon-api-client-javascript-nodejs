'use strict';

var url = require('url');


var StripeInvoiceItem = require('./StripeInvoiceItemService');


module.exports.stripeInvoiceItem.count = function stripeInvoiceItem.count (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.count(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.create = function stripeInvoiceItem.create (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.create(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.createChangeStream__get_StripeInvoiceItems_changeStream = function stripeInvoiceItem.createChangeStream__get_StripeInvoiceItems_changeStream (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.createChangeStream__get_StripeInvoiceItems_changeStream(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.createChangeStream__post_StripeInvoiceItems_changeStream = function stripeInvoiceItem.createChangeStream__post_StripeInvoiceItems_changeStream (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.createChangeStream__post_StripeInvoiceItems_changeStream(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.deleteById = function stripeInvoiceItem.deleteById (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.deleteById(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.exists__get_StripeInvoiceItems_{id}_exists = function stripeInvoiceItem.exists__get_StripeInvoiceItems_{id}_exists (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.exists__get_StripeInvoiceItems_{id}_exists(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.exists__head_StripeInvoiceItems_{id} = function stripeInvoiceItem.exists__head_StripeInvoiceItems_{id} (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.exists__head_StripeInvoiceItems_{id}(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.find = function stripeInvoiceItem.find (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.find(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.findById = function stripeInvoiceItem.findById (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.findById(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.findOne = function stripeInvoiceItem.findOne (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.findOne(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.prototype.updateAttributes__patch_StripeInvoiceItems_{id} = function stripeInvoiceItem.prototype.updateAttributes__patch_StripeInvoiceItems_{id} (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.prototype.updateAttributes__patch_StripeInvoiceItems_{id}(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.prototype.updateAttributes__put_StripeInvoiceItems_{id} = function stripeInvoiceItem.prototype.updateAttributes__put_StripeInvoiceItems_{id} (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.prototype.updateAttributes__put_StripeInvoiceItems_{id}(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.replaceById = function stripeInvoiceItem.replaceById (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.replaceById(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.replaceOrCreate = function stripeInvoiceItem.replaceOrCreate (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.updateAll = function stripeInvoiceItem.updateAll (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.updateAll(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.upsertWithWhere = function stripeInvoiceItem.upsertWithWhere (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.upsert__patch_StripeInvoiceItems = function stripeInvoiceItem.upsert__patch_StripeInvoiceItems (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.upsert__patch_StripeInvoiceItems(req.swagger.params, res, next);
};

module.exports.stripeInvoiceItem.upsert__put_StripeInvoiceItems = function stripeInvoiceItem.upsert__put_StripeInvoiceItems (req, res, next) {
  StripeInvoiceItem.stripeInvoiceItem.upsert__put_StripeInvoiceItems(req.swagger.params, res, next);
};
