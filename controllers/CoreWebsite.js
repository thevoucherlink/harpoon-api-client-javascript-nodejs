'use strict';

var url = require('url');


var CoreWebsite = require('./CoreWebsiteService');


module.exports.coreWebsite.count = function coreWebsite.count (req, res, next) {
  CoreWebsite.coreWebsite.count(req.swagger.params, res, next);
};

module.exports.coreWebsite.create = function coreWebsite.create (req, res, next) {
  CoreWebsite.coreWebsite.create(req.swagger.params, res, next);
};

module.exports.coreWebsite.createChangeStream__get_CoreWebsites_changeStream = function coreWebsite.createChangeStream__get_CoreWebsites_changeStream (req, res, next) {
  CoreWebsite.coreWebsite.createChangeStream__get_CoreWebsites_changeStream(req.swagger.params, res, next);
};

module.exports.coreWebsite.createChangeStream__post_CoreWebsites_changeStream = function coreWebsite.createChangeStream__post_CoreWebsites_changeStream (req, res, next) {
  CoreWebsite.coreWebsite.createChangeStream__post_CoreWebsites_changeStream(req.swagger.params, res, next);
};

module.exports.coreWebsite.deleteById = function coreWebsite.deleteById (req, res, next) {
  CoreWebsite.coreWebsite.deleteById(req.swagger.params, res, next);
};

module.exports.coreWebsite.exists__get_CoreWebsites_{id}_exists = function coreWebsite.exists__get_CoreWebsites_{id}_exists (req, res, next) {
  CoreWebsite.coreWebsite.exists__get_CoreWebsites_{id}_exists(req.swagger.params, res, next);
};

module.exports.coreWebsite.exists__head_CoreWebsites_{id} = function coreWebsite.exists__head_CoreWebsites_{id} (req, res, next) {
  CoreWebsite.coreWebsite.exists__head_CoreWebsites_{id}(req.swagger.params, res, next);
};

module.exports.coreWebsite.find = function coreWebsite.find (req, res, next) {
  CoreWebsite.coreWebsite.find(req.swagger.params, res, next);
};

module.exports.coreWebsite.findById = function coreWebsite.findById (req, res, next) {
  CoreWebsite.coreWebsite.findById(req.swagger.params, res, next);
};

module.exports.coreWebsite.findOne = function coreWebsite.findOne (req, res, next) {
  CoreWebsite.coreWebsite.findOne(req.swagger.params, res, next);
};

module.exports.coreWebsite.prototype.updateAttributes__patch_CoreWebsites_{id} = function coreWebsite.prototype.updateAttributes__patch_CoreWebsites_{id} (req, res, next) {
  CoreWebsite.coreWebsite.prototype.updateAttributes__patch_CoreWebsites_{id}(req.swagger.params, res, next);
};

module.exports.coreWebsite.prototype.updateAttributes__put_CoreWebsites_{id} = function coreWebsite.prototype.updateAttributes__put_CoreWebsites_{id} (req, res, next) {
  CoreWebsite.coreWebsite.prototype.updateAttributes__put_CoreWebsites_{id}(req.swagger.params, res, next);
};

module.exports.coreWebsite.replaceById = function coreWebsite.replaceById (req, res, next) {
  CoreWebsite.coreWebsite.replaceById(req.swagger.params, res, next);
};

module.exports.coreWebsite.replaceOrCreate = function coreWebsite.replaceOrCreate (req, res, next) {
  CoreWebsite.coreWebsite.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.coreWebsite.updateAll = function coreWebsite.updateAll (req, res, next) {
  CoreWebsite.coreWebsite.updateAll(req.swagger.params, res, next);
};

module.exports.coreWebsite.upsertWithWhere = function coreWebsite.upsertWithWhere (req, res, next) {
  CoreWebsite.coreWebsite.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.coreWebsite.upsert__patch_CoreWebsites = function coreWebsite.upsert__patch_CoreWebsites (req, res, next) {
  CoreWebsite.coreWebsite.upsert__patch_CoreWebsites(req.swagger.params, res, next);
};

module.exports.coreWebsite.upsert__put_CoreWebsites = function coreWebsite.upsert__put_CoreWebsites (req, res, next) {
  CoreWebsite.coreWebsite.upsert__put_CoreWebsites(req.swagger.params, res, next);
};
