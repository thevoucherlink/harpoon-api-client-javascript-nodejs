'use strict';

var url = require('url');


var Event = require('./EventService');


module.exports.event.attendees = function event.attendees (req, res, next) {
  Event.event.attendees(req.swagger.params, res, next);
};

module.exports.event.checkout = function event.checkout (req, res, next) {
  Event.event.checkout(req.swagger.params, res, next);
};

module.exports.event.find = function event.find (req, res, next) {
  Event.event.find(req.swagger.params, res, next);
};

module.exports.event.findById = function event.findById (req, res, next) {
  Event.event.findById(req.swagger.params, res, next);
};

module.exports.event.findInLater = function event.findInLater (req, res, next) {
  Event.event.findInLater(req.swagger.params, res, next);
};

module.exports.event.findInThisMonth = function event.findInThisMonth (req, res, next) {
  Event.event.findInThisMonth(req.swagger.params, res, next);
};

module.exports.event.findOne = function event.findOne (req, res, next) {
  Event.event.findOne(req.swagger.params, res, next);
};

module.exports.event.redeem = function event.redeem (req, res, next) {
  Event.event.redeem(req.swagger.params, res, next);
};

module.exports.event.replaceById = function event.replaceById (req, res, next) {
  Event.event.replaceById(req.swagger.params, res, next);
};

module.exports.event.replaceOrCreate = function event.replaceOrCreate (req, res, next) {
  Event.event.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.event.upsertWithWhere = function event.upsertWithWhere (req, res, next) {
  Event.event.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.event.venues = function event.venues (req, res, next) {
  Event.event.venues(req.swagger.params, res, next);
};
