'use strict';

exports.stripeInvoiceItem.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.createChangeStream__get_StripeInvoiceItems_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.createChangeStream__post_StripeInvoiceItems_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.exists__get_StripeInvoiceItems_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.exists__head_StripeInvoiceItems_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.prototype.updateAttributes__patch_StripeInvoiceItems_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.prototype.updateAttributes__put_StripeInvoiceItems_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.upsert__patch_StripeInvoiceItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoiceItem.upsert__put_StripeInvoiceItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeInvoiceItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

