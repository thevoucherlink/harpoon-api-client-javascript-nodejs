'use strict';

var url = require('url');


var HarpoonHpublicApplicationpartner = require('./HarpoonHpublicApplicationpartnerService');


module.exports.harpoonHpublicApplicationpartner.count = function harpoonHpublicApplicationpartner.count (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.count(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.create = function harpoonHpublicApplicationpartner.create (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.create(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.createChangeStream__get_HarpoonHpublicApplicationpartners_changeStream = function harpoonHpublicApplicationpartner.createChangeStream__get_HarpoonHpublicApplicationpartners_changeStream (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.createChangeStream__get_HarpoonHpublicApplicationpartners_changeStream(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.createChangeStream__post_HarpoonHpublicApplicationpartners_changeStream = function harpoonHpublicApplicationpartner.createChangeStream__post_HarpoonHpublicApplicationpartners_changeStream (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.createChangeStream__post_HarpoonHpublicApplicationpartners_changeStream(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.deleteById = function harpoonHpublicApplicationpartner.deleteById (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.deleteById(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.exists__get_HarpoonHpublicApplicationpartners_{id}_exists = function harpoonHpublicApplicationpartner.exists__get_HarpoonHpublicApplicationpartners_{id}_exists (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.exists__get_HarpoonHpublicApplicationpartners_{id}_exists(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.exists__head_HarpoonHpublicApplicationpartners_{id} = function harpoonHpublicApplicationpartner.exists__head_HarpoonHpublicApplicationpartners_{id} (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.exists__head_HarpoonHpublicApplicationpartners_{id}(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.find = function harpoonHpublicApplicationpartner.find (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.find(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.findById = function harpoonHpublicApplicationpartner.findById (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.findById(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.findOne = function harpoonHpublicApplicationpartner.findOne (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.findOne(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.prototype.__get__udropshipVendor = function harpoonHpublicApplicationpartner.prototype.__get__udropshipVendor (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.prototype.__get__udropshipVendor(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.prototype.updateAttributes__patch_HarpoonHpublicApplicationpartners_{id} = function harpoonHpublicApplicationpartner.prototype.updateAttributes__patch_HarpoonHpublicApplicationpartners_{id} (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.prototype.updateAttributes__patch_HarpoonHpublicApplicationpartners_{id}(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.prototype.updateAttributes__put_HarpoonHpublicApplicationpartners_{id} = function harpoonHpublicApplicationpartner.prototype.updateAttributes__put_HarpoonHpublicApplicationpartners_{id} (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.prototype.updateAttributes__put_HarpoonHpublicApplicationpartners_{id}(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.replaceById = function harpoonHpublicApplicationpartner.replaceById (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.replaceById(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.replaceOrCreate = function harpoonHpublicApplicationpartner.replaceOrCreate (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.updateAll = function harpoonHpublicApplicationpartner.updateAll (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.updateAll(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.upsertWithWhere = function harpoonHpublicApplicationpartner.upsertWithWhere (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.upsert__patch_HarpoonHpublicApplicationpartners = function harpoonHpublicApplicationpartner.upsert__patch_HarpoonHpublicApplicationpartners (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.upsert__patch_HarpoonHpublicApplicationpartners(req.swagger.params, res, next);
};

module.exports.harpoonHpublicApplicationpartner.upsert__put_HarpoonHpublicApplicationpartners = function harpoonHpublicApplicationpartner.upsert__put_HarpoonHpublicApplicationpartners (req, res, next) {
  HarpoonHpublicApplicationpartner.harpoonHpublicApplicationpartner.upsert__put_HarpoonHpublicApplicationpartners(req.swagger.params, res, next);
};
