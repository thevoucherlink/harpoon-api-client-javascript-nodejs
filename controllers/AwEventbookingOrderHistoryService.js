'use strict';

exports.awEventbookingOrderHistory.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.createChangeStream__get_AwEventbookingOrderHistories_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.createChangeStream__post_AwEventbookingOrderHistories_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.exists__get_AwEventbookingOrderHistories_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.exists__head_AwEventbookingOrderHistories_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.prototype.updateAttributes__patch_AwEventbookingOrderHistories_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.prototype.updateAttributes__put_AwEventbookingOrderHistories_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.upsert__patch_AwEventbookingOrderHistories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingOrderHistory.upsert__put_AwEventbookingOrderHistories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingOrderHistory)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

