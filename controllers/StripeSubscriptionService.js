'use strict';

exports.stripeSubscription.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.createChangeStream__get_StripeSubscriptions_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.createChangeStream__post_StripeSubscriptions_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.exists__get_StripeSubscriptions_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.exists__head_StripeSubscriptions_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.prototype.updateAttributes__patch_StripeSubscriptions_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.prototype.updateAttributes__put_StripeSubscriptions_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.upsert__patch_StripeSubscriptions = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeSubscription.upsert__put_StripeSubscriptions = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeSubscription)
  **/
    var examples = {};
  examples['application/json'] = {
  "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "quantity" : 1.3579000000000001069366817318950779736042022705078125,
  "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
  "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
  "cancelAtPeriodEnd" : true,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "plan" : {
    "statement_descriptor" : "aeiou",
    "amount" : "aeiou",
    "metadata" : "aeiou",
    "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "currency" : "aeiou",
    "interval" : "aeiou",
    "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou",
  "customer" : "aeiou",
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

