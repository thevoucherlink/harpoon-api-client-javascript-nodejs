'use strict';

exports.offer.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.offer.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.offer.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Offer)
  **/
    var examples = {};
  examples['application/json'] = {
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.offer.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Offer)
  **/
    var examples = {};
  examples['application/json'] = {
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.offer.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (Offer)
  **/
    var examples = {};
  examples['application/json'] = {
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

