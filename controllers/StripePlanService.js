'use strict';

exports.stripePlan.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.createChangeStream__get_StripePlans_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.createChangeStream__post_StripePlans_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.exists__get_StripePlans_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.exists__head_StripePlans_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.prototype.updateAttributes__patch_StripePlans_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.prototype.updateAttributes__put_StripePlans_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.upsert__patch_StripePlans = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripePlan.upsert__put_StripePlans = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripePlan)
  **/
    var examples = {};
  examples['application/json'] = {
  "statement_descriptor" : "aeiou",
  "amount" : "aeiou",
  "metadata" : "aeiou",
  "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "currency" : "aeiou",
  "interval" : "aeiou",
  "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

