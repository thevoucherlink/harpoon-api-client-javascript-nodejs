'use strict';

var url = require('url');


var DealGroup = require('./DealGroupService');


module.exports.dealGroup.checkout = function dealGroup.checkout (req, res, next) {
  DealGroup.dealGroup.checkout(req.swagger.params, res, next);
};

module.exports.dealGroup.find = function dealGroup.find (req, res, next) {
  DealGroup.dealGroup.find(req.swagger.params, res, next);
};

module.exports.dealGroup.findById = function dealGroup.findById (req, res, next) {
  DealGroup.dealGroup.findById(req.swagger.params, res, next);
};

module.exports.dealGroup.findOne = function dealGroup.findOne (req, res, next) {
  DealGroup.dealGroup.findOne(req.swagger.params, res, next);
};

module.exports.dealGroup.redeem = function dealGroup.redeem (req, res, next) {
  DealGroup.dealGroup.redeem(req.swagger.params, res, next);
};

module.exports.dealGroup.replaceById = function dealGroup.replaceById (req, res, next) {
  DealGroup.dealGroup.replaceById(req.swagger.params, res, next);
};

module.exports.dealGroup.replaceOrCreate = function dealGroup.replaceOrCreate (req, res, next) {
  DealGroup.dealGroup.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.dealGroup.upsertWithWhere = function dealGroup.upsertWithWhere (req, res, next) {
  DealGroup.dealGroup.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.dealGroup.venues = function dealGroup.venues (req, res, next) {
  DealGroup.dealGroup.venues(req.swagger.params, res, next);
};
