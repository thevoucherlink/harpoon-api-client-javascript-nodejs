'use strict';

var url = require('url');


var CatalogProduct = require('./CatalogProductService');


module.exports.catalogProduct.count = function catalogProduct.count (req, res, next) {
  CatalogProduct.catalogProduct.count(req.swagger.params, res, next);
};

module.exports.catalogProduct.create = function catalogProduct.create (req, res, next) {
  CatalogProduct.catalogProduct.create(req.swagger.params, res, next);
};

module.exports.catalogProduct.createChangeStream__get_CatalogProducts_changeStream = function catalogProduct.createChangeStream__get_CatalogProducts_changeStream (req, res, next) {
  CatalogProduct.catalogProduct.createChangeStream__get_CatalogProducts_changeStream(req.swagger.params, res, next);
};

module.exports.catalogProduct.createChangeStream__post_CatalogProducts_changeStream = function catalogProduct.createChangeStream__post_CatalogProducts_changeStream (req, res, next) {
  CatalogProduct.catalogProduct.createChangeStream__post_CatalogProducts_changeStream(req.swagger.params, res, next);
};

module.exports.catalogProduct.deleteById = function catalogProduct.deleteById (req, res, next) {
  CatalogProduct.catalogProduct.deleteById(req.swagger.params, res, next);
};

module.exports.catalogProduct.exists__get_CatalogProducts_{id}_exists = function catalogProduct.exists__get_CatalogProducts_{id}_exists (req, res, next) {
  CatalogProduct.catalogProduct.exists__get_CatalogProducts_{id}_exists(req.swagger.params, res, next);
};

module.exports.catalogProduct.exists__head_CatalogProducts_{id} = function catalogProduct.exists__head_CatalogProducts_{id} (req, res, next) {
  CatalogProduct.catalogProduct.exists__head_CatalogProducts_{id}(req.swagger.params, res, next);
};

module.exports.catalogProduct.find = function catalogProduct.find (req, res, next) {
  CatalogProduct.catalogProduct.find(req.swagger.params, res, next);
};

module.exports.catalogProduct.findById = function catalogProduct.findById (req, res, next) {
  CatalogProduct.catalogProduct.findById(req.swagger.params, res, next);
};

module.exports.catalogProduct.findOne = function catalogProduct.findOne (req, res, next) {
  CatalogProduct.catalogProduct.findOne(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__count__catalogCategories = function catalogProduct.prototype.__count__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__count__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__count__udropshipVendorProducts = function catalogProduct.prototype.__count__udropshipVendorProducts (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__count__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__count__udropshipVendors = function catalogProduct.prototype.__count__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__count__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__create__awCollpurDeal = function catalogProduct.prototype.__create__awCollpurDeal (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__create__awCollpurDeal(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__create__catalogCategories = function catalogProduct.prototype.__create__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__create__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__create__inventory = function catalogProduct.prototype.__create__inventory (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__create__inventory(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__create__productEventCompetition = function catalogProduct.prototype.__create__productEventCompetition (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__create__productEventCompetition(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__create__udropshipVendorProducts = function catalogProduct.prototype.__create__udropshipVendorProducts (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__create__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__create__udropshipVendors = function catalogProduct.prototype.__create__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__create__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__delete__catalogCategories = function catalogProduct.prototype.__delete__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__delete__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__delete__udropshipVendorProducts = function catalogProduct.prototype.__delete__udropshipVendorProducts (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__delete__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__delete__udropshipVendors = function catalogProduct.prototype.__delete__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__delete__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__destroyById__catalogCategories = function catalogProduct.prototype.__destroyById__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__destroyById__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__destroyById__udropshipVendorProducts = function catalogProduct.prototype.__destroyById__udropshipVendorProducts (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__destroyById__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__destroyById__udropshipVendors = function catalogProduct.prototype.__destroyById__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__destroyById__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__destroy__awCollpurDeal = function catalogProduct.prototype.__destroy__awCollpurDeal (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__destroy__awCollpurDeal(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__destroy__inventory = function catalogProduct.prototype.__destroy__inventory (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__destroy__inventory(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__destroy__productEventCompetition = function catalogProduct.prototype.__destroy__productEventCompetition (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__destroy__productEventCompetition(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__exists__catalogCategories = function catalogProduct.prototype.__exists__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__exists__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__exists__udropshipVendors = function catalogProduct.prototype.__exists__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__exists__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__findById__catalogCategories = function catalogProduct.prototype.__findById__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__findById__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__findById__udropshipVendorProducts = function catalogProduct.prototype.__findById__udropshipVendorProducts (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__findById__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__findById__udropshipVendors = function catalogProduct.prototype.__findById__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__findById__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__get__awCollpurDeal = function catalogProduct.prototype.__get__awCollpurDeal (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__get__awCollpurDeal(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__get__catalogCategories = function catalogProduct.prototype.__get__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__get__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__get__checkoutAgreement = function catalogProduct.prototype.__get__checkoutAgreement (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__get__checkoutAgreement(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__get__creator = function catalogProduct.prototype.__get__creator (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__get__creator(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__get__inventory = function catalogProduct.prototype.__get__inventory (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__get__inventory(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__get__productEventCompetition = function catalogProduct.prototype.__get__productEventCompetition (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__get__productEventCompetition(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__get__udropshipVendorProducts = function catalogProduct.prototype.__get__udropshipVendorProducts (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__get__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__get__udropshipVendors = function catalogProduct.prototype.__get__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__get__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__link__catalogCategories = function catalogProduct.prototype.__link__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__link__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__link__udropshipVendors = function catalogProduct.prototype.__link__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__link__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__unlink__catalogCategories = function catalogProduct.prototype.__unlink__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__unlink__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__unlink__udropshipVendors = function catalogProduct.prototype.__unlink__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__unlink__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__updateById__catalogCategories = function catalogProduct.prototype.__updateById__catalogCategories (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__updateById__catalogCategories(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__updateById__udropshipVendorProducts = function catalogProduct.prototype.__updateById__udropshipVendorProducts (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__updateById__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__updateById__udropshipVendors = function catalogProduct.prototype.__updateById__udropshipVendors (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__updateById__udropshipVendors(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__update__awCollpurDeal = function catalogProduct.prototype.__update__awCollpurDeal (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__update__awCollpurDeal(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__update__inventory = function catalogProduct.prototype.__update__inventory (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__update__inventory(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.__update__productEventCompetition = function catalogProduct.prototype.__update__productEventCompetition (req, res, next) {
  CatalogProduct.catalogProduct.prototype.__update__productEventCompetition(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.updateAttributes__patch_CatalogProducts_{id} = function catalogProduct.prototype.updateAttributes__patch_CatalogProducts_{id} (req, res, next) {
  CatalogProduct.catalogProduct.prototype.updateAttributes__patch_CatalogProducts_{id}(req.swagger.params, res, next);
};

module.exports.catalogProduct.prototype.updateAttributes__put_CatalogProducts_{id} = function catalogProduct.prototype.updateAttributes__put_CatalogProducts_{id} (req, res, next) {
  CatalogProduct.catalogProduct.prototype.updateAttributes__put_CatalogProducts_{id}(req.swagger.params, res, next);
};

module.exports.catalogProduct.replaceById = function catalogProduct.replaceById (req, res, next) {
  CatalogProduct.catalogProduct.replaceById(req.swagger.params, res, next);
};

module.exports.catalogProduct.replaceOrCreate = function catalogProduct.replaceOrCreate (req, res, next) {
  CatalogProduct.catalogProduct.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.catalogProduct.updateAll = function catalogProduct.updateAll (req, res, next) {
  CatalogProduct.catalogProduct.updateAll(req.swagger.params, res, next);
};

module.exports.catalogProduct.upsertWithWhere = function catalogProduct.upsertWithWhere (req, res, next) {
  CatalogProduct.catalogProduct.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.catalogProduct.upsert__patch_CatalogProducts = function catalogProduct.upsert__patch_CatalogProducts (req, res, next) {
  CatalogProduct.catalogProduct.upsert__patch_CatalogProducts(req.swagger.params, res, next);
};

module.exports.catalogProduct.upsert__put_CatalogProducts = function catalogProduct.upsert__put_CatalogProducts (req, res, next) {
  CatalogProduct.catalogProduct.upsert__put_CatalogProducts(req.swagger.params, res, next);
};
