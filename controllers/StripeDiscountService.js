'use strict';

exports.stripeDiscount.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.createChangeStream__get_StripeDiscounts_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.createChangeStream__post_StripeDiscounts_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.exists__get_StripeDiscounts_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.exists__head_StripeDiscounts_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.prototype.updateAttributes__patch_StripeDiscounts_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.prototype.updateAttributes__put_StripeDiscounts_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.upsert__patch_StripeDiscounts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeDiscount.upsert__put_StripeDiscounts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeDiscount)
  **/
    var examples = {};
  examples['application/json'] = {
  "coupon" : {
    "metadata" : "aeiou",
    "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
    "livemode" : true,
    "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
    "created" : 1.3579000000000001069366817318950779736042022705078125,
    "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
    "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
    "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
    "valid" : true,
    "duration" : "aeiou",
    "currency" : "aeiou",
    "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  },
  "start" : 1.3579000000000001069366817318950779736042022705078125,
  "end" : 1.3579000000000001069366817318950779736042022705078125,
  "subscription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

