'use strict';

exports.awEventbookingEventTicket.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.createChangeStream__get_AwEventbookingEventTickets_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.createChangeStream__post_AwEventbookingEventTickets_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.exists__get_AwEventbookingEventTickets_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.exists__head_AwEventbookingEventTickets_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__count__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__count__awEventbookingTicket = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__count__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__create__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEventTicketAttribute)
  **/
    var examples = {};
  examples['application/json'] = {
  "attributeCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "value" : "aeiou",
  "ticketId" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__create__awEventbookingTicket = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__create__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__delete__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEventTicket.prototype.__delete__awEventbookingTicket = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEventTicket.prototype.__delete__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEventTicket.prototype.__destroyById__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEventTicket.prototype.__destroyById__awEventbookingTicket = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEventTicket.prototype.__destroyById__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEventTicket.prototype.__findById__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "attributeCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "value" : "aeiou",
  "ticketId" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__findById__awEventbookingTicket = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__findById__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__get__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "attributeCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "value" : "aeiou",
  "ticketId" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__get__awEventbookingTicket = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__get__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__updateById__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AwEventbookingEventTicketAttribute)
  **/
    var examples = {};
  examples['application/json'] = {
  "attributeCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "value" : "aeiou",
  "ticketId" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__updateById__awEventbookingTicket = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.__updateById__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.updateAttributes__patch_AwEventbookingEventTickets_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.prototype.updateAttributes__put_AwEventbookingEventTickets_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.upsert__patch_AwEventbookingEventTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEventTicket.upsert__put_AwEventbookingEventTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

