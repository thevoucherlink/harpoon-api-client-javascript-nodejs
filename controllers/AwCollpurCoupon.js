'use strict';

var url = require('url');


var AwCollpurCoupon = require('./AwCollpurCouponService');


module.exports.awCollpurCoupon.count = function awCollpurCoupon.count (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.count(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.create = function awCollpurCoupon.create (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.create(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.createChangeStream__get_AwCollpurCoupons_changeStream = function awCollpurCoupon.createChangeStream__get_AwCollpurCoupons_changeStream (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.createChangeStream__get_AwCollpurCoupons_changeStream(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.createChangeStream__post_AwCollpurCoupons_changeStream = function awCollpurCoupon.createChangeStream__post_AwCollpurCoupons_changeStream (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.createChangeStream__post_AwCollpurCoupons_changeStream(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.deleteById = function awCollpurCoupon.deleteById (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.deleteById(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.exists__get_AwCollpurCoupons_{id}_exists = function awCollpurCoupon.exists__get_AwCollpurCoupons_{id}_exists (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.exists__get_AwCollpurCoupons_{id}_exists(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.exists__head_AwCollpurCoupons_{id} = function awCollpurCoupon.exists__head_AwCollpurCoupons_{id} (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.exists__head_AwCollpurCoupons_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.find = function awCollpurCoupon.find (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.find(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.findById = function awCollpurCoupon.findById (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.findById(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.findOne = function awCollpurCoupon.findOne (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.findOne(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.prototype.updateAttributes__patch_AwCollpurCoupons_{id} = function awCollpurCoupon.prototype.updateAttributes__patch_AwCollpurCoupons_{id} (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.prototype.updateAttributes__patch_AwCollpurCoupons_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.prototype.updateAttributes__put_AwCollpurCoupons_{id} = function awCollpurCoupon.prototype.updateAttributes__put_AwCollpurCoupons_{id} (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.prototype.updateAttributes__put_AwCollpurCoupons_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.replaceById = function awCollpurCoupon.replaceById (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.replaceById(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.replaceOrCreate = function awCollpurCoupon.replaceOrCreate (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.updateAll = function awCollpurCoupon.updateAll (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.updateAll(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.upsertWithWhere = function awCollpurCoupon.upsertWithWhere (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.upsert__patch_AwCollpurCoupons = function awCollpurCoupon.upsert__patch_AwCollpurCoupons (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.upsert__patch_AwCollpurCoupons(req.swagger.params, res, next);
};

module.exports.awCollpurCoupon.upsert__put_AwCollpurCoupons = function awCollpurCoupon.upsert__put_AwCollpurCoupons (req, res, next) {
  AwCollpurCoupon.awCollpurCoupon.upsert__put_AwCollpurCoupons(req.swagger.params, res, next);
};
