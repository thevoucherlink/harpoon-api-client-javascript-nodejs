'use strict';

var url = require('url');


var StripeInvoice = require('./StripeInvoiceService');


module.exports.stripeInvoice.count = function stripeInvoice.count (req, res, next) {
  StripeInvoice.stripeInvoice.count(req.swagger.params, res, next);
};

module.exports.stripeInvoice.create = function stripeInvoice.create (req, res, next) {
  StripeInvoice.stripeInvoice.create(req.swagger.params, res, next);
};

module.exports.stripeInvoice.createChangeStream__get_StripeInvoices_changeStream = function stripeInvoice.createChangeStream__get_StripeInvoices_changeStream (req, res, next) {
  StripeInvoice.stripeInvoice.createChangeStream__get_StripeInvoices_changeStream(req.swagger.params, res, next);
};

module.exports.stripeInvoice.createChangeStream__post_StripeInvoices_changeStream = function stripeInvoice.createChangeStream__post_StripeInvoices_changeStream (req, res, next) {
  StripeInvoice.stripeInvoice.createChangeStream__post_StripeInvoices_changeStream(req.swagger.params, res, next);
};

module.exports.stripeInvoice.deleteById = function stripeInvoice.deleteById (req, res, next) {
  StripeInvoice.stripeInvoice.deleteById(req.swagger.params, res, next);
};

module.exports.stripeInvoice.exists__get_StripeInvoices_{id}_exists = function stripeInvoice.exists__get_StripeInvoices_{id}_exists (req, res, next) {
  StripeInvoice.stripeInvoice.exists__get_StripeInvoices_{id}_exists(req.swagger.params, res, next);
};

module.exports.stripeInvoice.exists__head_StripeInvoices_{id} = function stripeInvoice.exists__head_StripeInvoices_{id} (req, res, next) {
  StripeInvoice.stripeInvoice.exists__head_StripeInvoices_{id}(req.swagger.params, res, next);
};

module.exports.stripeInvoice.find = function stripeInvoice.find (req, res, next) {
  StripeInvoice.stripeInvoice.find(req.swagger.params, res, next);
};

module.exports.stripeInvoice.findById = function stripeInvoice.findById (req, res, next) {
  StripeInvoice.stripeInvoice.findById(req.swagger.params, res, next);
};

module.exports.stripeInvoice.findOne = function stripeInvoice.findOne (req, res, next) {
  StripeInvoice.stripeInvoice.findOne(req.swagger.params, res, next);
};

module.exports.stripeInvoice.prototype.updateAttributes__patch_StripeInvoices_{id} = function stripeInvoice.prototype.updateAttributes__patch_StripeInvoices_{id} (req, res, next) {
  StripeInvoice.stripeInvoice.prototype.updateAttributes__patch_StripeInvoices_{id}(req.swagger.params, res, next);
};

module.exports.stripeInvoice.prototype.updateAttributes__put_StripeInvoices_{id} = function stripeInvoice.prototype.updateAttributes__put_StripeInvoices_{id} (req, res, next) {
  StripeInvoice.stripeInvoice.prototype.updateAttributes__put_StripeInvoices_{id}(req.swagger.params, res, next);
};

module.exports.stripeInvoice.replaceById = function stripeInvoice.replaceById (req, res, next) {
  StripeInvoice.stripeInvoice.replaceById(req.swagger.params, res, next);
};

module.exports.stripeInvoice.replaceOrCreate = function stripeInvoice.replaceOrCreate (req, res, next) {
  StripeInvoice.stripeInvoice.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.stripeInvoice.updateAll = function stripeInvoice.updateAll (req, res, next) {
  StripeInvoice.stripeInvoice.updateAll(req.swagger.params, res, next);
};

module.exports.stripeInvoice.upsertWithWhere = function stripeInvoice.upsertWithWhere (req, res, next) {
  StripeInvoice.stripeInvoice.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.stripeInvoice.upsert__patch_StripeInvoices = function stripeInvoice.upsert__patch_StripeInvoices (req, res, next) {
  StripeInvoice.stripeInvoice.upsert__patch_StripeInvoices(req.swagger.params, res, next);
};

module.exports.stripeInvoice.upsert__put_StripeInvoices = function stripeInvoice.upsert__put_StripeInvoices (req, res, next) {
  StripeInvoice.stripeInvoice.upsert__put_StripeInvoices(req.swagger.params, res, next);
};
