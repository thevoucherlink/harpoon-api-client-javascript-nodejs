'use strict';

exports.radioPresenterRadioShow.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.createChangeStream__get_RadioPresenterRadioShows_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.createChangeStream__post_RadioPresenterRadioShows_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.exists__get_RadioPresenterRadioShows_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.exists__head_RadioPresenterRadioShows_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.prototype.__get__radioPresenter = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "name" : "aeiou",
  "radioShows" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.prototype.__get__radioShow = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.prototype.updateAttributes__patch_RadioPresenterRadioShows_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.prototype.updateAttributes__put_RadioPresenterRadioShows_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.upsert__patch_RadioPresenterRadioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPresenterRadioShow.upsert__put_RadioPresenterRadioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

