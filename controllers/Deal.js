'use strict';

var url = require('url');


var Deal = require('./DealService');


module.exports.deal.find = function deal.find (req, res, next) {
  Deal.deal.find(req.swagger.params, res, next);
};

module.exports.deal.findById = function deal.findById (req, res, next) {
  Deal.deal.findById(req.swagger.params, res, next);
};

module.exports.deal.findOne = function deal.findOne (req, res, next) {
  Deal.deal.findOne(req.swagger.params, res, next);
};

module.exports.deal.replaceById = function deal.replaceById (req, res, next) {
  Deal.deal.replaceById(req.swagger.params, res, next);
};

module.exports.deal.replaceOrCreate = function deal.replaceOrCreate (req, res, next) {
  Deal.deal.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.deal.upsertWithWhere = function deal.upsertWithWhere (req, res, next) {
  Deal.deal.upsertWithWhere(req.swagger.params, res, next);
};
