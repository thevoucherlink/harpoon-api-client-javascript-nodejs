'use strict';

var url = require('url');


var AwCollpurDeal = require('./AwCollpurDealService');


module.exports.awCollpurDeal.count = function awCollpurDeal.count (req, res, next) {
  AwCollpurDeal.awCollpurDeal.count(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.create = function awCollpurDeal.create (req, res, next) {
  AwCollpurDeal.awCollpurDeal.create(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.createChangeStream__get_AwCollpurDeals_changeStream = function awCollpurDeal.createChangeStream__get_AwCollpurDeals_changeStream (req, res, next) {
  AwCollpurDeal.awCollpurDeal.createChangeStream__get_AwCollpurDeals_changeStream(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.createChangeStream__post_AwCollpurDeals_changeStream = function awCollpurDeal.createChangeStream__post_AwCollpurDeals_changeStream (req, res, next) {
  AwCollpurDeal.awCollpurDeal.createChangeStream__post_AwCollpurDeals_changeStream(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.deleteById = function awCollpurDeal.deleteById (req, res, next) {
  AwCollpurDeal.awCollpurDeal.deleteById(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.exists__get_AwCollpurDeals_{id}_exists = function awCollpurDeal.exists__get_AwCollpurDeals_{id}_exists (req, res, next) {
  AwCollpurDeal.awCollpurDeal.exists__get_AwCollpurDeals_{id}_exists(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.exists__head_AwCollpurDeals_{id} = function awCollpurDeal.exists__head_AwCollpurDeals_{id} (req, res, next) {
  AwCollpurDeal.awCollpurDeal.exists__head_AwCollpurDeals_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.find = function awCollpurDeal.find (req, res, next) {
  AwCollpurDeal.awCollpurDeal.find(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.findById = function awCollpurDeal.findById (req, res, next) {
  AwCollpurDeal.awCollpurDeal.findById(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.findOne = function awCollpurDeal.findOne (req, res, next) {
  AwCollpurDeal.awCollpurDeal.findOne(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.__count__awCollpurDealPurchases = function awCollpurDeal.prototype.__count__awCollpurDealPurchases (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.__count__awCollpurDealPurchases(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.__create__awCollpurDealPurchases = function awCollpurDeal.prototype.__create__awCollpurDealPurchases (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.__create__awCollpurDealPurchases(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.__delete__awCollpurDealPurchases = function awCollpurDeal.prototype.__delete__awCollpurDealPurchases (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.__delete__awCollpurDealPurchases(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.__destroyById__awCollpurDealPurchases = function awCollpurDeal.prototype.__destroyById__awCollpurDealPurchases (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.__destroyById__awCollpurDealPurchases(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.__findById__awCollpurDealPurchases = function awCollpurDeal.prototype.__findById__awCollpurDealPurchases (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.__findById__awCollpurDealPurchases(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.__get__awCollpurDealPurchases = function awCollpurDeal.prototype.__get__awCollpurDealPurchases (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.__get__awCollpurDealPurchases(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.__updateById__awCollpurDealPurchases = function awCollpurDeal.prototype.__updateById__awCollpurDealPurchases (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.__updateById__awCollpurDealPurchases(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.updateAttributes__patch_AwCollpurDeals_{id} = function awCollpurDeal.prototype.updateAttributes__patch_AwCollpurDeals_{id} (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.updateAttributes__patch_AwCollpurDeals_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.prototype.updateAttributes__put_AwCollpurDeals_{id} = function awCollpurDeal.prototype.updateAttributes__put_AwCollpurDeals_{id} (req, res, next) {
  AwCollpurDeal.awCollpurDeal.prototype.updateAttributes__put_AwCollpurDeals_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.replaceById = function awCollpurDeal.replaceById (req, res, next) {
  AwCollpurDeal.awCollpurDeal.replaceById(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.replaceOrCreate = function awCollpurDeal.replaceOrCreate (req, res, next) {
  AwCollpurDeal.awCollpurDeal.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.updateAll = function awCollpurDeal.updateAll (req, res, next) {
  AwCollpurDeal.awCollpurDeal.updateAll(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.upsertWithWhere = function awCollpurDeal.upsertWithWhere (req, res, next) {
  AwCollpurDeal.awCollpurDeal.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.upsert__patch_AwCollpurDeals = function awCollpurDeal.upsert__patch_AwCollpurDeals (req, res, next) {
  AwCollpurDeal.awCollpurDeal.upsert__patch_AwCollpurDeals(req.swagger.params, res, next);
};

module.exports.awCollpurDeal.upsert__put_AwCollpurDeals = function awCollpurDeal.upsert__put_AwCollpurDeals (req, res, next) {
  AwCollpurDeal.awCollpurDeal.upsert__put_AwCollpurDeals(req.swagger.params, res, next);
};
