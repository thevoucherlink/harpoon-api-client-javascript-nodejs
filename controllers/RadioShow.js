'use strict';

var url = require('url');


var RadioShow = require('./RadioShowService');


module.exports.radioShow.count = function radioShow.count (req, res, next) {
  RadioShow.radioShow.count(req.swagger.params, res, next);
};

module.exports.radioShow.create = function radioShow.create (req, res, next) {
  RadioShow.radioShow.create(req.swagger.params, res, next);
};

module.exports.radioShow.createChangeStream__get_RadioShows_changeStream = function radioShow.createChangeStream__get_RadioShows_changeStream (req, res, next) {
  RadioShow.radioShow.createChangeStream__get_RadioShows_changeStream(req.swagger.params, res, next);
};

module.exports.radioShow.createChangeStream__post_RadioShows_changeStream = function radioShow.createChangeStream__post_RadioShows_changeStream (req, res, next) {
  RadioShow.radioShow.createChangeStream__post_RadioShows_changeStream(req.swagger.params, res, next);
};

module.exports.radioShow.deleteById = function radioShow.deleteById (req, res, next) {
  RadioShow.radioShow.deleteById(req.swagger.params, res, next);
};

module.exports.radioShow.exists__get_RadioShows_{id}_exists = function radioShow.exists__get_RadioShows_{id}_exists (req, res, next) {
  RadioShow.radioShow.exists__get_RadioShows_{id}_exists(req.swagger.params, res, next);
};

module.exports.radioShow.exists__head_RadioShows_{id} = function radioShow.exists__head_RadioShows_{id} (req, res, next) {
  RadioShow.radioShow.exists__head_RadioShows_{id}(req.swagger.params, res, next);
};

module.exports.radioShow.find = function radioShow.find (req, res, next) {
  RadioShow.radioShow.find(req.swagger.params, res, next);
};

module.exports.radioShow.findById = function radioShow.findById (req, res, next) {
  RadioShow.radioShow.findById(req.swagger.params, res, next);
};

module.exports.radioShow.findOne = function radioShow.findOne (req, res, next) {
  RadioShow.radioShow.findOne(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__count__radioPresenters = function radioShow.prototype.__count__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__count__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__count__radioShowTimes = function radioShow.prototype.__count__radioShowTimes (req, res, next) {
  RadioShow.radioShow.prototype.__count__radioShowTimes(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__create__radioPresenters = function radioShow.prototype.__create__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__create__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__create__radioShowTimes = function radioShow.prototype.__create__radioShowTimes (req, res, next) {
  RadioShow.radioShow.prototype.__create__radioShowTimes(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__delete__radioPresenters = function radioShow.prototype.__delete__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__delete__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__delete__radioShowTimes = function radioShow.prototype.__delete__radioShowTimes (req, res, next) {
  RadioShow.radioShow.prototype.__delete__radioShowTimes(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__destroyById__radioPresenters = function radioShow.prototype.__destroyById__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__destroyById__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__destroyById__radioShowTimes = function radioShow.prototype.__destroyById__radioShowTimes (req, res, next) {
  RadioShow.radioShow.prototype.__destroyById__radioShowTimes(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__exists__radioPresenters = function radioShow.prototype.__exists__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__exists__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__findById__radioPresenters = function radioShow.prototype.__findById__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__findById__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__findById__radioShowTimes = function radioShow.prototype.__findById__radioShowTimes (req, res, next) {
  RadioShow.radioShow.prototype.__findById__radioShowTimes(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__get__playlistItem = function radioShow.prototype.__get__playlistItem (req, res, next) {
  RadioShow.radioShow.prototype.__get__playlistItem(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__get__radioPresenters = function radioShow.prototype.__get__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__get__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__get__radioShowTimes = function radioShow.prototype.__get__radioShowTimes (req, res, next) {
  RadioShow.radioShow.prototype.__get__radioShowTimes(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__get__radioStream = function radioShow.prototype.__get__radioStream (req, res, next) {
  RadioShow.radioShow.prototype.__get__radioStream(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__link__radioPresenters = function radioShow.prototype.__link__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__link__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__unlink__radioPresenters = function radioShow.prototype.__unlink__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__unlink__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__updateById__radioPresenters = function radioShow.prototype.__updateById__radioPresenters (req, res, next) {
  RadioShow.radioShow.prototype.__updateById__radioPresenters(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.__updateById__radioShowTimes = function radioShow.prototype.__updateById__radioShowTimes (req, res, next) {
  RadioShow.radioShow.prototype.__updateById__radioShowTimes(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.updateAttributes__patch_RadioShows_{id} = function radioShow.prototype.updateAttributes__patch_RadioShows_{id} (req, res, next) {
  RadioShow.radioShow.prototype.updateAttributes__patch_RadioShows_{id}(req.swagger.params, res, next);
};

module.exports.radioShow.prototype.updateAttributes__put_RadioShows_{id} = function radioShow.prototype.updateAttributes__put_RadioShows_{id} (req, res, next) {
  RadioShow.radioShow.prototype.updateAttributes__put_RadioShows_{id}(req.swagger.params, res, next);
};

module.exports.radioShow.replaceById = function radioShow.replaceById (req, res, next) {
  RadioShow.radioShow.replaceById(req.swagger.params, res, next);
};

module.exports.radioShow.replaceOrCreate = function radioShow.replaceOrCreate (req, res, next) {
  RadioShow.radioShow.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.radioShow.updateAll = function radioShow.updateAll (req, res, next) {
  RadioShow.radioShow.updateAll(req.swagger.params, res, next);
};

module.exports.radioShow.uploadFile = function radioShow.uploadFile (req, res, next) {
  RadioShow.radioShow.uploadFile(req.swagger.params, res, next);
};

module.exports.radioShow.upsertWithWhere = function radioShow.upsertWithWhere (req, res, next) {
  RadioShow.radioShow.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.radioShow.upsert__patch_RadioShows = function radioShow.upsert__patch_RadioShows (req, res, next) {
  RadioShow.radioShow.upsert__patch_RadioShows(req.swagger.params, res, next);
};

module.exports.radioShow.upsert__put_RadioShows = function radioShow.upsert__put_RadioShows (req, res, next) {
  RadioShow.radioShow.upsert__put_RadioShows(req.swagger.params, res, next);
};
