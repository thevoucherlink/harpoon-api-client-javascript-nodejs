'use strict';

var url = require('url');


var RadioPresenterRadioShow = require('./RadioPresenterRadioShowService');


module.exports.radioPresenterRadioShow.count = function radioPresenterRadioShow.count (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.count(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.create = function radioPresenterRadioShow.create (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.create(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.createChangeStream__get_RadioPresenterRadioShows_changeStream = function radioPresenterRadioShow.createChangeStream__get_RadioPresenterRadioShows_changeStream (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.createChangeStream__get_RadioPresenterRadioShows_changeStream(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.createChangeStream__post_RadioPresenterRadioShows_changeStream = function radioPresenterRadioShow.createChangeStream__post_RadioPresenterRadioShows_changeStream (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.createChangeStream__post_RadioPresenterRadioShows_changeStream(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.deleteById = function radioPresenterRadioShow.deleteById (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.deleteById(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.exists__get_RadioPresenterRadioShows_{id}_exists = function radioPresenterRadioShow.exists__get_RadioPresenterRadioShows_{id}_exists (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.exists__get_RadioPresenterRadioShows_{id}_exists(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.exists__head_RadioPresenterRadioShows_{id} = function radioPresenterRadioShow.exists__head_RadioPresenterRadioShows_{id} (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.exists__head_RadioPresenterRadioShows_{id}(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.find = function radioPresenterRadioShow.find (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.find(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.findById = function radioPresenterRadioShow.findById (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.findById(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.findOne = function radioPresenterRadioShow.findOne (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.findOne(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.prototype.__get__radioPresenter = function radioPresenterRadioShow.prototype.__get__radioPresenter (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.prototype.__get__radioPresenter(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.prototype.__get__radioShow = function radioPresenterRadioShow.prototype.__get__radioShow (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.prototype.__get__radioShow(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.prototype.updateAttributes__patch_RadioPresenterRadioShows_{id} = function radioPresenterRadioShow.prototype.updateAttributes__patch_RadioPresenterRadioShows_{id} (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.prototype.updateAttributes__patch_RadioPresenterRadioShows_{id}(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.prototype.updateAttributes__put_RadioPresenterRadioShows_{id} = function radioPresenterRadioShow.prototype.updateAttributes__put_RadioPresenterRadioShows_{id} (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.prototype.updateAttributes__put_RadioPresenterRadioShows_{id}(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.replaceById = function radioPresenterRadioShow.replaceById (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.replaceById(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.replaceOrCreate = function radioPresenterRadioShow.replaceOrCreate (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.updateAll = function radioPresenterRadioShow.updateAll (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.updateAll(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.upsertWithWhere = function radioPresenterRadioShow.upsertWithWhere (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.upsert__patch_RadioPresenterRadioShows = function radioPresenterRadioShow.upsert__patch_RadioPresenterRadioShows (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.upsert__patch_RadioPresenterRadioShows(req.swagger.params, res, next);
};

module.exports.radioPresenterRadioShow.upsert__put_RadioPresenterRadioShows = function radioPresenterRadioShow.upsert__put_RadioPresenterRadioShows (req, res, next) {
  RadioPresenterRadioShow.radioPresenterRadioShow.upsert__put_RadioPresenterRadioShows(req.swagger.params, res, next);
};
