'use strict';

var url = require('url');


var PlayerTrack = require('./PlayerTrackService');


module.exports.playerTrack.count = function playerTrack.count (req, res, next) {
  PlayerTrack.playerTrack.count(req.swagger.params, res, next);
};

module.exports.playerTrack.create = function playerTrack.create (req, res, next) {
  PlayerTrack.playerTrack.create(req.swagger.params, res, next);
};

module.exports.playerTrack.createChangeStream__get_PlayerTracks_changeStream = function playerTrack.createChangeStream__get_PlayerTracks_changeStream (req, res, next) {
  PlayerTrack.playerTrack.createChangeStream__get_PlayerTracks_changeStream(req.swagger.params, res, next);
};

module.exports.playerTrack.createChangeStream__post_PlayerTracks_changeStream = function playerTrack.createChangeStream__post_PlayerTracks_changeStream (req, res, next) {
  PlayerTrack.playerTrack.createChangeStream__post_PlayerTracks_changeStream(req.swagger.params, res, next);
};

module.exports.playerTrack.deleteById = function playerTrack.deleteById (req, res, next) {
  PlayerTrack.playerTrack.deleteById(req.swagger.params, res, next);
};

module.exports.playerTrack.exists__get_PlayerTracks_{id}_exists = function playerTrack.exists__get_PlayerTracks_{id}_exists (req, res, next) {
  PlayerTrack.playerTrack.exists__get_PlayerTracks_{id}_exists(req.swagger.params, res, next);
};

module.exports.playerTrack.exists__head_PlayerTracks_{id} = function playerTrack.exists__head_PlayerTracks_{id} (req, res, next) {
  PlayerTrack.playerTrack.exists__head_PlayerTracks_{id}(req.swagger.params, res, next);
};

module.exports.playerTrack.find = function playerTrack.find (req, res, next) {
  PlayerTrack.playerTrack.find(req.swagger.params, res, next);
};

module.exports.playerTrack.findById = function playerTrack.findById (req, res, next) {
  PlayerTrack.playerTrack.findById(req.swagger.params, res, next);
};

module.exports.playerTrack.findOne = function playerTrack.findOne (req, res, next) {
  PlayerTrack.playerTrack.findOne(req.swagger.params, res, next);
};

module.exports.playerTrack.prototype.__get__playlistItem = function playerTrack.prototype.__get__playlistItem (req, res, next) {
  PlayerTrack.playerTrack.prototype.__get__playlistItem(req.swagger.params, res, next);
};

module.exports.playerTrack.prototype.updateAttributes__patch_PlayerTracks_{id} = function playerTrack.prototype.updateAttributes__patch_PlayerTracks_{id} (req, res, next) {
  PlayerTrack.playerTrack.prototype.updateAttributes__patch_PlayerTracks_{id}(req.swagger.params, res, next);
};

module.exports.playerTrack.prototype.updateAttributes__put_PlayerTracks_{id} = function playerTrack.prototype.updateAttributes__put_PlayerTracks_{id} (req, res, next) {
  PlayerTrack.playerTrack.prototype.updateAttributes__put_PlayerTracks_{id}(req.swagger.params, res, next);
};

module.exports.playerTrack.replaceById = function playerTrack.replaceById (req, res, next) {
  PlayerTrack.playerTrack.replaceById(req.swagger.params, res, next);
};

module.exports.playerTrack.replaceOrCreate = function playerTrack.replaceOrCreate (req, res, next) {
  PlayerTrack.playerTrack.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.playerTrack.updateAll = function playerTrack.updateAll (req, res, next) {
  PlayerTrack.playerTrack.updateAll(req.swagger.params, res, next);
};

module.exports.playerTrack.upsertWithWhere = function playerTrack.upsertWithWhere (req, res, next) {
  PlayerTrack.playerTrack.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.playerTrack.upsert__patch_PlayerTracks = function playerTrack.upsert__patch_PlayerTracks (req, res, next) {
  PlayerTrack.playerTrack.upsert__patch_PlayerTracks(req.swagger.params, res, next);
};

module.exports.playerTrack.upsert__put_PlayerTracks = function playerTrack.upsert__put_PlayerTracks (req, res, next) {
  PlayerTrack.playerTrack.upsert__put_PlayerTracks(req.swagger.params, res, next);
};
