'use strict';

var url = require('url');


var RadioStream = require('./RadioStreamService');


module.exports.radioStream.count = function radioStream.count (req, res, next) {
  RadioStream.radioStream.count(req.swagger.params, res, next);
};

module.exports.radioStream.create = function radioStream.create (req, res, next) {
  RadioStream.radioStream.create(req.swagger.params, res, next);
};

module.exports.radioStream.createChangeStream__get_RadioStreams_changeStream = function radioStream.createChangeStream__get_RadioStreams_changeStream (req, res, next) {
  RadioStream.radioStream.createChangeStream__get_RadioStreams_changeStream(req.swagger.params, res, next);
};

module.exports.radioStream.createChangeStream__post_RadioStreams_changeStream = function radioStream.createChangeStream__post_RadioStreams_changeStream (req, res, next) {
  RadioStream.radioStream.createChangeStream__post_RadioStreams_changeStream(req.swagger.params, res, next);
};

module.exports.radioStream.deleteById = function radioStream.deleteById (req, res, next) {
  RadioStream.radioStream.deleteById(req.swagger.params, res, next);
};

module.exports.radioStream.exists__get_RadioStreams_{id}_exists = function radioStream.exists__get_RadioStreams_{id}_exists (req, res, next) {
  RadioStream.radioStream.exists__get_RadioStreams_{id}_exists(req.swagger.params, res, next);
};

module.exports.radioStream.exists__head_RadioStreams_{id} = function radioStream.exists__head_RadioStreams_{id} (req, res, next) {
  RadioStream.radioStream.exists__head_RadioStreams_{id}(req.swagger.params, res, next);
};

module.exports.radioStream.find = function radioStream.find (req, res, next) {
  RadioStream.radioStream.find(req.swagger.params, res, next);
};

module.exports.radioStream.findById = function radioStream.findById (req, res, next) {
  RadioStream.radioStream.findById(req.swagger.params, res, next);
};

module.exports.radioStream.findOne = function radioStream.findOne (req, res, next) {
  RadioStream.radioStream.findOne(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.__count__radioShows = function radioStream.prototype.__count__radioShows (req, res, next) {
  RadioStream.radioStream.prototype.__count__radioShows(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.__create__radioShows = function radioStream.prototype.__create__radioShows (req, res, next) {
  RadioStream.radioStream.prototype.__create__radioShows(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.__delete__radioShows = function radioStream.prototype.__delete__radioShows (req, res, next) {
  RadioStream.radioStream.prototype.__delete__radioShows(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.__destroyById__radioShows = function radioStream.prototype.__destroyById__radioShows (req, res, next) {
  RadioStream.radioStream.prototype.__destroyById__radioShows(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.__findById__radioShows = function radioStream.prototype.__findById__radioShows (req, res, next) {
  RadioStream.radioStream.prototype.__findById__radioShows(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.__get__radioShows = function radioStream.prototype.__get__radioShows (req, res, next) {
  RadioStream.radioStream.prototype.__get__radioShows(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.__updateById__radioShows = function radioStream.prototype.__updateById__radioShows (req, res, next) {
  RadioStream.radioStream.prototype.__updateById__radioShows(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.updateAttributes__patch_RadioStreams_{id} = function radioStream.prototype.updateAttributes__patch_RadioStreams_{id} (req, res, next) {
  RadioStream.radioStream.prototype.updateAttributes__patch_RadioStreams_{id}(req.swagger.params, res, next);
};

module.exports.radioStream.prototype.updateAttributes__put_RadioStreams_{id} = function radioStream.prototype.updateAttributes__put_RadioStreams_{id} (req, res, next) {
  RadioStream.radioStream.prototype.updateAttributes__put_RadioStreams_{id}(req.swagger.params, res, next);
};

module.exports.radioStream.radioShowCurrent = function radioStream.radioShowCurrent (req, res, next) {
  RadioStream.radioStream.radioShowCurrent(req.swagger.params, res, next);
};

module.exports.radioStream.radioShowSchedule = function radioStream.radioShowSchedule (req, res, next) {
  RadioStream.radioStream.radioShowSchedule(req.swagger.params, res, next);
};

module.exports.radioStream.replaceById = function radioStream.replaceById (req, res, next) {
  RadioStream.radioStream.replaceById(req.swagger.params, res, next);
};

module.exports.radioStream.replaceOrCreate = function radioStream.replaceOrCreate (req, res, next) {
  RadioStream.radioStream.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.radioStream.updateAll = function radioStream.updateAll (req, res, next) {
  RadioStream.radioStream.updateAll(req.swagger.params, res, next);
};

module.exports.radioStream.upsertWithWhere = function radioStream.upsertWithWhere (req, res, next) {
  RadioStream.radioStream.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.radioStream.upsert__patch_RadioStreams = function radioStream.upsert__patch_RadioStreams (req, res, next) {
  RadioStream.radioStream.upsert__patch_RadioStreams(req.swagger.params, res, next);
};

module.exports.radioStream.upsert__put_RadioStreams = function radioStream.upsert__put_RadioStreams (req, res, next) {
  RadioStream.radioStream.upsert__put_RadioStreams(req.swagger.params, res, next);
};
