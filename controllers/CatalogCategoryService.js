'use strict';

exports.catalogCategory.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.createChangeStream__get_CatalogCategories_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.createChangeStream__post_CatalogCategories_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.exists__get_CatalogCategories_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.exists__head_CatalogCategories_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.__count__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.__create__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.__delete__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogCategory.prototype.__destroyById__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogCategory.prototype.__exists__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = true;
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.__findById__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.__get__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.__link__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (CatalogCategoryProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogCategory" : "{}",
  "categoryId" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.__unlink__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogCategory.prototype.__updateById__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.updateAttributes__patch_CatalogCategories_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.prototype.updateAttributes__put_CatalogCategories_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.upsert__patch_CatalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogCategory.upsert__put_CatalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

