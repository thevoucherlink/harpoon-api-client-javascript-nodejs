'use strict';

var url = require('url');


var Coupon = require('./CouponService');


module.exports.coupon.checkout = function coupon.checkout (req, res, next) {
  Coupon.coupon.checkout(req.swagger.params, res, next);
};

module.exports.coupon.find = function coupon.find (req, res, next) {
  Coupon.coupon.find(req.swagger.params, res, next);
};

module.exports.coupon.findById = function coupon.findById (req, res, next) {
  Coupon.coupon.findById(req.swagger.params, res, next);
};

module.exports.coupon.findOne = function coupon.findOne (req, res, next) {
  Coupon.coupon.findOne(req.swagger.params, res, next);
};

module.exports.coupon.redeem = function coupon.redeem (req, res, next) {
  Coupon.coupon.redeem(req.swagger.params, res, next);
};

module.exports.coupon.replaceById = function coupon.replaceById (req, res, next) {
  Coupon.coupon.replaceById(req.swagger.params, res, next);
};

module.exports.coupon.replaceOrCreate = function coupon.replaceOrCreate (req, res, next) {
  Coupon.coupon.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.coupon.upsertWithWhere = function coupon.upsertWithWhere (req, res, next) {
  Coupon.coupon.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.coupon.venues = function coupon.venues (req, res, next) {
  Coupon.coupon.venues(req.swagger.params, res, next);
};
