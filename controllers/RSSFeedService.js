'use strict';

exports.rssFeed.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.createChangeStream__get_RssFeeds_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.createChangeStream__post_RssFeeds_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.exists__get_RssFeeds_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.exists__head_RssFeeds_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.prototype.__get__rssFeedGroup = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.prototype.updateAttributes__patch_RssFeeds_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.prototype.updateAttributes__put_RssFeeds_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.upsert__patch_RssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeed.upsert__put_RssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

