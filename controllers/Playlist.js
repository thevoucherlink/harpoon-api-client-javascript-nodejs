'use strict';

var url = require('url');


var Playlist = require('./PlaylistService');


module.exports.playlist.count = function playlist.count (req, res, next) {
  Playlist.playlist.count(req.swagger.params, res, next);
};

module.exports.playlist.create = function playlist.create (req, res, next) {
  Playlist.playlist.create(req.swagger.params, res, next);
};

module.exports.playlist.createChangeStream__get_Playlists_changeStream = function playlist.createChangeStream__get_Playlists_changeStream (req, res, next) {
  Playlist.playlist.createChangeStream__get_Playlists_changeStream(req.swagger.params, res, next);
};

module.exports.playlist.createChangeStream__post_Playlists_changeStream = function playlist.createChangeStream__post_Playlists_changeStream (req, res, next) {
  Playlist.playlist.createChangeStream__post_Playlists_changeStream(req.swagger.params, res, next);
};

module.exports.playlist.deleteById = function playlist.deleteById (req, res, next) {
  Playlist.playlist.deleteById(req.swagger.params, res, next);
};

module.exports.playlist.exists__get_Playlists_{id}_exists = function playlist.exists__get_Playlists_{id}_exists (req, res, next) {
  Playlist.playlist.exists__get_Playlists_{id}_exists(req.swagger.params, res, next);
};

module.exports.playlist.exists__head_Playlists_{id} = function playlist.exists__head_Playlists_{id} (req, res, next) {
  Playlist.playlist.exists__head_Playlists_{id}(req.swagger.params, res, next);
};

module.exports.playlist.find = function playlist.find (req, res, next) {
  Playlist.playlist.find(req.swagger.params, res, next);
};

module.exports.playlist.findById = function playlist.findById (req, res, next) {
  Playlist.playlist.findById(req.swagger.params, res, next);
};

module.exports.playlist.findOne = function playlist.findOne (req, res, next) {
  Playlist.playlist.findOne(req.swagger.params, res, next);
};

module.exports.playlist.prototype.__count__playlistItems = function playlist.prototype.__count__playlistItems (req, res, next) {
  Playlist.playlist.prototype.__count__playlistItems(req.swagger.params, res, next);
};

module.exports.playlist.prototype.__create__playlistItems = function playlist.prototype.__create__playlistItems (req, res, next) {
  Playlist.playlist.prototype.__create__playlistItems(req.swagger.params, res, next);
};

module.exports.playlist.prototype.__delete__playlistItems = function playlist.prototype.__delete__playlistItems (req, res, next) {
  Playlist.playlist.prototype.__delete__playlistItems(req.swagger.params, res, next);
};

module.exports.playlist.prototype.__destroyById__playlistItems = function playlist.prototype.__destroyById__playlistItems (req, res, next) {
  Playlist.playlist.prototype.__destroyById__playlistItems(req.swagger.params, res, next);
};

module.exports.playlist.prototype.__findById__playlistItems = function playlist.prototype.__findById__playlistItems (req, res, next) {
  Playlist.playlist.prototype.__findById__playlistItems(req.swagger.params, res, next);
};

module.exports.playlist.prototype.__get__playlistItems = function playlist.prototype.__get__playlistItems (req, res, next) {
  Playlist.playlist.prototype.__get__playlistItems(req.swagger.params, res, next);
};

module.exports.playlist.prototype.__updateById__playlistItems = function playlist.prototype.__updateById__playlistItems (req, res, next) {
  Playlist.playlist.prototype.__updateById__playlistItems(req.swagger.params, res, next);
};

module.exports.playlist.prototype.updateAttributes__patch_Playlists_{id} = function playlist.prototype.updateAttributes__patch_Playlists_{id} (req, res, next) {
  Playlist.playlist.prototype.updateAttributes__patch_Playlists_{id}(req.swagger.params, res, next);
};

module.exports.playlist.prototype.updateAttributes__put_Playlists_{id} = function playlist.prototype.updateAttributes__put_Playlists_{id} (req, res, next) {
  Playlist.playlist.prototype.updateAttributes__put_Playlists_{id}(req.swagger.params, res, next);
};

module.exports.playlist.replaceById = function playlist.replaceById (req, res, next) {
  Playlist.playlist.replaceById(req.swagger.params, res, next);
};

module.exports.playlist.replaceOrCreate = function playlist.replaceOrCreate (req, res, next) {
  Playlist.playlist.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.playlist.updateAll = function playlist.updateAll (req, res, next) {
  Playlist.playlist.updateAll(req.swagger.params, res, next);
};

module.exports.playlist.upsertWithWhere = function playlist.upsertWithWhere (req, res, next) {
  Playlist.playlist.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.playlist.upsert__patch_Playlists = function playlist.upsert__patch_Playlists (req, res, next) {
  Playlist.playlist.upsert__patch_Playlists(req.swagger.params, res, next);
};

module.exports.playlist.upsert__put_Playlists = function playlist.upsert__put_Playlists (req, res, next) {
  Playlist.playlist.upsert__put_Playlists(req.swagger.params, res, next);
};
