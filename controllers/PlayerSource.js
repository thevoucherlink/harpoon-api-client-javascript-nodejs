'use strict';

var url = require('url');


var PlayerSource = require('./PlayerSourceService');


module.exports.playerSource.count = function playerSource.count (req, res, next) {
  PlayerSource.playerSource.count(req.swagger.params, res, next);
};

module.exports.playerSource.create = function playerSource.create (req, res, next) {
  PlayerSource.playerSource.create(req.swagger.params, res, next);
};

module.exports.playerSource.createChangeStream__get_PlayerSources_changeStream = function playerSource.createChangeStream__get_PlayerSources_changeStream (req, res, next) {
  PlayerSource.playerSource.createChangeStream__get_PlayerSources_changeStream(req.swagger.params, res, next);
};

module.exports.playerSource.createChangeStream__post_PlayerSources_changeStream = function playerSource.createChangeStream__post_PlayerSources_changeStream (req, res, next) {
  PlayerSource.playerSource.createChangeStream__post_PlayerSources_changeStream(req.swagger.params, res, next);
};

module.exports.playerSource.deleteById = function playerSource.deleteById (req, res, next) {
  PlayerSource.playerSource.deleteById(req.swagger.params, res, next);
};

module.exports.playerSource.exists__get_PlayerSources_{id}_exists = function playerSource.exists__get_PlayerSources_{id}_exists (req, res, next) {
  PlayerSource.playerSource.exists__get_PlayerSources_{id}_exists(req.swagger.params, res, next);
};

module.exports.playerSource.exists__head_PlayerSources_{id} = function playerSource.exists__head_PlayerSources_{id} (req, res, next) {
  PlayerSource.playerSource.exists__head_PlayerSources_{id}(req.swagger.params, res, next);
};

module.exports.playerSource.find = function playerSource.find (req, res, next) {
  PlayerSource.playerSource.find(req.swagger.params, res, next);
};

module.exports.playerSource.findById = function playerSource.findById (req, res, next) {
  PlayerSource.playerSource.findById(req.swagger.params, res, next);
};

module.exports.playerSource.findOne = function playerSource.findOne (req, res, next) {
  PlayerSource.playerSource.findOne(req.swagger.params, res, next);
};

module.exports.playerSource.prototype.__get__playlistItem = function playerSource.prototype.__get__playlistItem (req, res, next) {
  PlayerSource.playerSource.prototype.__get__playlistItem(req.swagger.params, res, next);
};

module.exports.playerSource.prototype.updateAttributes__patch_PlayerSources_{id} = function playerSource.prototype.updateAttributes__patch_PlayerSources_{id} (req, res, next) {
  PlayerSource.playerSource.prototype.updateAttributes__patch_PlayerSources_{id}(req.swagger.params, res, next);
};

module.exports.playerSource.prototype.updateAttributes__put_PlayerSources_{id} = function playerSource.prototype.updateAttributes__put_PlayerSources_{id} (req, res, next) {
  PlayerSource.playerSource.prototype.updateAttributes__put_PlayerSources_{id}(req.swagger.params, res, next);
};

module.exports.playerSource.replaceById = function playerSource.replaceById (req, res, next) {
  PlayerSource.playerSource.replaceById(req.swagger.params, res, next);
};

module.exports.playerSource.replaceOrCreate = function playerSource.replaceOrCreate (req, res, next) {
  PlayerSource.playerSource.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.playerSource.updateAll = function playerSource.updateAll (req, res, next) {
  PlayerSource.playerSource.updateAll(req.swagger.params, res, next);
};

module.exports.playerSource.upsertWithWhere = function playerSource.upsertWithWhere (req, res, next) {
  PlayerSource.playerSource.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.playerSource.upsert__patch_PlayerSources = function playerSource.upsert__patch_PlayerSources (req, res, next) {
  PlayerSource.playerSource.upsert__patch_PlayerSources(req.swagger.params, res, next);
};

module.exports.playerSource.upsert__put_PlayerSources = function playerSource.upsert__put_PlayerSources (req, res, next) {
  PlayerSource.playerSource.upsert__put_PlayerSources(req.swagger.params, res, next);
};
