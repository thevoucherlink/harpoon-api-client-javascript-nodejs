'use strict';

var url = require('url');


var StripeDiscount = require('./StripeDiscountService');


module.exports.stripeDiscount.count = function stripeDiscount.count (req, res, next) {
  StripeDiscount.stripeDiscount.count(req.swagger.params, res, next);
};

module.exports.stripeDiscount.create = function stripeDiscount.create (req, res, next) {
  StripeDiscount.stripeDiscount.create(req.swagger.params, res, next);
};

module.exports.stripeDiscount.createChangeStream__get_StripeDiscounts_changeStream = function stripeDiscount.createChangeStream__get_StripeDiscounts_changeStream (req, res, next) {
  StripeDiscount.stripeDiscount.createChangeStream__get_StripeDiscounts_changeStream(req.swagger.params, res, next);
};

module.exports.stripeDiscount.createChangeStream__post_StripeDiscounts_changeStream = function stripeDiscount.createChangeStream__post_StripeDiscounts_changeStream (req, res, next) {
  StripeDiscount.stripeDiscount.createChangeStream__post_StripeDiscounts_changeStream(req.swagger.params, res, next);
};

module.exports.stripeDiscount.deleteById = function stripeDiscount.deleteById (req, res, next) {
  StripeDiscount.stripeDiscount.deleteById(req.swagger.params, res, next);
};

module.exports.stripeDiscount.exists__get_StripeDiscounts_{id}_exists = function stripeDiscount.exists__get_StripeDiscounts_{id}_exists (req, res, next) {
  StripeDiscount.stripeDiscount.exists__get_StripeDiscounts_{id}_exists(req.swagger.params, res, next);
};

module.exports.stripeDiscount.exists__head_StripeDiscounts_{id} = function stripeDiscount.exists__head_StripeDiscounts_{id} (req, res, next) {
  StripeDiscount.stripeDiscount.exists__head_StripeDiscounts_{id}(req.swagger.params, res, next);
};

module.exports.stripeDiscount.find = function stripeDiscount.find (req, res, next) {
  StripeDiscount.stripeDiscount.find(req.swagger.params, res, next);
};

module.exports.stripeDiscount.findById = function stripeDiscount.findById (req, res, next) {
  StripeDiscount.stripeDiscount.findById(req.swagger.params, res, next);
};

module.exports.stripeDiscount.findOne = function stripeDiscount.findOne (req, res, next) {
  StripeDiscount.stripeDiscount.findOne(req.swagger.params, res, next);
};

module.exports.stripeDiscount.prototype.updateAttributes__patch_StripeDiscounts_{id} = function stripeDiscount.prototype.updateAttributes__patch_StripeDiscounts_{id} (req, res, next) {
  StripeDiscount.stripeDiscount.prototype.updateAttributes__patch_StripeDiscounts_{id}(req.swagger.params, res, next);
};

module.exports.stripeDiscount.prototype.updateAttributes__put_StripeDiscounts_{id} = function stripeDiscount.prototype.updateAttributes__put_StripeDiscounts_{id} (req, res, next) {
  StripeDiscount.stripeDiscount.prototype.updateAttributes__put_StripeDiscounts_{id}(req.swagger.params, res, next);
};

module.exports.stripeDiscount.replaceById = function stripeDiscount.replaceById (req, res, next) {
  StripeDiscount.stripeDiscount.replaceById(req.swagger.params, res, next);
};

module.exports.stripeDiscount.replaceOrCreate = function stripeDiscount.replaceOrCreate (req, res, next) {
  StripeDiscount.stripeDiscount.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.stripeDiscount.updateAll = function stripeDiscount.updateAll (req, res, next) {
  StripeDiscount.stripeDiscount.updateAll(req.swagger.params, res, next);
};

module.exports.stripeDiscount.upsertWithWhere = function stripeDiscount.upsertWithWhere (req, res, next) {
  StripeDiscount.stripeDiscount.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.stripeDiscount.upsert__patch_StripeDiscounts = function stripeDiscount.upsert__patch_StripeDiscounts (req, res, next) {
  StripeDiscount.stripeDiscount.upsert__patch_StripeDiscounts(req.swagger.params, res, next);
};

module.exports.stripeDiscount.upsert__put_StripeDiscounts = function stripeDiscount.upsert__put_StripeDiscounts (req, res, next) {
  StripeDiscount.stripeDiscount.upsert__put_StripeDiscounts(req.swagger.params, res, next);
};
