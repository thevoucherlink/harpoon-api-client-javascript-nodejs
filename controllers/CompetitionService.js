'use strict';

exports.competition.attendees = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.checkout = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CompetitionCheckoutData)
  **/
    var examples = {};
  examples['application/json'] = {
  "cartId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "url" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "competitionQuestion" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "earnMoreChances" : true,
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "hasJoined" : true,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "earnMoreChancesURL" : "aeiou",
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "competitionAnswer" : "aeiou",
  "isWinner" : true,
  "chances" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "competitionQuestion" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "earnMoreChances" : true,
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "hasJoined" : true,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "earnMoreChancesURL" : "aeiou",
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "competitionAnswer" : "aeiou",
  "isWinner" : true,
  "chances" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "competitionQuestion" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "earnMoreChances" : true,
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "hasJoined" : true,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "earnMoreChancesURL" : "aeiou",
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "competitionAnswer" : "aeiou",
  "isWinner" : true,
  "chances" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.redeem = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CompetitionRedeemData)
  **/
    var examples = {};
  examples['application/json'] = {
  "isAvailable" : true,
  "code" : "aeiou",
  "attendee" : {
    "lastName" : "aeiou",
    "profilePictureUpload" : "",
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "privacy" : {
      "activity" : "aeiou",
      "notificationBeacon" : true,
      "notificationPush" : true,
      "notificationLocation" : true
    },
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "badge" : {
      "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
      "all" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
      "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
      "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
      "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
      "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
      "competition" : 1.3579000000000001069366817318950779736042022705078125,
      "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
      "offer" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
      "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "event" : 1.3579000000000001069366817318950779736042022705078125,
      "brand" : 1.3579000000000001069366817318950779736042022705078125,
      "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
    },
    "password" : "aeiou",
    "dob" : "aeiou",
    "appId" : "aeiou",
    "connection" : "",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  },
  "ticketPrice" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "redemptionType" : "aeiou",
  "qrcode" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competition" : {
    "competitionQuestion" : "aeiou",
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "collectionNotes" : "aeiou",
    "description" : "aeiou",
    "shareLink" : "aeiou",
    "checkoutLink" : "aeiou",
    "type" : {
      "parent" : "",
      "children" : [ "" ],
      "hasChildren" : true,
      "isPrimary" : true,
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "productCount" : 1.3579000000000001069366817318950779736042022705078125
    },
    "earnMoreChances" : true,
    "nearestVenue" : {
      "country" : "aeiou",
      "address" : "aeiou",
      "city" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "coordinates" : {
        "latitude" : 1.3579000000000001069366817318950779736042022705078125,
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "longitude" : 1.3579000000000001069366817318950779736042022705078125
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "brand" : {
        "website" : "aeiou",
        "address" : "aeiou",
        "description" : "aeiou",
        "managerName" : "aeiou",
        "isFollowed" : true,
        "cover" : "aeiou",
        "phone" : "aeiou",
        "name" : "aeiou",
        "logo" : "aeiou",
        "categories" : [ "" ],
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
        "email" : "aeiou"
      },
      "email" : "aeiou"
    },
    "locationLink" : "aeiou",
    "altLink" : "aeiou",
    "baseCurrency" : "aeiou",
    "cover" : "aeiou",
    "alias" : "aeiou",
    "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "from" : "2000-01-23T04:56:07.000+00:00",
    "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "hasJoined" : true,
    "isFeatured" : true,
    "priceText" : "aeiou",
    "brand" : "",
    "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
    "earnMoreChancesURL" : "aeiou",
    "actionText" : "aeiou",
    "campaignType" : "",
    "termsConditions" : "aeiou",
    "redemptionType" : "aeiou",
    "attendees" : [ {
      "lastName" : "aeiou",
      "profilePictureUpload" : {
        "file" : "aeiou",
        "contentType" : "aeiou"
      },
      "coverUpload" : "",
      "metadata" : "{}",
      "gender" : "aeiou",
      "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
      "authorizationCode" : "aeiou",
      "joinedAt" : "2000-01-23T04:56:07.000+00:00",
      "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
      "isFollowed" : true,
      "cover" : "aeiou",
      "firstName" : "aeiou",
      "profilePicture" : "aeiou",
      "password" : "aeiou",
      "dob" : "aeiou",
      "connection" : {
        "twitter" : {
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "userId" : "aeiou"
        },
        "facebook" : {
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "userId" : "aeiou"
        },
        "id" : 1.3579000000000001069366817318950779736042022705078125
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "isFollower" : true,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    } ],
    "facebook" : {
      "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
      "node" : "aeiou",
      "updatedTime" : "aeiou",
      "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
      "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
      "place" : "{}",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "category" : "aeiou"
    },
    "competitionAnswer" : "aeiou",
    "isWinner" : true,
    "chances" : [ {
      "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
      "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
      "price" : 1.3579000000000001069366817318950779736042022705078125,
      "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    } ],
    "name" : "aeiou",
    "bannerText" : "aeiou",
    "topic" : "",
    "to" : "2000-01-23T04:56:07.000+00:00",
    "category" : "",
    "closestPurchase" : {
      "expiredAt" : "2000-01-23T04:56:07.000+00:00",
      "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou"
  },
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "expiredAt" : "2000-01-23T04:56:07.000+00:00",
  "winner" : {
    "name" : "aeiou",
    "position" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "chosenAt" : "2000-01-23T04:56:07.000+00:00"
  },
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Competition)
  **/
    var examples = {};
  examples['application/json'] = {
  "competitionQuestion" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "earnMoreChances" : true,
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "hasJoined" : true,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "earnMoreChancesURL" : "aeiou",
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "competitionAnswer" : "aeiou",
  "isWinner" : true,
  "chances" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Competition)
  **/
    var examples = {};
  examples['application/json'] = {
  "competitionQuestion" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "earnMoreChances" : true,
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "hasJoined" : true,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "earnMoreChancesURL" : "aeiou",
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "competitionAnswer" : "aeiou",
  "isWinner" : true,
  "chances" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (Competition)
  **/
    var examples = {};
  examples['application/json'] = {
  "competitionQuestion" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "earnMoreChances" : true,
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "hasJoined" : true,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "earnMoreChancesURL" : "aeiou",
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "competitionAnswer" : "aeiou",
  "isWinner" : true,
  "chances" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.validateAnswer = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * answer (CompetitionAnswerData)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.competition.venues = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "country" : "aeiou",
  "address" : "aeiou",
  "city" : "aeiou",
  "phone" : "aeiou",
  "name" : "aeiou",
  "coordinates" : {
    "latitude" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "longitude" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "brand" : {
    "website" : "aeiou",
    "address" : "aeiou",
    "description" : "aeiou",
    "managerName" : "aeiou",
    "isFollowed" : true,
    "cover" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "logo" : "aeiou",
    "categories" : [ {
      "parent" : "",
      "children" : [ "" ],
      "hasChildren" : true,
      "isPrimary" : true,
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "productCount" : 1.3579000000000001069366817318950779736042022705078125
    } ],
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  },
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

