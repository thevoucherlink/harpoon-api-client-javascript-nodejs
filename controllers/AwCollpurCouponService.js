'use strict';

exports.awCollpurCoupon.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.createChangeStream__get_AwCollpurCoupons_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.createChangeStream__post_AwCollpurCoupons_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.exists__get_AwCollpurCoupons_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.exists__head_AwCollpurCoupons_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.prototype.updateAttributes__patch_AwCollpurCoupons_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.prototype.updateAttributes__put_AwCollpurCoupons_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.upsert__patch_AwCollpurCoupons = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurCoupon.upsert__put_AwCollpurCoupons = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

