'use strict';

var url = require('url');


var UdropshipVendor = require('./UdropshipVendorService');


module.exports.udropshipVendor.count = function udropshipVendor.count (req, res, next) {
  UdropshipVendor.udropshipVendor.count(req.swagger.params, res, next);
};

module.exports.udropshipVendor.create = function udropshipVendor.create (req, res, next) {
  UdropshipVendor.udropshipVendor.create(req.swagger.params, res, next);
};

module.exports.udropshipVendor.createChangeStream__get_UdropshipVendors_changeStream = function udropshipVendor.createChangeStream__get_UdropshipVendors_changeStream (req, res, next) {
  UdropshipVendor.udropshipVendor.createChangeStream__get_UdropshipVendors_changeStream(req.swagger.params, res, next);
};

module.exports.udropshipVendor.createChangeStream__post_UdropshipVendors_changeStream = function udropshipVendor.createChangeStream__post_UdropshipVendors_changeStream (req, res, next) {
  UdropshipVendor.udropshipVendor.createChangeStream__post_UdropshipVendors_changeStream(req.swagger.params, res, next);
};

module.exports.udropshipVendor.deleteById = function udropshipVendor.deleteById (req, res, next) {
  UdropshipVendor.udropshipVendor.deleteById(req.swagger.params, res, next);
};

module.exports.udropshipVendor.exists__get_UdropshipVendors_{id}_exists = function udropshipVendor.exists__get_UdropshipVendors_{id}_exists (req, res, next) {
  UdropshipVendor.udropshipVendor.exists__get_UdropshipVendors_{id}_exists(req.swagger.params, res, next);
};

module.exports.udropshipVendor.exists__head_UdropshipVendors_{id} = function udropshipVendor.exists__head_UdropshipVendors_{id} (req, res, next) {
  UdropshipVendor.udropshipVendor.exists__head_UdropshipVendors_{id}(req.swagger.params, res, next);
};

module.exports.udropshipVendor.find = function udropshipVendor.find (req, res, next) {
  UdropshipVendor.udropshipVendor.find(req.swagger.params, res, next);
};

module.exports.udropshipVendor.findById = function udropshipVendor.findById (req, res, next) {
  UdropshipVendor.udropshipVendor.findById(req.swagger.params, res, next);
};

module.exports.udropshipVendor.findOne = function udropshipVendor.findOne (req, res, next) {
  UdropshipVendor.udropshipVendor.findOne(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__count__catalogProducts = function udropshipVendor.prototype.__count__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__count__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__count__harpoonHpublicApplicationpartners = function udropshipVendor.prototype.__count__harpoonHpublicApplicationpartners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__count__harpoonHpublicApplicationpartners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__count__harpoonHpublicv12VendorAppCategories = function udropshipVendor.prototype.__count__harpoonHpublicv12VendorAppCategories (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__count__harpoonHpublicv12VendorAppCategories(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__count__partners = function udropshipVendor.prototype.__count__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__count__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__count__udropshipVendorProducts = function udropshipVendor.prototype.__count__udropshipVendorProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__count__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__count__vendorLocations = function udropshipVendor.prototype.__count__vendorLocations (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__count__vendorLocations(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__create__catalogProducts = function udropshipVendor.prototype.__create__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__create__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__create__config = function udropshipVendor.prototype.__create__config (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__create__config(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__create__harpoonHpublicApplicationpartners = function udropshipVendor.prototype.__create__harpoonHpublicApplicationpartners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__create__harpoonHpublicApplicationpartners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__create__harpoonHpublicv12VendorAppCategories = function udropshipVendor.prototype.__create__harpoonHpublicv12VendorAppCategories (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__create__harpoonHpublicv12VendorAppCategories(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__create__partners = function udropshipVendor.prototype.__create__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__create__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__create__udropshipVendorProducts = function udropshipVendor.prototype.__create__udropshipVendorProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__create__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__create__vendorLocations = function udropshipVendor.prototype.__create__vendorLocations (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__create__vendorLocations(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__delete__catalogProducts = function udropshipVendor.prototype.__delete__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__delete__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__delete__harpoonHpublicApplicationpartners = function udropshipVendor.prototype.__delete__harpoonHpublicApplicationpartners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__delete__harpoonHpublicApplicationpartners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__delete__harpoonHpublicv12VendorAppCategories = function udropshipVendor.prototype.__delete__harpoonHpublicv12VendorAppCategories (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__delete__harpoonHpublicv12VendorAppCategories(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__delete__partners = function udropshipVendor.prototype.__delete__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__delete__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__delete__udropshipVendorProducts = function udropshipVendor.prototype.__delete__udropshipVendorProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__delete__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__delete__vendorLocations = function udropshipVendor.prototype.__delete__vendorLocations (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__delete__vendorLocations(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__destroyById__catalogProducts = function udropshipVendor.prototype.__destroyById__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__destroyById__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__destroyById__harpoonHpublicApplicationpartners = function udropshipVendor.prototype.__destroyById__harpoonHpublicApplicationpartners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__destroyById__harpoonHpublicApplicationpartners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__destroyById__harpoonHpublicv12VendorAppCategories = function udropshipVendor.prototype.__destroyById__harpoonHpublicv12VendorAppCategories (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__destroyById__harpoonHpublicv12VendorAppCategories(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__destroyById__partners = function udropshipVendor.prototype.__destroyById__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__destroyById__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__destroyById__udropshipVendorProducts = function udropshipVendor.prototype.__destroyById__udropshipVendorProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__destroyById__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__destroyById__vendorLocations = function udropshipVendor.prototype.__destroyById__vendorLocations (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__destroyById__vendorLocations(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__destroy__config = function udropshipVendor.prototype.__destroy__config (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__destroy__config(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__exists__catalogProducts = function udropshipVendor.prototype.__exists__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__exists__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__exists__partners = function udropshipVendor.prototype.__exists__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__exists__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__findById__catalogProducts = function udropshipVendor.prototype.__findById__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__findById__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__findById__harpoonHpublicApplicationpartners = function udropshipVendor.prototype.__findById__harpoonHpublicApplicationpartners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__findById__harpoonHpublicApplicationpartners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__findById__harpoonHpublicv12VendorAppCategories = function udropshipVendor.prototype.__findById__harpoonHpublicv12VendorAppCategories (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__findById__harpoonHpublicv12VendorAppCategories(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__findById__partners = function udropshipVendor.prototype.__findById__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__findById__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__findById__udropshipVendorProducts = function udropshipVendor.prototype.__findById__udropshipVendorProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__findById__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__findById__vendorLocations = function udropshipVendor.prototype.__findById__vendorLocations (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__findById__vendorLocations(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__get__catalogProducts = function udropshipVendor.prototype.__get__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__get__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__get__config = function udropshipVendor.prototype.__get__config (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__get__config(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__get__harpoonHpublicApplicationpartners = function udropshipVendor.prototype.__get__harpoonHpublicApplicationpartners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__get__harpoonHpublicApplicationpartners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__get__harpoonHpublicv12VendorAppCategories = function udropshipVendor.prototype.__get__harpoonHpublicv12VendorAppCategories (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__get__harpoonHpublicv12VendorAppCategories(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__get__partners = function udropshipVendor.prototype.__get__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__get__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__get__udropshipVendorProducts = function udropshipVendor.prototype.__get__udropshipVendorProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__get__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__get__vendorLocations = function udropshipVendor.prototype.__get__vendorLocations (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__get__vendorLocations(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__link__catalogProducts = function udropshipVendor.prototype.__link__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__link__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__link__partners = function udropshipVendor.prototype.__link__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__link__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__unlink__catalogProducts = function udropshipVendor.prototype.__unlink__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__unlink__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__unlink__partners = function udropshipVendor.prototype.__unlink__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__unlink__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__updateById__catalogProducts = function udropshipVendor.prototype.__updateById__catalogProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__updateById__catalogProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__updateById__harpoonHpublicApplicationpartners = function udropshipVendor.prototype.__updateById__harpoonHpublicApplicationpartners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__updateById__harpoonHpublicApplicationpartners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__updateById__harpoonHpublicv12VendorAppCategories = function udropshipVendor.prototype.__updateById__harpoonHpublicv12VendorAppCategories (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__updateById__harpoonHpublicv12VendorAppCategories(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__updateById__partners = function udropshipVendor.prototype.__updateById__partners (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__updateById__partners(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__updateById__udropshipVendorProducts = function udropshipVendor.prototype.__updateById__udropshipVendorProducts (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__updateById__udropshipVendorProducts(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__updateById__vendorLocations = function udropshipVendor.prototype.__updateById__vendorLocations (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__updateById__vendorLocations(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.__update__config = function udropshipVendor.prototype.__update__config (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.__update__config(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.updateAttributes__patch_UdropshipVendors_{id} = function udropshipVendor.prototype.updateAttributes__patch_UdropshipVendors_{id} (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.updateAttributes__patch_UdropshipVendors_{id}(req.swagger.params, res, next);
};

module.exports.udropshipVendor.prototype.updateAttributes__put_UdropshipVendors_{id} = function udropshipVendor.prototype.updateAttributes__put_UdropshipVendors_{id} (req, res, next) {
  UdropshipVendor.udropshipVendor.prototype.updateAttributes__put_UdropshipVendors_{id}(req.swagger.params, res, next);
};

module.exports.udropshipVendor.replaceById = function udropshipVendor.replaceById (req, res, next) {
  UdropshipVendor.udropshipVendor.replaceById(req.swagger.params, res, next);
};

module.exports.udropshipVendor.replaceOrCreate = function udropshipVendor.replaceOrCreate (req, res, next) {
  UdropshipVendor.udropshipVendor.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.udropshipVendor.updateAll = function udropshipVendor.updateAll (req, res, next) {
  UdropshipVendor.udropshipVendor.updateAll(req.swagger.params, res, next);
};

module.exports.udropshipVendor.upsertWithWhere = function udropshipVendor.upsertWithWhere (req, res, next) {
  UdropshipVendor.udropshipVendor.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.udropshipVendor.upsert__patch_UdropshipVendors = function udropshipVendor.upsert__patch_UdropshipVendors (req, res, next) {
  UdropshipVendor.udropshipVendor.upsert__patch_UdropshipVendors(req.swagger.params, res, next);
};

module.exports.udropshipVendor.upsert__put_UdropshipVendors = function udropshipVendor.upsert__put_UdropshipVendors (req, res, next) {
  UdropshipVendor.udropshipVendor.upsert__put_UdropshipVendors(req.swagger.params, res, next);
};
