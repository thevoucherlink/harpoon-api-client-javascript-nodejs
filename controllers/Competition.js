'use strict';

var url = require('url');


var Competition = require('./CompetitionService');


module.exports.competition.attendees = function competition.attendees (req, res, next) {
  Competition.competition.attendees(req.swagger.params, res, next);
};

module.exports.competition.checkout = function competition.checkout (req, res, next) {
  Competition.competition.checkout(req.swagger.params, res, next);
};

module.exports.competition.find = function competition.find (req, res, next) {
  Competition.competition.find(req.swagger.params, res, next);
};

module.exports.competition.findById = function competition.findById (req, res, next) {
  Competition.competition.findById(req.swagger.params, res, next);
};

module.exports.competition.findOne = function competition.findOne (req, res, next) {
  Competition.competition.findOne(req.swagger.params, res, next);
};

module.exports.competition.redeem = function competition.redeem (req, res, next) {
  Competition.competition.redeem(req.swagger.params, res, next);
};

module.exports.competition.replaceById = function competition.replaceById (req, res, next) {
  Competition.competition.replaceById(req.swagger.params, res, next);
};

module.exports.competition.replaceOrCreate = function competition.replaceOrCreate (req, res, next) {
  Competition.competition.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.competition.upsertWithWhere = function competition.upsertWithWhere (req, res, next) {
  Competition.competition.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.competition.validateAnswer = function competition.validateAnswer (req, res, next) {
  Competition.competition.validateAnswer(req.swagger.params, res, next);
};

module.exports.competition.venues = function competition.venues (req, res, next) {
  Competition.competition.venues(req.swagger.params, res, next);
};
