'use strict';

exports.awEventbookingTicket.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.createChangeStream__get_AwEventbookingTickets_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.createChangeStream__post_AwEventbookingTickets_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.exists__get_AwEventbookingTickets_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.exists__head_AwEventbookingTickets_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.prototype.updateAttributes__patch_AwEventbookingTickets_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.prototype.updateAttributes__put_AwEventbookingTickets_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.upsert__patch_AwEventbookingTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingTicket.upsert__put_AwEventbookingTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

