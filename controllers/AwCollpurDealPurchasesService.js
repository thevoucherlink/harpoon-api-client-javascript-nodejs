'use strict';

exports.awCollpurDealPurchases.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.createChangeStream__get_AwCollpurDealPurchases_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.createChangeStream__post_AwCollpurDealPurchases_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.exists__get_AwCollpurDealPurchases_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.exists__head_AwCollpurDealPurchases_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.prototype.__create__awCollpurCoupon = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.prototype.__destroy__awCollpurCoupon = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awCollpurDealPurchases.prototype.__get__awCollpurCoupon = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.prototype.__update__awCollpurCoupon = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "couponDeliveryDatetime" : "2000-01-23T04:56:07.000+00:00",
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseId" : 1.3579000000000001069366817318950779736042022705078125,
  "couponDateUpdated" : "2000-01-23T04:56:07.000+00:00",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "couponCode" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.prototype.updateAttributes__patch_AwCollpurDealPurchases_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.prototype.updateAttributes__put_AwCollpurDealPurchases_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.upsert__patch_AwCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDealPurchases.upsert__put_AwCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

