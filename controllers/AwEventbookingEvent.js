'use strict';

var url = require('url');


var AwEventbookingEvent = require('./AwEventbookingEventService');


module.exports.awEventbookingEvent.count = function awEventbookingEvent.count (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.count(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.create = function awEventbookingEvent.create (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.create(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.createChangeStream__get_AwEventbookingEvents_changeStream = function awEventbookingEvent.createChangeStream__get_AwEventbookingEvents_changeStream (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.createChangeStream__get_AwEventbookingEvents_changeStream(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.createChangeStream__post_AwEventbookingEvents_changeStream = function awEventbookingEvent.createChangeStream__post_AwEventbookingEvents_changeStream (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.createChangeStream__post_AwEventbookingEvents_changeStream(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.deleteById = function awEventbookingEvent.deleteById (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.deleteById(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.exists__get_AwEventbookingEvents_{id}_exists = function awEventbookingEvent.exists__get_AwEventbookingEvents_{id}_exists (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.exists__get_AwEventbookingEvents_{id}_exists(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.exists__head_AwEventbookingEvents_{id} = function awEventbookingEvent.exists__head_AwEventbookingEvents_{id} (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.exists__head_AwEventbookingEvents_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.find = function awEventbookingEvent.find (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.find(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.findById = function awEventbookingEvent.findById (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.findById(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.findOne = function awEventbookingEvent.findOne (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.findOne(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__count__attributes = function awEventbookingEvent.prototype.__count__attributes (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__count__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__count__purchasedTickets = function awEventbookingEvent.prototype.__count__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__count__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__count__tickets = function awEventbookingEvent.prototype.__count__tickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__count__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__create__attributes = function awEventbookingEvent.prototype.__create__attributes (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__create__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__create__purchasedTickets = function awEventbookingEvent.prototype.__create__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__create__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__create__tickets = function awEventbookingEvent.prototype.__create__tickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__create__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__delete__attributes = function awEventbookingEvent.prototype.__delete__attributes (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__delete__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__delete__purchasedTickets = function awEventbookingEvent.prototype.__delete__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__delete__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__delete__tickets = function awEventbookingEvent.prototype.__delete__tickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__delete__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__destroyById__attributes = function awEventbookingEvent.prototype.__destroyById__attributes (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__destroyById__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__destroyById__purchasedTickets = function awEventbookingEvent.prototype.__destroyById__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__destroyById__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__destroyById__tickets = function awEventbookingEvent.prototype.__destroyById__tickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__destroyById__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__exists__purchasedTickets = function awEventbookingEvent.prototype.__exists__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__exists__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__findById__attributes = function awEventbookingEvent.prototype.__findById__attributes (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__findById__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__findById__purchasedTickets = function awEventbookingEvent.prototype.__findById__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__findById__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__findById__tickets = function awEventbookingEvent.prototype.__findById__tickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__findById__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__get__attributes = function awEventbookingEvent.prototype.__get__attributes (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__get__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__get__purchasedTickets = function awEventbookingEvent.prototype.__get__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__get__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__get__tickets = function awEventbookingEvent.prototype.__get__tickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__get__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__link__purchasedTickets = function awEventbookingEvent.prototype.__link__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__link__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__unlink__purchasedTickets = function awEventbookingEvent.prototype.__unlink__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__unlink__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__updateById__attributes = function awEventbookingEvent.prototype.__updateById__attributes (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__updateById__attributes(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__updateById__purchasedTickets = function awEventbookingEvent.prototype.__updateById__purchasedTickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__updateById__purchasedTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.__updateById__tickets = function awEventbookingEvent.prototype.__updateById__tickets (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.__updateById__tickets(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.updateAttributes__patch_AwEventbookingEvents_{id} = function awEventbookingEvent.prototype.updateAttributes__patch_AwEventbookingEvents_{id} (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.updateAttributes__patch_AwEventbookingEvents_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.prototype.updateAttributes__put_AwEventbookingEvents_{id} = function awEventbookingEvent.prototype.updateAttributes__put_AwEventbookingEvents_{id} (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.prototype.updateAttributes__put_AwEventbookingEvents_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.replaceById = function awEventbookingEvent.replaceById (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.replaceById(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.replaceOrCreate = function awEventbookingEvent.replaceOrCreate (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.updateAll = function awEventbookingEvent.updateAll (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.updateAll(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.upsertWithWhere = function awEventbookingEvent.upsertWithWhere (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.upsert__patch_AwEventbookingEvents = function awEventbookingEvent.upsert__patch_AwEventbookingEvents (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.upsert__patch_AwEventbookingEvents(req.swagger.params, res, next);
};

module.exports.awEventbookingEvent.upsert__put_AwEventbookingEvents = function awEventbookingEvent.upsert__put_AwEventbookingEvents (req, res, next) {
  AwEventbookingEvent.awEventbookingEvent.upsert__put_AwEventbookingEvents(req.swagger.params, res, next);
};
