'use strict';

var url = require('url');


var Analytic = require('./AnalyticService');


module.exports.analytic.replaceById = function analytic.replaceById (req, res, next) {
  Analytic.analytic.replaceById(req.swagger.params, res, next);
};

module.exports.analytic.replaceOrCreate = function analytic.replaceOrCreate (req, res, next) {
  Analytic.analytic.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.analytic.track = function analytic.track (req, res, next) {
  Analytic.analytic.track(req.swagger.params, res, next);
};

module.exports.analytic.upsertWithWhere = function analytic.upsertWithWhere (req, res, next) {
  Analytic.analytic.upsertWithWhere(req.swagger.params, res, next);
};
