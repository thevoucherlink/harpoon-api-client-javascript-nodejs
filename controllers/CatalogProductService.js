'use strict';

exports.catalogProduct.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.createChangeStream__get_CatalogProducts_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.createChangeStream__post_CatalogProducts_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.exists__get_CatalogProducts_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.exists__head_CatalogProducts_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__count__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__count__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__count__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__create__awCollpurDeal = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__create__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__create__inventory = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogInventoryStockItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "manageStock" : 1.3579000000000001069366817318950779736042022705078125,
  "enableQtyIncrements" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigMaxSaleQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigNotifyStockQty" : 1.3579000000000001069366817318950779736042022705078125,
  "lowStockDate" : "2000-01-23T04:56:07.000+00:00",
  "notifyStockQty" : "aeiou",
  "qtyIncrements" : "aeiou",
  "useConfigMinSaleQty" : 1.3579000000000001069366817318950779736042022705078125,
  "minSaleQty" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "stockStatusChangedAuto" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "maxSaleQty" : "aeiou",
  "isQtyDecimal" : 1.3579000000000001069366817318950779736042022705078125,
  "isDecimalDivided" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigEnableQtyInc" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigManageStock" : 1.3579000000000001069366817318950779736042022705078125,
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "qty" : "aeiou",
  "useConfigQtyIncrements" : 1.3579000000000001069366817318950779736042022705078125,
  "stockId" : 1.3579000000000001069366817318950779736042022705078125,
  "isInStock" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigBackorders" : 1.3579000000000001069366817318950779736042022705078125,
  "minQty" : "aeiou",
  "useConfigMinQty" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__create__productEventCompetition = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__create__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (UdropshipVendorProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__create__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__delete__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__delete__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__delete__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__destroyById__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__destroyById__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__destroyById__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__destroy__awCollpurDeal = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__destroy__inventory = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__destroy__productEventCompetition = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__exists__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = true;
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__exists__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = true;
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__findById__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__findById__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__findById__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__get__awCollpurDeal = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__get__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__get__checkoutAgreement = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "isHtml" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "checkboxText" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "content" : "aeiou",
  "contentHeight" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__get__creator = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__get__inventory = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "manageStock" : 1.3579000000000001069366817318950779736042022705078125,
  "enableQtyIncrements" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigMaxSaleQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigNotifyStockQty" : 1.3579000000000001069366817318950779736042022705078125,
  "lowStockDate" : "2000-01-23T04:56:07.000+00:00",
  "notifyStockQty" : "aeiou",
  "qtyIncrements" : "aeiou",
  "useConfigMinSaleQty" : 1.3579000000000001069366817318950779736042022705078125,
  "minSaleQty" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "stockStatusChangedAuto" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "maxSaleQty" : "aeiou",
  "isQtyDecimal" : 1.3579000000000001069366817318950779736042022705078125,
  "isDecimalDivided" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigEnableQtyInc" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigManageStock" : 1.3579000000000001069366817318950779736042022705078125,
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "qty" : "aeiou",
  "useConfigQtyIncrements" : 1.3579000000000001069366817318950779736042022705078125,
  "stockId" : 1.3579000000000001069366817318950779736042022705078125,
  "isInStock" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigBackorders" : 1.3579000000000001069366817318950779736042022705078125,
  "minQty" : "aeiou",
  "useConfigMinQty" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__get__productEventCompetition = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__get__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__get__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__link__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (CatalogCategoryProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogCategory" : "{}",
  "categoryId" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__link__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (UdropshipVendorProductAssoc)
  **/
    var examples = {};
  examples['application/json'] = {
  "udropshipVendor" : "{}",
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isUdmulti" : 1.3579000000000001069366817318950779736042022705078125,
  "isAttribute" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__unlink__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__unlink__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.catalogProduct.prototype.__updateById__catalogCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (CatalogCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "customDesignTo" : "2000-01-23T04:56:07.000+00:00",
  "allChildren" : "aeiou",
  "description" : "aeiou",
  "ummDdWidth" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "metaDescription" : "aeiou",
  "isAnchor" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "path" : "aeiou",
  "metaKeywords" : "aeiou",
  "availableSortBy" : "aeiou",
  "children" : "aeiou",
  "pathInStore" : "aeiou",
  "filterPriceRange" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "ummCatLabel" : "aeiou",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "childrenCount" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdProportions" : "aeiou",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "pageLayout" : "aeiou",
  "level" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdColumns" : 1.3579000000000001069366817318950779736042022705078125,
  "landingPage" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdBlocks" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "urlPath" : "aeiou",
  "parentId" : 1.3579000000000001069366817318950779736042022705078125,
  "displayMode" : "aeiou",
  "customUseParentSettings" : 1.3579000000000001069366817318950779736042022705078125,
  "ummDdType" : "aeiou",
  "urlKey" : "aeiou",
  "defaultSortBy" : "aeiou",
  "ummCatTarget" : "aeiou",
  "customApplyToProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "metaTitle" : "aeiou",
  "customDesignFrom" : "2000-01-23T04:56:07.000+00:00",
  "customLayoutUpdate" : "aeiou",
  "name" : "aeiou",
  "includeInMenu" : 1.3579000000000001069366817318950779736042022705078125,
  "position" : 1.3579000000000001069366817318950779736042022705078125,
  "customDesign" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__updateById__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (UdropshipVendorProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__updateById__udropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__update__awCollpurDeal = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__update__inventory = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogInventoryStockItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "manageStock" : 1.3579000000000001069366817318950779736042022705078125,
  "enableQtyIncrements" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigMaxSaleQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigNotifyStockQty" : 1.3579000000000001069366817318950779736042022705078125,
  "lowStockDate" : "2000-01-23T04:56:07.000+00:00",
  "notifyStockQty" : "aeiou",
  "qtyIncrements" : "aeiou",
  "useConfigMinSaleQty" : 1.3579000000000001069366817318950779736042022705078125,
  "minSaleQty" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "stockStatusChangedAuto" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "maxSaleQty" : "aeiou",
  "isQtyDecimal" : 1.3579000000000001069366817318950779736042022705078125,
  "isDecimalDivided" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigEnableQtyInc" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigManageStock" : 1.3579000000000001069366817318950779736042022705078125,
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "qty" : "aeiou",
  "useConfigQtyIncrements" : 1.3579000000000001069366817318950779736042022705078125,
  "stockId" : 1.3579000000000001069366817318950779736042022705078125,
  "isInStock" : 1.3579000000000001069366817318950779736042022705078125,
  "useConfigBackorders" : 1.3579000000000001069366817318950779736042022705078125,
  "minQty" : "aeiou",
  "useConfigMinQty" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.__update__productEventCompetition = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.updateAttributes__patch_CatalogProducts_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.prototype.updateAttributes__put_CatalogProducts_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.upsert__patch_CatalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.catalogProduct.upsert__put_CatalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

