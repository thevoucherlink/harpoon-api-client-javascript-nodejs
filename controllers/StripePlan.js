'use strict';

var url = require('url');


var StripePlan = require('./StripePlanService');


module.exports.stripePlan.count = function stripePlan.count (req, res, next) {
  StripePlan.stripePlan.count(req.swagger.params, res, next);
};

module.exports.stripePlan.create = function stripePlan.create (req, res, next) {
  StripePlan.stripePlan.create(req.swagger.params, res, next);
};

module.exports.stripePlan.createChangeStream__get_StripePlans_changeStream = function stripePlan.createChangeStream__get_StripePlans_changeStream (req, res, next) {
  StripePlan.stripePlan.createChangeStream__get_StripePlans_changeStream(req.swagger.params, res, next);
};

module.exports.stripePlan.createChangeStream__post_StripePlans_changeStream = function stripePlan.createChangeStream__post_StripePlans_changeStream (req, res, next) {
  StripePlan.stripePlan.createChangeStream__post_StripePlans_changeStream(req.swagger.params, res, next);
};

module.exports.stripePlan.deleteById = function stripePlan.deleteById (req, res, next) {
  StripePlan.stripePlan.deleteById(req.swagger.params, res, next);
};

module.exports.stripePlan.exists__get_StripePlans_{id}_exists = function stripePlan.exists__get_StripePlans_{id}_exists (req, res, next) {
  StripePlan.stripePlan.exists__get_StripePlans_{id}_exists(req.swagger.params, res, next);
};

module.exports.stripePlan.exists__head_StripePlans_{id} = function stripePlan.exists__head_StripePlans_{id} (req, res, next) {
  StripePlan.stripePlan.exists__head_StripePlans_{id}(req.swagger.params, res, next);
};

module.exports.stripePlan.find = function stripePlan.find (req, res, next) {
  StripePlan.stripePlan.find(req.swagger.params, res, next);
};

module.exports.stripePlan.findById = function stripePlan.findById (req, res, next) {
  StripePlan.stripePlan.findById(req.swagger.params, res, next);
};

module.exports.stripePlan.findOne = function stripePlan.findOne (req, res, next) {
  StripePlan.stripePlan.findOne(req.swagger.params, res, next);
};

module.exports.stripePlan.prototype.updateAttributes__patch_StripePlans_{id} = function stripePlan.prototype.updateAttributes__patch_StripePlans_{id} (req, res, next) {
  StripePlan.stripePlan.prototype.updateAttributes__patch_StripePlans_{id}(req.swagger.params, res, next);
};

module.exports.stripePlan.prototype.updateAttributes__put_StripePlans_{id} = function stripePlan.prototype.updateAttributes__put_StripePlans_{id} (req, res, next) {
  StripePlan.stripePlan.prototype.updateAttributes__put_StripePlans_{id}(req.swagger.params, res, next);
};

module.exports.stripePlan.replaceById = function stripePlan.replaceById (req, res, next) {
  StripePlan.stripePlan.replaceById(req.swagger.params, res, next);
};

module.exports.stripePlan.replaceOrCreate = function stripePlan.replaceOrCreate (req, res, next) {
  StripePlan.stripePlan.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.stripePlan.updateAll = function stripePlan.updateAll (req, res, next) {
  StripePlan.stripePlan.updateAll(req.swagger.params, res, next);
};

module.exports.stripePlan.upsertWithWhere = function stripePlan.upsertWithWhere (req, res, next) {
  StripePlan.stripePlan.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.stripePlan.upsert__patch_StripePlans = function stripePlan.upsert__patch_StripePlans (req, res, next) {
  StripePlan.stripePlan.upsert__patch_StripePlans(req.swagger.params, res, next);
};

module.exports.stripePlan.upsert__put_StripePlans = function stripePlan.upsert__put_StripePlans (req, res, next) {
  StripePlan.stripePlan.upsert__put_StripePlans(req.swagger.params, res, next);
};
