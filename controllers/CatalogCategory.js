'use strict';

var url = require('url');


var CatalogCategory = require('./CatalogCategoryService');


module.exports.catalogCategory.count = function catalogCategory.count (req, res, next) {
  CatalogCategory.catalogCategory.count(req.swagger.params, res, next);
};

module.exports.catalogCategory.create = function catalogCategory.create (req, res, next) {
  CatalogCategory.catalogCategory.create(req.swagger.params, res, next);
};

module.exports.catalogCategory.createChangeStream__get_CatalogCategories_changeStream = function catalogCategory.createChangeStream__get_CatalogCategories_changeStream (req, res, next) {
  CatalogCategory.catalogCategory.createChangeStream__get_CatalogCategories_changeStream(req.swagger.params, res, next);
};

module.exports.catalogCategory.createChangeStream__post_CatalogCategories_changeStream = function catalogCategory.createChangeStream__post_CatalogCategories_changeStream (req, res, next) {
  CatalogCategory.catalogCategory.createChangeStream__post_CatalogCategories_changeStream(req.swagger.params, res, next);
};

module.exports.catalogCategory.deleteById = function catalogCategory.deleteById (req, res, next) {
  CatalogCategory.catalogCategory.deleteById(req.swagger.params, res, next);
};

module.exports.catalogCategory.exists__get_CatalogCategories_{id}_exists = function catalogCategory.exists__get_CatalogCategories_{id}_exists (req, res, next) {
  CatalogCategory.catalogCategory.exists__get_CatalogCategories_{id}_exists(req.swagger.params, res, next);
};

module.exports.catalogCategory.exists__head_CatalogCategories_{id} = function catalogCategory.exists__head_CatalogCategories_{id} (req, res, next) {
  CatalogCategory.catalogCategory.exists__head_CatalogCategories_{id}(req.swagger.params, res, next);
};

module.exports.catalogCategory.find = function catalogCategory.find (req, res, next) {
  CatalogCategory.catalogCategory.find(req.swagger.params, res, next);
};

module.exports.catalogCategory.findById = function catalogCategory.findById (req, res, next) {
  CatalogCategory.catalogCategory.findById(req.swagger.params, res, next);
};

module.exports.catalogCategory.findOne = function catalogCategory.findOne (req, res, next) {
  CatalogCategory.catalogCategory.findOne(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__count__catalogProducts = function catalogCategory.prototype.__count__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__count__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__create__catalogProducts = function catalogCategory.prototype.__create__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__create__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__delete__catalogProducts = function catalogCategory.prototype.__delete__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__delete__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__destroyById__catalogProducts = function catalogCategory.prototype.__destroyById__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__destroyById__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__exists__catalogProducts = function catalogCategory.prototype.__exists__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__exists__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__findById__catalogProducts = function catalogCategory.prototype.__findById__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__findById__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__get__catalogProducts = function catalogCategory.prototype.__get__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__get__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__link__catalogProducts = function catalogCategory.prototype.__link__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__link__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__unlink__catalogProducts = function catalogCategory.prototype.__unlink__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__unlink__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.__updateById__catalogProducts = function catalogCategory.prototype.__updateById__catalogProducts (req, res, next) {
  CatalogCategory.catalogCategory.prototype.__updateById__catalogProducts(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.updateAttributes__patch_CatalogCategories_{id} = function catalogCategory.prototype.updateAttributes__patch_CatalogCategories_{id} (req, res, next) {
  CatalogCategory.catalogCategory.prototype.updateAttributes__patch_CatalogCategories_{id}(req.swagger.params, res, next);
};

module.exports.catalogCategory.prototype.updateAttributes__put_CatalogCategories_{id} = function catalogCategory.prototype.updateAttributes__put_CatalogCategories_{id} (req, res, next) {
  CatalogCategory.catalogCategory.prototype.updateAttributes__put_CatalogCategories_{id}(req.swagger.params, res, next);
};

module.exports.catalogCategory.replaceById = function catalogCategory.replaceById (req, res, next) {
  CatalogCategory.catalogCategory.replaceById(req.swagger.params, res, next);
};

module.exports.catalogCategory.replaceOrCreate = function catalogCategory.replaceOrCreate (req, res, next) {
  CatalogCategory.catalogCategory.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.catalogCategory.updateAll = function catalogCategory.updateAll (req, res, next) {
  CatalogCategory.catalogCategory.updateAll(req.swagger.params, res, next);
};

module.exports.catalogCategory.upsertWithWhere = function catalogCategory.upsertWithWhere (req, res, next) {
  CatalogCategory.catalogCategory.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.catalogCategory.upsert__patch_CatalogCategories = function catalogCategory.upsert__patch_CatalogCategories (req, res, next) {
  CatalogCategory.catalogCategory.upsert__patch_CatalogCategories(req.swagger.params, res, next);
};

module.exports.catalogCategory.upsert__put_CatalogCategories = function catalogCategory.upsert__put_CatalogCategories (req, res, next) {
  CatalogCategory.catalogCategory.upsert__put_CatalogCategories(req.swagger.params, res, next);
};
