'use strict';

exports.playlistItem.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.createChangeStream__get_PlaylistItems_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.createChangeStream__post_PlaylistItems_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.exists__get_PlaylistItems_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.exists__head_PlaylistItems_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__count__playerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__count__playerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__count__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__create__playerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__create__playerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__create__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__delete__playerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.playlistItem.prototype.__delete__playerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.playlistItem.prototype.__delete__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.playlistItem.prototype.__destroyById__playerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.playlistItem.prototype.__destroyById__playerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.playlistItem.prototype.__destroyById__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.playlistItem.prototype.__findById__playerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__findById__playerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__findById__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__get__playerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__get__playerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__get__playlist = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__get__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__updateById__playerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__updateById__playerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.__updateById__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.updateAttributes__patch_PlaylistItems_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.prototype.updateAttributes__put_PlaylistItems_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.upsert__patch_PlaylistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlistItem.upsert__put_PlaylistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

