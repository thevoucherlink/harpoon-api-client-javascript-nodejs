'use strict';

exports.customer.activities = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "cover" : "aeiou",
  "privacyCode" : "aeiou",
  "related" : {
    "dealPaid" : 1.3579000000000001069366817318950779736042022705078125,
    "eventId" : 1.3579000000000001069366817318950779736042022705078125,
    "competitionId" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "brandId" : 1.3579000000000001069366817318950779736042022705078125,
    "customerId" : 1.3579000000000001069366817318950779736042022705078125,
    "couponId" : 1.3579000000000001069366817318950779736042022705078125
  },
  "postedAt" : "2000-01-23T04:56:07.000+00:00",
  "link" : "aeiou",
  "actionCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "message" : "aeiou",
  "customer" : {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "privacy" : {
      "activity" : "aeiou",
      "notificationBeacon" : true,
      "notificationPush" : true,
      "notificationLocation" : true
    },
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "badge" : {
      "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
      "all" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
      "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
      "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
      "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
      "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
      "competition" : 1.3579000000000001069366817318950779736042022705078125,
      "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
      "offer" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
      "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "event" : 1.3579000000000001069366817318950779736042022705078125,
      "brand" : 1.3579000000000001069366817318950779736042022705078125,
      "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
    },
    "password" : "aeiou",
    "dob" : "aeiou",
    "appId" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  }
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.badge = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
  "all" : 1.3579000000000001069366817318950779736042022705078125,
  "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
  "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
  "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
  "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "competition" : 1.3579000000000001069366817318950779736042022705078125,
  "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
  "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
  "offer" : 1.3579000000000001069366817318950779736042022705078125,
  "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
  "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "event" : 1.3579000000000001069366817318950779736042022705078125,
  "brand" : 1.3579000000000001069366817318950779736042022705078125,
  "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.cardCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CustomerCardCreateData)
  **/
    var examples = {};
  examples['application/json'] = {
  "exMonth" : "aeiou",
  "funding" : "aeiou",
  "cardholderName" : "aeiou",
  "name" : "aeiou",
  "lastDigits" : "aeiou",
  "id" : "aeiou",
  "type" : "aeiou",
  "exYear" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.cardDelete = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.cardFindById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exMonth" : "aeiou",
  "funding" : "aeiou",
  "cardholderName" : "aeiou",
  "name" : "aeiou",
  "lastDigits" : "aeiou",
  "id" : "aeiou",
  "type" : "aeiou",
  "exYear" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.cardUpdate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  * data (Card)
  **/
    var examples = {};
  examples['application/json'] = {
  "exMonth" : "aeiou",
  "funding" : "aeiou",
  "cardholderName" : "aeiou",
  "name" : "aeiou",
  "lastDigits" : "aeiou",
  "id" : "aeiou",
  "type" : "aeiou",
  "exYear" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.cards = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "exMonth" : "aeiou",
  "funding" : "aeiou",
  "cardholderName" : "aeiou",
  "name" : "aeiou",
  "lastDigits" : "aeiou",
  "id" : "aeiou",
  "type" : "aeiou",
  "exYear" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.competitionOrders = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "isAvailable" : true,
  "code" : "aeiou",
  "attendee" : {
    "lastName" : "aeiou",
    "profilePictureUpload" : "",
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "privacy" : {
      "activity" : "aeiou",
      "notificationBeacon" : true,
      "notificationPush" : true,
      "notificationLocation" : true
    },
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "badge" : {
      "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
      "all" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
      "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
      "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
      "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
      "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
      "competition" : 1.3579000000000001069366817318950779736042022705078125,
      "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
      "offer" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
      "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "event" : 1.3579000000000001069366817318950779736042022705078125,
      "brand" : 1.3579000000000001069366817318950779736042022705078125,
      "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
    },
    "password" : "aeiou",
    "dob" : "aeiou",
    "appId" : "aeiou",
    "connection" : "",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  },
  "ticketPrice" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "redemptionType" : "aeiou",
  "qrcode" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competition" : {
    "competitionQuestion" : "aeiou",
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "collectionNotes" : "aeiou",
    "description" : "aeiou",
    "shareLink" : "aeiou",
    "checkoutLink" : "aeiou",
    "type" : {
      "parent" : "",
      "children" : [ "" ],
      "hasChildren" : true,
      "isPrimary" : true,
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "productCount" : 1.3579000000000001069366817318950779736042022705078125
    },
    "earnMoreChances" : true,
    "nearestVenue" : {
      "country" : "aeiou",
      "address" : "aeiou",
      "city" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "coordinates" : {
        "latitude" : 1.3579000000000001069366817318950779736042022705078125,
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "longitude" : 1.3579000000000001069366817318950779736042022705078125
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "brand" : {
        "website" : "aeiou",
        "address" : "aeiou",
        "description" : "aeiou",
        "managerName" : "aeiou",
        "isFollowed" : true,
        "cover" : "aeiou",
        "phone" : "aeiou",
        "name" : "aeiou",
        "logo" : "aeiou",
        "categories" : [ "" ],
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
        "email" : "aeiou"
      },
      "email" : "aeiou"
    },
    "locationLink" : "aeiou",
    "altLink" : "aeiou",
    "baseCurrency" : "aeiou",
    "cover" : "aeiou",
    "alias" : "aeiou",
    "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "from" : "2000-01-23T04:56:07.000+00:00",
    "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "hasJoined" : true,
    "isFeatured" : true,
    "priceText" : "aeiou",
    "brand" : "",
    "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
    "earnMoreChancesURL" : "aeiou",
    "actionText" : "aeiou",
    "campaignType" : "",
    "termsConditions" : "aeiou",
    "redemptionType" : "aeiou",
    "attendees" : [ {
      "lastName" : "aeiou",
      "profilePictureUpload" : {
        "file" : "aeiou",
        "contentType" : "aeiou"
      },
      "coverUpload" : "",
      "metadata" : "{}",
      "gender" : "aeiou",
      "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
      "authorizationCode" : "aeiou",
      "joinedAt" : "2000-01-23T04:56:07.000+00:00",
      "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
      "isFollowed" : true,
      "cover" : "aeiou",
      "firstName" : "aeiou",
      "profilePicture" : "aeiou",
      "password" : "aeiou",
      "dob" : "aeiou",
      "connection" : {
        "twitter" : {
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "userId" : "aeiou"
        },
        "facebook" : {
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "userId" : "aeiou"
        },
        "id" : 1.3579000000000001069366817318950779736042022705078125
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "isFollower" : true,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    } ],
    "facebook" : {
      "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
      "node" : "aeiou",
      "updatedTime" : "aeiou",
      "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
      "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
      "place" : "{}",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "category" : "aeiou"
    },
    "competitionAnswer" : "aeiou",
    "isWinner" : true,
    "chances" : [ {
      "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
      "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
      "price" : 1.3579000000000001069366817318950779736042022705078125,
      "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    } ],
    "name" : "aeiou",
    "bannerText" : "aeiou",
    "topic" : "",
    "to" : "2000-01-23T04:56:07.000+00:00",
    "category" : "",
    "closestPurchase" : {
      "expiredAt" : "2000-01-23T04:56:07.000+00:00",
      "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou"
  },
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "expiredAt" : "2000-01-23T04:56:07.000+00:00",
  "winner" : {
    "name" : "aeiou",
    "position" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "chosenAt" : "2000-01-23T04:56:07.000+00:00"
  },
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.couponOrders = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "isAvailable" : true,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "expiredAt" : "2000-01-23T04:56:07.000+00:00",
  "code" : {
    "image" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "type" : "aeiou"
  },
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "redemptionType" : "aeiou",
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Customer)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.dealGroupOrders = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "isAvailable" : true,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "expiredAt" : "2000-01-23T04:56:07.000+00:00",
  "code" : {
    "image" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "type" : "aeiou"
  },
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "redemptionType" : "aeiou",
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.dealPaidOrders = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "isAvailable" : true,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "expiredAt" : "2000-01-23T04:56:07.000+00:00",
  "code" : {
    "image" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "type" : "aeiou"
  },
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "redemptionType" : "aeiou",
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.eventAttending = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "tickets" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "connectFacebookId" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "isGoing" : true,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.eventOrders = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "isAvailable" : true,
  "code" : "aeiou",
  "attendee" : {
    "lastName" : "aeiou",
    "profilePictureUpload" : "",
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "privacy" : {
      "activity" : "aeiou",
      "notificationBeacon" : true,
      "notificationPush" : true,
      "notificationLocation" : true
    },
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "badge" : {
      "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
      "all" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
      "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
      "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
      "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
      "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
      "competition" : 1.3579000000000001069366817318950779736042022705078125,
      "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
      "offer" : 1.3579000000000001069366817318950779736042022705078125,
      "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
      "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "event" : 1.3579000000000001069366817318950779736042022705078125,
      "brand" : 1.3579000000000001069366817318950779736042022705078125,
      "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
    },
    "password" : "aeiou",
    "dob" : "aeiou",
    "appId" : "aeiou",
    "connection" : "",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  },
  "ticketPrice" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "redemptionType" : "aeiou",
  "qrcode" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "expiredAt" : "2000-01-23T04:56:07.000+00:00",
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "event" : {
    "tickets" : [ {
      "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
      "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
      "price" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    } ],
    "collectionNotes" : "aeiou",
    "description" : "aeiou",
    "shareLink" : "aeiou",
    "connectFacebookId" : "aeiou",
    "checkoutLink" : "aeiou",
    "nearestVenue" : {
      "country" : "aeiou",
      "address" : "aeiou",
      "city" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "coordinates" : {
        "latitude" : 1.3579000000000001069366817318950779736042022705078125,
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "longitude" : 1.3579000000000001069366817318950779736042022705078125
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "brand" : {
        "website" : "aeiou",
        "address" : "aeiou",
        "description" : "aeiou",
        "managerName" : "aeiou",
        "isFollowed" : true,
        "cover" : "aeiou",
        "phone" : "aeiou",
        "name" : "aeiou",
        "logo" : "aeiou",
        "categories" : [ "" ],
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
        "email" : "aeiou"
      },
      "email" : "aeiou"
    },
    "locationLink" : "aeiou",
    "altLink" : "aeiou",
    "baseCurrency" : "aeiou",
    "cover" : "aeiou",
    "alias" : "aeiou",
    "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "from" : "2000-01-23T04:56:07.000+00:00",
    "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFeatured" : true,
    "priceText" : "aeiou",
    "brand" : "",
    "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
    "actionText" : "aeiou",
    "campaignType" : {
      "parent" : "",
      "children" : [ "" ],
      "hasChildren" : true,
      "isPrimary" : true,
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "productCount" : 1.3579000000000001069366817318950779736042022705078125
    },
    "termsConditions" : "aeiou",
    "redemptionType" : "aeiou",
    "attendees" : [ {
      "lastName" : "aeiou",
      "profilePictureUpload" : {
        "file" : "aeiou",
        "contentType" : "aeiou"
      },
      "coverUpload" : "",
      "metadata" : "{}",
      "gender" : "aeiou",
      "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
      "authorizationCode" : "aeiou",
      "joinedAt" : "2000-01-23T04:56:07.000+00:00",
      "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
      "isFollowed" : true,
      "cover" : "aeiou",
      "firstName" : "aeiou",
      "profilePicture" : "aeiou",
      "password" : "aeiou",
      "dob" : "aeiou",
      "connection" : {
        "twitter" : {
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "userId" : "aeiou"
        },
        "facebook" : {
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "userId" : "aeiou"
        },
        "id" : 1.3579000000000001069366817318950779736042022705078125
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "isFollower" : true,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    } ],
    "facebook" : {
      "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
      "node" : "aeiou",
      "updatedTime" : "aeiou",
      "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
      "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
      "place" : "{}",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "category" : "aeiou"
    },
    "isGoing" : true,
    "name" : "aeiou",
    "bannerText" : "aeiou",
    "topic" : "",
    "to" : "2000-01-23T04:56:07.000+00:00",
    "category" : "",
    "closestPurchase" : {
      "expiredAt" : "2000-01-23T04:56:07.000+00:00",
      "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou"
  },
  "status" : "aeiou",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.follow = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.followers = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "password" : "aeiou",
  "dob" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.followings = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "password" : "aeiou",
  "dob" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.login = function(args, res, next) {
  /**
   * parameters expected in the args:
  * credentials (CustomerLoginCredentials)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.notifications = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "cover" : "aeiou",
  "coverUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "related" : {
    "eventId" : 1.3579000000000001069366817318950779736042022705078125,
    "competitionId" : 1.3579000000000001069366817318950779736042022705078125,
    "brandId" : 1.3579000000000001069366817318950779736042022705078125,
    "venueId" : 1.3579000000000001069366817318950779736042022705078125,
    "customerId" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "couponId" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroupId" : 1.3579000000000001069366817318950779736042022705078125,
    "dealPaidId" : 1.3579000000000001069366817318950779736042022705078125
  },
  "link" : "aeiou",
  "from" : {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ {
        "parent" : "",
        "children" : [ "" ],
        "hasChildren" : true,
        "isPrimary" : true,
        "name" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "productCount" : 1.3579000000000001069366817318950779736042022705078125
      } ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "customer" : {
      "lastName" : "aeiou",
      "profilePictureUpload" : "",
      "coverUpload" : "",
      "metadata" : "{}",
      "gender" : "aeiou",
      "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
      "authorizationCode" : "aeiou",
      "privacy" : {
        "activity" : "aeiou",
        "notificationBeacon" : true,
        "notificationPush" : true,
        "notificationLocation" : true
      },
      "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
      "isFollowed" : true,
      "cover" : "aeiou",
      "firstName" : "aeiou",
      "profilePicture" : "aeiou",
      "badge" : {
        "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
        "all" : 1.3579000000000001069366817318950779736042022705078125,
        "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
        "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
        "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
        "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
        "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
        "competition" : 1.3579000000000001069366817318950779736042022705078125,
        "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
        "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
        "offer" : 1.3579000000000001069366817318950779736042022705078125,
        "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
        "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "event" : 1.3579000000000001069366817318950779736042022705078125,
        "brand" : 1.3579000000000001069366817318950779736042022705078125,
        "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
      },
      "password" : "aeiou",
      "dob" : "aeiou",
      "appId" : "aeiou",
      "connection" : {
        "twitter" : {
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "userId" : "aeiou"
        },
        "facebook" : {
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "userId" : "aeiou"
        },
        "id" : 1.3579000000000001069366817318950779736042022705078125
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "isFollower" : true,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    }
  },
  "actionCode" : "aeiou",
  "sentAt" : "2000-01-23T04:56:07.000+00:00",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "message" : "aeiou",
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.passwordResetAlt = function(args, res, next) {
  /**
   * parameters expected in the args:
  * credentials (CustomerPasswordResetCredentials)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.passwordUpdate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * credentials (CustomerPasswordUpdateCredentials)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.refreshToken = function(args, res, next) {
  /**
   * parameters expected in the args:
  * credentials (CustomerRefreshTokenCredentials)
  **/
    var examples = {};
  examples['application/json'] = {
  "expiresIn" : 1.3579000000000001069366817318950779736042022705078125,
  "macAlgorithm" : "aeiou",
  "macKey" : "aeiou",
  "scope" : "aeiou",
  "kid" : "aeiou",
  "accessToken" : "aeiou",
  "tokenType" : "aeiou",
  "refreshToken" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Customer)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Customer)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.tokenFromAuthorizationCode = function(args, res, next) {
  /**
   * parameters expected in the args:
  * credentials (CustomerTokenFromAuthorizationCodeCredentials)
  **/
    var examples = {};
  examples['application/json'] = {
  "expiresIn" : 1.3579000000000001069366817318950779736042022705078125,
  "macAlgorithm" : "aeiou",
  "macKey" : "aeiou",
  "scope" : "aeiou",
  "kid" : "aeiou",
  "accessToken" : "aeiou",
  "tokenType" : "aeiou",
  "refreshToken" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.unfollow = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * fk (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.update = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CustomerUpdateData)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (Customer)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.verify = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.walletCompetition = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "competitionQuestion" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "earnMoreChances" : true,
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "hasJoined" : true,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "earnMoreChancesURL" : "aeiou",
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "competitionAnswer" : "aeiou",
  "isWinner" : true,
  "chances" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.walletCoupon = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "price" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "hasClaimed" : true,
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyClaimed" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.walletDeal = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.walletDealGroup = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "price" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "hasClaimed" : true,
  "product" : {
    "cover" : "aeiou",
    "base_currency" : "aeiou",
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "description" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyClaimed" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.walletDealPaid = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "price" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "hasClaimed" : true,
  "product" : {
    "cover" : "aeiou",
    "base_currency" : "aeiou",
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "description" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyClaimed" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.walletEvent = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "tickets" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "connectFacebookId" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "isGoing" : true,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.customer.walletOffer = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

