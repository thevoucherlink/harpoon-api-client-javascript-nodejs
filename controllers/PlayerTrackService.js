'use strict';

exports.playerTrack.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.createChangeStream__get_PlayerTracks_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.createChangeStream__post_PlayerTracks_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.exists__get_PlayerTracks_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.exists__head_PlayerTracks_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.prototype.__get__playlistItem = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.prototype.updateAttributes__patch_PlayerTracks_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.prototype.updateAttributes__put_PlayerTracks_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.upsert__patch_PlayerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerTrack.upsert__put_PlayerTracks = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlayerTrack)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

