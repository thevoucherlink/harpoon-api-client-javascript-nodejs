'use strict';

exports.playlist.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.createChangeStream__get_Playlists_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.createChangeStream__post_Playlists_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.exists__get_Playlists_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.exists__head_Playlists_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.prototype.__count__playlistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.prototype.__create__playlistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.prototype.__delete__playlistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.playlist.prototype.__destroyById__playlistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.playlist.prototype.__findById__playlistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.prototype.__get__playlistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.prototype.__updateById__playlistItems = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (PlaylistItem)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.prototype.updateAttributes__patch_Playlists_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.prototype.updateAttributes__put_Playlists_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.upsert__patch_Playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playlist.upsert__put_Playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

