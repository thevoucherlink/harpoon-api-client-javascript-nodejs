'use strict';

exports.radioStream.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.createChangeStream__get_RadioStreams_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.createChangeStream__post_RadioStreams_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.exists__get_RadioStreams_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.exists__head_RadioStreams_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.prototype.__count__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.prototype.__create__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.prototype.__delete__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.radioStream.prototype.__destroyById__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.radioStream.prototype.__findById__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.prototype.__get__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.prototype.__updateById__radioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.prototype.updateAttributes__patch_RadioStreams_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.prototype.updateAttributes__put_RadioStreams_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.radioShowCurrent = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.radioShowSchedule = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.upsert__patch_RadioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioStream.upsert__put_RadioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

