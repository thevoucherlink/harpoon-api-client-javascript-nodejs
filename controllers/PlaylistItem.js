'use strict';

var url = require('url');


var PlaylistItem = require('./PlaylistItemService');


module.exports.playlistItem.count = function playlistItem.count (req, res, next) {
  PlaylistItem.playlistItem.count(req.swagger.params, res, next);
};

module.exports.playlistItem.create = function playlistItem.create (req, res, next) {
  PlaylistItem.playlistItem.create(req.swagger.params, res, next);
};

module.exports.playlistItem.createChangeStream__get_PlaylistItems_changeStream = function playlistItem.createChangeStream__get_PlaylistItems_changeStream (req, res, next) {
  PlaylistItem.playlistItem.createChangeStream__get_PlaylistItems_changeStream(req.swagger.params, res, next);
};

module.exports.playlistItem.createChangeStream__post_PlaylistItems_changeStream = function playlistItem.createChangeStream__post_PlaylistItems_changeStream (req, res, next) {
  PlaylistItem.playlistItem.createChangeStream__post_PlaylistItems_changeStream(req.swagger.params, res, next);
};

module.exports.playlistItem.deleteById = function playlistItem.deleteById (req, res, next) {
  PlaylistItem.playlistItem.deleteById(req.swagger.params, res, next);
};

module.exports.playlistItem.exists__get_PlaylistItems_{id}_exists = function playlistItem.exists__get_PlaylistItems_{id}_exists (req, res, next) {
  PlaylistItem.playlistItem.exists__get_PlaylistItems_{id}_exists(req.swagger.params, res, next);
};

module.exports.playlistItem.exists__head_PlaylistItems_{id} = function playlistItem.exists__head_PlaylistItems_{id} (req, res, next) {
  PlaylistItem.playlistItem.exists__head_PlaylistItems_{id}(req.swagger.params, res, next);
};

module.exports.playlistItem.find = function playlistItem.find (req, res, next) {
  PlaylistItem.playlistItem.find(req.swagger.params, res, next);
};

module.exports.playlistItem.findById = function playlistItem.findById (req, res, next) {
  PlaylistItem.playlistItem.findById(req.swagger.params, res, next);
};

module.exports.playlistItem.findOne = function playlistItem.findOne (req, res, next) {
  PlaylistItem.playlistItem.findOne(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__count__playerSources = function playlistItem.prototype.__count__playerSources (req, res, next) {
  PlaylistItem.playlistItem.prototype.__count__playerSources(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__count__playerTracks = function playlistItem.prototype.__count__playerTracks (req, res, next) {
  PlaylistItem.playlistItem.prototype.__count__playerTracks(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__count__radioShows = function playlistItem.prototype.__count__radioShows (req, res, next) {
  PlaylistItem.playlistItem.prototype.__count__radioShows(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__create__playerSources = function playlistItem.prototype.__create__playerSources (req, res, next) {
  PlaylistItem.playlistItem.prototype.__create__playerSources(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__create__playerTracks = function playlistItem.prototype.__create__playerTracks (req, res, next) {
  PlaylistItem.playlistItem.prototype.__create__playerTracks(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__create__radioShows = function playlistItem.prototype.__create__radioShows (req, res, next) {
  PlaylistItem.playlistItem.prototype.__create__radioShows(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__delete__playerSources = function playlistItem.prototype.__delete__playerSources (req, res, next) {
  PlaylistItem.playlistItem.prototype.__delete__playerSources(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__delete__playerTracks = function playlistItem.prototype.__delete__playerTracks (req, res, next) {
  PlaylistItem.playlistItem.prototype.__delete__playerTracks(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__delete__radioShows = function playlistItem.prototype.__delete__radioShows (req, res, next) {
  PlaylistItem.playlistItem.prototype.__delete__radioShows(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__destroyById__playerSources = function playlistItem.prototype.__destroyById__playerSources (req, res, next) {
  PlaylistItem.playlistItem.prototype.__destroyById__playerSources(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__destroyById__playerTracks = function playlistItem.prototype.__destroyById__playerTracks (req, res, next) {
  PlaylistItem.playlistItem.prototype.__destroyById__playerTracks(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__destroyById__radioShows = function playlistItem.prototype.__destroyById__radioShows (req, res, next) {
  PlaylistItem.playlistItem.prototype.__destroyById__radioShows(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__findById__playerSources = function playlistItem.prototype.__findById__playerSources (req, res, next) {
  PlaylistItem.playlistItem.prototype.__findById__playerSources(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__findById__playerTracks = function playlistItem.prototype.__findById__playerTracks (req, res, next) {
  PlaylistItem.playlistItem.prototype.__findById__playerTracks(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__findById__radioShows = function playlistItem.prototype.__findById__radioShows (req, res, next) {
  PlaylistItem.playlistItem.prototype.__findById__radioShows(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__get__playerSources = function playlistItem.prototype.__get__playerSources (req, res, next) {
  PlaylistItem.playlistItem.prototype.__get__playerSources(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__get__playerTracks = function playlistItem.prototype.__get__playerTracks (req, res, next) {
  PlaylistItem.playlistItem.prototype.__get__playerTracks(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__get__playlist = function playlistItem.prototype.__get__playlist (req, res, next) {
  PlaylistItem.playlistItem.prototype.__get__playlist(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__get__radioShows = function playlistItem.prototype.__get__radioShows (req, res, next) {
  PlaylistItem.playlistItem.prototype.__get__radioShows(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__updateById__playerSources = function playlistItem.prototype.__updateById__playerSources (req, res, next) {
  PlaylistItem.playlistItem.prototype.__updateById__playerSources(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__updateById__playerTracks = function playlistItem.prototype.__updateById__playerTracks (req, res, next) {
  PlaylistItem.playlistItem.prototype.__updateById__playerTracks(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.__updateById__radioShows = function playlistItem.prototype.__updateById__radioShows (req, res, next) {
  PlaylistItem.playlistItem.prototype.__updateById__radioShows(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.updateAttributes__patch_PlaylistItems_{id} = function playlistItem.prototype.updateAttributes__patch_PlaylistItems_{id} (req, res, next) {
  PlaylistItem.playlistItem.prototype.updateAttributes__patch_PlaylistItems_{id}(req.swagger.params, res, next);
};

module.exports.playlistItem.prototype.updateAttributes__put_PlaylistItems_{id} = function playlistItem.prototype.updateAttributes__put_PlaylistItems_{id} (req, res, next) {
  PlaylistItem.playlistItem.prototype.updateAttributes__put_PlaylistItems_{id}(req.swagger.params, res, next);
};

module.exports.playlistItem.replaceById = function playlistItem.replaceById (req, res, next) {
  PlaylistItem.playlistItem.replaceById(req.swagger.params, res, next);
};

module.exports.playlistItem.replaceOrCreate = function playlistItem.replaceOrCreate (req, res, next) {
  PlaylistItem.playlistItem.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.playlistItem.updateAll = function playlistItem.updateAll (req, res, next) {
  PlaylistItem.playlistItem.updateAll(req.swagger.params, res, next);
};

module.exports.playlistItem.upsertWithWhere = function playlistItem.upsertWithWhere (req, res, next) {
  PlaylistItem.playlistItem.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.playlistItem.upsert__patch_PlaylistItems = function playlistItem.upsert__patch_PlaylistItems (req, res, next) {
  PlaylistItem.playlistItem.upsert__patch_PlaylistItems(req.swagger.params, res, next);
};

module.exports.playlistItem.upsert__put_PlaylistItems = function playlistItem.upsert__put_PlaylistItems (req, res, next) {
  PlaylistItem.playlistItem.upsert__put_PlaylistItems(req.swagger.params, res, next);
};
