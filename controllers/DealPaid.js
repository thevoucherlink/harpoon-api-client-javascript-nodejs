'use strict';

var url = require('url');


var DealPaid = require('./DealPaidService');


module.exports.dealPaid.checkout = function dealPaid.checkout (req, res, next) {
  DealPaid.dealPaid.checkout(req.swagger.params, res, next);
};

module.exports.dealPaid.find = function dealPaid.find (req, res, next) {
  DealPaid.dealPaid.find(req.swagger.params, res, next);
};

module.exports.dealPaid.findById = function dealPaid.findById (req, res, next) {
  DealPaid.dealPaid.findById(req.swagger.params, res, next);
};

module.exports.dealPaid.findOne = function dealPaid.findOne (req, res, next) {
  DealPaid.dealPaid.findOne(req.swagger.params, res, next);
};

module.exports.dealPaid.redeem = function dealPaid.redeem (req, res, next) {
  DealPaid.dealPaid.redeem(req.swagger.params, res, next);
};

module.exports.dealPaid.replaceById = function dealPaid.replaceById (req, res, next) {
  DealPaid.dealPaid.replaceById(req.swagger.params, res, next);
};

module.exports.dealPaid.replaceOrCreate = function dealPaid.replaceOrCreate (req, res, next) {
  DealPaid.dealPaid.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.dealPaid.upsertWithWhere = function dealPaid.upsertWithWhere (req, res, next) {
  DealPaid.dealPaid.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.dealPaid.venues = function dealPaid.venues (req, res, next) {
  DealPaid.dealPaid.venues(req.swagger.params, res, next);
};
