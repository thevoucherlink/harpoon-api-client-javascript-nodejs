'use strict';

exports.analytic.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Analytic)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.analytic.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Analytic)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.analytic.track = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AnalyticRequest)
  **/
    var examples = {};
  examples['application/json'] = {
  "app" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.analytic.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (Analytic)
  **/
    var examples = {};
  examples['application/json'] = {
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

