'use strict';

exports.udropshipVendor.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.createChangeStream__get_UdropshipVendors_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.createChangeStream__post_UdropshipVendors_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.exists__get_UdropshipVendors_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.exists__head_UdropshipVendors_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__count__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__count__harpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__count__harpoonHpublicv12VendorAppCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__count__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__count__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__count__vendorLocations = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__create__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__create__config = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (HarpoonHpublicVendorconfig)
  **/
    var examples = {};
  examples['application/json'] = {
  "facebookFeedUpdatedAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookEventCount" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookEventUpdatedAt" : "2000-01-23T04:56:07.000+00:00",
  "stripeAccessToken" : "aeiou",
  "stripePublishableKey" : "aeiou",
  "type" : 1.3579000000000001069366817318950779736042022705078125,
  "stripeUserId" : "aeiou",
  "facebookFeedCount" : 1.3579000000000001069366817318950779736042022705078125,
  "cover" : "aeiou",
  "stripeRefreshToken" : "aeiou",
  "vendorconfigId" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookPageId" : "aeiou",
  "facebookEventEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "feesEventTicket" : "aeiou",
  "facebookPageToken" : "aeiou",
  "logo" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou",
  "facebookFeedEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__create__harpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__create__harpoonHpublicv12VendorAppCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (HarpoonHpublicv12VendorAppCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "isPrimary" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "categoryId" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__create__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__create__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (UdropshipVendorProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__create__vendorLocations = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (HarpoonHpublicBrandvenue)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "country" : "aeiou",
  "address" : "aeiou",
  "phone" : "aeiou",
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "latitude" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "longitude" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__delete__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__delete__harpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__delete__harpoonHpublicv12VendorAppCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__delete__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__delete__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__delete__vendorLocations = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__destroyById__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__destroyById__harpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__destroyById__harpoonHpublicv12VendorAppCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__destroyById__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__destroyById__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__destroyById__vendorLocations = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__destroy__config = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__exists__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = true;
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__exists__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = true;
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__findById__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__findById__harpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__findById__harpoonHpublicv12VendorAppCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "isPrimary" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "categoryId" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__findById__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__findById__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__findById__vendorLocations = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "country" : "aeiou",
  "address" : "aeiou",
  "phone" : "aeiou",
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "latitude" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "longitude" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__get__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__get__config = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "facebookFeedUpdatedAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookEventCount" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookEventUpdatedAt" : "2000-01-23T04:56:07.000+00:00",
  "stripeAccessToken" : "aeiou",
  "stripePublishableKey" : "aeiou",
  "type" : 1.3579000000000001069366817318950779736042022705078125,
  "stripeUserId" : "aeiou",
  "facebookFeedCount" : 1.3579000000000001069366817318950779736042022705078125,
  "cover" : "aeiou",
  "stripeRefreshToken" : "aeiou",
  "vendorconfigId" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookPageId" : "aeiou",
  "facebookEventEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "feesEventTicket" : "aeiou",
  "facebookPageToken" : "aeiou",
  "logo" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou",
  "facebookFeedEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__get__harpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__get__harpoonHpublicv12VendorAppCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "isPrimary" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "categoryId" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__get__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__get__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__get__vendorLocations = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "country" : "aeiou",
  "address" : "aeiou",
  "phone" : "aeiou",
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "latitude" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "longitude" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__link__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (UdropshipVendorProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__link__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (UdropshipVendorPartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__unlink__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__unlink__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.udropshipVendor.prototype.__updateById__catalogProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (CatalogProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "venueList" : "aeiou",
  "pushwooshTokens" : "aeiou",
  "awCollpurDeal" : "{}",
  "linksTitle" : "aeiou",
  "udtiershipUseCustom" : 1.3579000000000001069366817318950779736042022705078125,
  "connectFacebookId" : "aeiou",
  "recurringProfile" : "aeiou",
  "hasOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "smallImageLabel" : "aeiou",
  "price" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou",
  "passedValidation" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutAgreement" : "{}",
  "campaignType" : "aeiou",
  "campaignId" : "aeiou",
  "facebookId" : "aeiou",
  "competitionAnswer" : "aeiou",
  "facebookPlace" : "aeiou",
  "udropshipVendors" : [ "{}" ],
  "couponCodeText" : "aeiou",
  "udropshipVendor" : 1.3579000000000001069366817318950779736042022705078125,
  "typeId" : "aeiou",
  "competitionMultijoinReset" : "2000-01-23T04:56:07.000+00:00",
  "couponCodeType" : "aeiou",
  "productEventCompetition" : "{}",
  "udropshipCalculateRates" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendorProducts" : [ "{}" ],
  "newsToDate" : "2000-01-23T04:56:07.000+00:00",
  "notificationSilent" : 1.3579000000000001069366817318950779736042022705078125,
  "checkoutLink" : "aeiou",
  "udropshipVendorValue" : "aeiou",
  "altLink" : "aeiou",
  "shipmentType" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestionValidate" : true,
  "taxClassId" : 1.3579000000000001069366817318950779736042022705078125,
  "campaignLiveFrom" : "2000-01-23T04:56:07.000+00:00",
  "msrp" : "aeiou",
  "validationReport" : "aeiou",
  "competitionMultijoinStatus" : true,
  "priceText" : "aeiou",
  "thumbnailLabel" : "aeiou",
  "notificationInvisible" : 1.3579000000000001069366817318950779736042022705078125,
  "creator" : "{}",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "priceType" : 1.3579000000000001069366817318950779736042022705078125,
  "isRecurring" : 1.3579000000000001069366817318950779736042022705078125,
  "skuType" : 1.3579000000000001069366817318950779736042022705078125,
  "udtiershipRates" : "aeiou",
  "facebookAttendingCount" : "aeiou",
  "urlKey" : "aeiou",
  "linksPurchasedSeparately" : 1.3579000000000001069366817318950779736042022705078125,
  "pushwooshIds" : "aeiou",
  "specialFromDate" : "2000-01-23T04:56:07.000+00:00",
  "facebookMaybeCount" : "aeiou",
  "newsFromDate" : "2000-01-23T04:56:07.000+00:00",
  "campaignLiveTo" : "2000-01-23T04:56:07.000+00:00",
  "smallImage" : "aeiou",
  "campaignShared" : "aeiou",
  "attributeSetId" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "requiredOptions" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookUpdatedTime" : "2000-01-23T04:56:07.000+00:00",
  "campaignStatus" : "aeiou",
  "visibleTo" : "2000-01-23T04:56:07.000+00:00",
  "competitionMultijoinCooldown" : "aeiou",
  "msrpEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "image" : "aeiou",
  "thumbnail" : "aeiou",
  "visibility" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookNode" : "aeiou",
  "weight" : "aeiou",
  "shortDescription" : "aeiou",
  "specialToDate" : "2000-01-23T04:56:07.000+00:00",
  "imageLabel" : "aeiou",
  "name" : "aeiou",
  "catalogCategories" : [ "{}" ],
  "priceView" : 1.3579000000000001069366817318950779736042022705078125,
  "competitionQuestion" : "aeiou",
  "creatorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "inventory" : "{}",
  "locationLink" : "aeiou",
  "facebookNoreplyCount" : "aeiou",
  "notificationBadge" : "aeiou",
  "giftMessageAvailable" : 1.3579000000000001069366817318950779736042022705078125,
  "reviewHtml" : "aeiou",
  "couponType" : "aeiou",
  "facebookCategory" : "aeiou",
  "agreementId" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "specialPrice" : "aeiou",
  "competitionWinnerEmail" : true,
  "actionText" : "aeiou",
  "cost" : "aeiou",
  "externalId" : "aeiou",
  "competitionWinnerType" : "aeiou",
  "urlPath" : "aeiou",
  "msrpDisplayActualPriceType" : "aeiou",
  "weightType" : 1.3579000000000001069366817318950779736042022705078125,
  "linksExist" : 1.3579000000000001069366817318950779736042022705078125,
  "unlockTime" : 1.3579000000000001069366817318950779736042022705078125,
  "partnerId" : "aeiou",
  "visibleFrom" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__updateById__harpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__updateById__harpoonHpublicv12VendorAppCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (HarpoonHpublicv12VendorAppCategory)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "isPrimary" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "categoryId" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__updateById__partners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__updateById__udropshipVendorProducts = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (UdropshipVendorProduct)
  **/
    var examples = {};
  examples['application/json'] = {
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "stockQty" : "aeiou",
  "reservedQty" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "availDate" : "2000-01-23T04:56:07.000+00:00",
  "relationshipStatus" : "aeiou",
  "priority" : 1.3579000000000001069366817318950779736042022705078125,
  "catalogProduct" : "{}",
  "vendorCost" : "aeiou",
  "vendorSku" : "aeiou",
  "availState" : "aeiou",
  "udropshipVendor" : "{}",
  "shippingPrice" : "aeiou",
  "backorders" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__updateById__vendorLocations = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (HarpoonHpublicBrandvenue)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "country" : "aeiou",
  "address" : "aeiou",
  "phone" : "aeiou",
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "latitude" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "longitude" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.__update__config = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (HarpoonHpublicVendorconfig)
  **/
    var examples = {};
  examples['application/json'] = {
  "facebookFeedUpdatedAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookEventCount" : 1.3579000000000001069366817318950779736042022705078125,
  "facebookEventUpdatedAt" : "2000-01-23T04:56:07.000+00:00",
  "stripeAccessToken" : "aeiou",
  "stripePublishableKey" : "aeiou",
  "type" : 1.3579000000000001069366817318950779736042022705078125,
  "stripeUserId" : "aeiou",
  "facebookFeedCount" : 1.3579000000000001069366817318950779736042022705078125,
  "cover" : "aeiou",
  "stripeRefreshToken" : "aeiou",
  "vendorconfigId" : 1.3579000000000001069366817318950779736042022705078125,
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "facebookPageId" : "aeiou",
  "facebookEventEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "feesEventTicket" : "aeiou",
  "facebookPageToken" : "aeiou",
  "logo" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou",
  "facebookFeedEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "updatedAt" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.updateAttributes__patch_UdropshipVendors_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.prototype.updateAttributes__put_UdropshipVendors_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.upsert__patch_UdropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.udropshipVendor.upsert__put_UdropshipVendors = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (UdropshipVendor)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

