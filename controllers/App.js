'use strict';

var url = require('url');


var App = require('./AppService');


module.exports.app.analyticTrack = function app.analyticTrack (req, res, next) {
  App.app.analyticTrack(req.swagger.params, res, next);
};

module.exports.app.count = function app.count (req, res, next) {
  App.app.count(req.swagger.params, res, next);
};

module.exports.app.createChangeStream__get_Apps_changeStream = function app.createChangeStream__get_Apps_changeStream (req, res, next) {
  App.app.createChangeStream__get_Apps_changeStream(req.swagger.params, res, next);
};

module.exports.app.createChangeStream__post_Apps_changeStream = function app.createChangeStream__post_Apps_changeStream (req, res, next) {
  App.app.createChangeStream__post_Apps_changeStream(req.swagger.params, res, next);
};

module.exports.app.createWithAuthentication = function app.createWithAuthentication (req, res, next) {
  App.app.createWithAuthentication(req.swagger.params, res, next);
};

module.exports.app.deleteById = function app.deleteById (req, res, next) {
  App.app.deleteById(req.swagger.params, res, next);
};

module.exports.app.exists__get_Apps_{id}_exists = function app.exists__get_Apps_{id}_exists (req, res, next) {
  App.app.exists__get_Apps_{id}_exists(req.swagger.params, res, next);
};

module.exports.app.exists__head_Apps_{id} = function app.exists__head_Apps_{id} (req, res, next) {
  App.app.exists__head_Apps_{id}(req.swagger.params, res, next);
};

module.exports.app.find = function app.find (req, res, next) {
  App.app.find(req.swagger.params, res, next);
};

module.exports.app.findById = function app.findById (req, res, next) {
  App.app.findById(req.swagger.params, res, next);
};

module.exports.app.findByIdForVersion = function app.findByIdForVersion (req, res, next) {
  App.app.findByIdForVersion(req.swagger.params, res, next);
};

module.exports.app.findOne = function app.findOne (req, res, next) {
  App.app.findOne(req.swagger.params, res, next);
};

module.exports.app.prototype.__count__appConfigs = function app.prototype.__count__appConfigs (req, res, next) {
  App.app.prototype.__count__appConfigs(req.swagger.params, res, next);
};

module.exports.app.prototype.__count__apps = function app.prototype.__count__apps (req, res, next) {
  App.app.prototype.__count__apps(req.swagger.params, res, next);
};

module.exports.app.prototype.__count__playlists = function app.prototype.__count__playlists (req, res, next) {
  App.app.prototype.__count__playlists(req.swagger.params, res, next);
};

module.exports.app.prototype.__count__radioStreams = function app.prototype.__count__radioStreams (req, res, next) {
  App.app.prototype.__count__radioStreams(req.swagger.params, res, next);
};

module.exports.app.prototype.__count__rssFeedGroups = function app.prototype.__count__rssFeedGroups (req, res, next) {
  App.app.prototype.__count__rssFeedGroups(req.swagger.params, res, next);
};

module.exports.app.prototype.__count__rssFeeds = function app.prototype.__count__rssFeeds (req, res, next) {
  App.app.prototype.__count__rssFeeds(req.swagger.params, res, next);
};

module.exports.app.prototype.__create__appConfigs = function app.prototype.__create__appConfigs (req, res, next) {
  App.app.prototype.__create__appConfigs(req.swagger.params, res, next);
};

module.exports.app.prototype.__create__apps = function app.prototype.__create__apps (req, res, next) {
  App.app.prototype.__create__apps(req.swagger.params, res, next);
};

module.exports.app.prototype.__create__customers = function app.prototype.__create__customers (req, res, next) {
  App.app.prototype.__create__customers(req.swagger.params, res, next);
};

module.exports.app.prototype.__create__playlists = function app.prototype.__create__playlists (req, res, next) {
  App.app.prototype.__create__playlists(req.swagger.params, res, next);
};

module.exports.app.prototype.__create__radioStreams = function app.prototype.__create__radioStreams (req, res, next) {
  App.app.prototype.__create__radioStreams(req.swagger.params, res, next);
};

module.exports.app.prototype.__create__rssFeedGroups = function app.prototype.__create__rssFeedGroups (req, res, next) {
  App.app.prototype.__create__rssFeedGroups(req.swagger.params, res, next);
};

module.exports.app.prototype.__create__rssFeeds = function app.prototype.__create__rssFeeds (req, res, next) {
  App.app.prototype.__create__rssFeeds(req.swagger.params, res, next);
};

module.exports.app.prototype.__delete__appConfigs = function app.prototype.__delete__appConfigs (req, res, next) {
  App.app.prototype.__delete__appConfigs(req.swagger.params, res, next);
};

module.exports.app.prototype.__delete__apps = function app.prototype.__delete__apps (req, res, next) {
  App.app.prototype.__delete__apps(req.swagger.params, res, next);
};

module.exports.app.prototype.__delete__playlists = function app.prototype.__delete__playlists (req, res, next) {
  App.app.prototype.__delete__playlists(req.swagger.params, res, next);
};

module.exports.app.prototype.__delete__radioStreams = function app.prototype.__delete__radioStreams (req, res, next) {
  App.app.prototype.__delete__radioStreams(req.swagger.params, res, next);
};

module.exports.app.prototype.__delete__rssFeedGroups = function app.prototype.__delete__rssFeedGroups (req, res, next) {
  App.app.prototype.__delete__rssFeedGroups(req.swagger.params, res, next);
};

module.exports.app.prototype.__delete__rssFeeds = function app.prototype.__delete__rssFeeds (req, res, next) {
  App.app.prototype.__delete__rssFeeds(req.swagger.params, res, next);
};

module.exports.app.prototype.__destroyById__appConfigs = function app.prototype.__destroyById__appConfigs (req, res, next) {
  App.app.prototype.__destroyById__appConfigs(req.swagger.params, res, next);
};

module.exports.app.prototype.__destroyById__apps = function app.prototype.__destroyById__apps (req, res, next) {
  App.app.prototype.__destroyById__apps(req.swagger.params, res, next);
};

module.exports.app.prototype.__destroyById__playlists = function app.prototype.__destroyById__playlists (req, res, next) {
  App.app.prototype.__destroyById__playlists(req.swagger.params, res, next);
};

module.exports.app.prototype.__destroyById__radioStreams = function app.prototype.__destroyById__radioStreams (req, res, next) {
  App.app.prototype.__destroyById__radioStreams(req.swagger.params, res, next);
};

module.exports.app.prototype.__destroyById__rssFeedGroups = function app.prototype.__destroyById__rssFeedGroups (req, res, next) {
  App.app.prototype.__destroyById__rssFeedGroups(req.swagger.params, res, next);
};

module.exports.app.prototype.__destroyById__rssFeeds = function app.prototype.__destroyById__rssFeeds (req, res, next) {
  App.app.prototype.__destroyById__rssFeeds(req.swagger.params, res, next);
};

module.exports.app.prototype.__findById__appConfigs = function app.prototype.__findById__appConfigs (req, res, next) {
  App.app.prototype.__findById__appConfigs(req.swagger.params, res, next);
};

module.exports.app.prototype.__findById__apps = function app.prototype.__findById__apps (req, res, next) {
  App.app.prototype.__findById__apps(req.swagger.params, res, next);
};

module.exports.app.prototype.__findById__customers = function app.prototype.__findById__customers (req, res, next) {
  App.app.prototype.__findById__customers(req.swagger.params, res, next);
};

module.exports.app.prototype.__findById__playlists = function app.prototype.__findById__playlists (req, res, next) {
  App.app.prototype.__findById__playlists(req.swagger.params, res, next);
};

module.exports.app.prototype.__findById__radioStreams = function app.prototype.__findById__radioStreams (req, res, next) {
  App.app.prototype.__findById__radioStreams(req.swagger.params, res, next);
};

module.exports.app.prototype.__findById__rssFeedGroups = function app.prototype.__findById__rssFeedGroups (req, res, next) {
  App.app.prototype.__findById__rssFeedGroups(req.swagger.params, res, next);
};

module.exports.app.prototype.__findById__rssFeeds = function app.prototype.__findById__rssFeeds (req, res, next) {
  App.app.prototype.__findById__rssFeeds(req.swagger.params, res, next);
};

module.exports.app.prototype.__get__appConfigs = function app.prototype.__get__appConfigs (req, res, next) {
  App.app.prototype.__get__appConfigs(req.swagger.params, res, next);
};

module.exports.app.prototype.__get__apps = function app.prototype.__get__apps (req, res, next) {
  App.app.prototype.__get__apps(req.swagger.params, res, next);
};

module.exports.app.prototype.__get__customers = function app.prototype.__get__customers (req, res, next) {
  App.app.prototype.__get__customers(req.swagger.params, res, next);
};

module.exports.app.prototype.__get__parent = function app.prototype.__get__parent (req, res, next) {
  App.app.prototype.__get__parent(req.swagger.params, res, next);
};

module.exports.app.prototype.__get__playlists = function app.prototype.__get__playlists (req, res, next) {
  App.app.prototype.__get__playlists(req.swagger.params, res, next);
};

module.exports.app.prototype.__get__radioStreams = function app.prototype.__get__radioStreams (req, res, next) {
  App.app.prototype.__get__radioStreams(req.swagger.params, res, next);
};

module.exports.app.prototype.__get__rssFeedGroups = function app.prototype.__get__rssFeedGroups (req, res, next) {
  App.app.prototype.__get__rssFeedGroups(req.swagger.params, res, next);
};

module.exports.app.prototype.__get__rssFeeds = function app.prototype.__get__rssFeeds (req, res, next) {
  App.app.prototype.__get__rssFeeds(req.swagger.params, res, next);
};

module.exports.app.prototype.__updateById__appConfigs = function app.prototype.__updateById__appConfigs (req, res, next) {
  App.app.prototype.__updateById__appConfigs(req.swagger.params, res, next);
};

module.exports.app.prototype.__updateById__apps = function app.prototype.__updateById__apps (req, res, next) {
  App.app.prototype.__updateById__apps(req.swagger.params, res, next);
};

module.exports.app.prototype.__updateById__customers = function app.prototype.__updateById__customers (req, res, next) {
  App.app.prototype.__updateById__customers(req.swagger.params, res, next);
};

module.exports.app.prototype.__updateById__playlists = function app.prototype.__updateById__playlists (req, res, next) {
  App.app.prototype.__updateById__playlists(req.swagger.params, res, next);
};

module.exports.app.prototype.__updateById__radioStreams = function app.prototype.__updateById__radioStreams (req, res, next) {
  App.app.prototype.__updateById__radioStreams(req.swagger.params, res, next);
};

module.exports.app.prototype.__updateById__rssFeedGroups = function app.prototype.__updateById__rssFeedGroups (req, res, next) {
  App.app.prototype.__updateById__rssFeedGroups(req.swagger.params, res, next);
};

module.exports.app.prototype.__updateById__rssFeeds = function app.prototype.__updateById__rssFeeds (req, res, next) {
  App.app.prototype.__updateById__rssFeeds(req.swagger.params, res, next);
};

module.exports.app.prototype.updateAttributes__patch_Apps_{id} = function app.prototype.updateAttributes__patch_Apps_{id} (req, res, next) {
  App.app.prototype.updateAttributes__patch_Apps_{id}(req.swagger.params, res, next);
};

module.exports.app.prototype.updateAttributes__put_Apps_{id} = function app.prototype.updateAttributes__put_Apps_{id} (req, res, next) {
  App.app.prototype.updateAttributes__put_Apps_{id}(req.swagger.params, res, next);
};

module.exports.app.replaceById = function app.replaceById (req, res, next) {
  App.app.replaceById(req.swagger.params, res, next);
};

module.exports.app.replaceOrCreate = function app.replaceOrCreate (req, res, next) {
  App.app.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.app.updateAll = function app.updateAll (req, res, next) {
  App.app.updateAll(req.swagger.params, res, next);
};

module.exports.app.uploadFile = function app.uploadFile (req, res, next) {
  App.app.uploadFile(req.swagger.params, res, next);
};

module.exports.app.upsertWithWhere = function app.upsertWithWhere (req, res, next) {
  App.app.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.app.upsert__patch_Apps = function app.upsert__patch_Apps (req, res, next) {
  App.app.upsert__patch_Apps(req.swagger.params, res, next);
};

module.exports.app.upsert__put_Apps = function app.upsert__put_Apps (req, res, next) {
  App.app.upsert__put_Apps(req.swagger.params, res, next);
};

module.exports.app.verify = function app.verify (req, res, next) {
  App.app.verify(req.swagger.params, res, next);
};
