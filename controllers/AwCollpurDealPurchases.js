'use strict';

var url = require('url');


var AwCollpurDealPurchases = require('./AwCollpurDealPurchasesService');


module.exports.awCollpurDealPurchases.count = function awCollpurDealPurchases.count (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.count(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.create = function awCollpurDealPurchases.create (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.create(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.createChangeStream__get_AwCollpurDealPurchases_changeStream = function awCollpurDealPurchases.createChangeStream__get_AwCollpurDealPurchases_changeStream (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.createChangeStream__get_AwCollpurDealPurchases_changeStream(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.createChangeStream__post_AwCollpurDealPurchases_changeStream = function awCollpurDealPurchases.createChangeStream__post_AwCollpurDealPurchases_changeStream (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.createChangeStream__post_AwCollpurDealPurchases_changeStream(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.deleteById = function awCollpurDealPurchases.deleteById (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.deleteById(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.exists__get_AwCollpurDealPurchases_{id}_exists = function awCollpurDealPurchases.exists__get_AwCollpurDealPurchases_{id}_exists (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.exists__get_AwCollpurDealPurchases_{id}_exists(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.exists__head_AwCollpurDealPurchases_{id} = function awCollpurDealPurchases.exists__head_AwCollpurDealPurchases_{id} (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.exists__head_AwCollpurDealPurchases_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.find = function awCollpurDealPurchases.find (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.find(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.findById = function awCollpurDealPurchases.findById (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.findById(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.findOne = function awCollpurDealPurchases.findOne (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.findOne(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.prototype.__create__awCollpurCoupon = function awCollpurDealPurchases.prototype.__create__awCollpurCoupon (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.prototype.__create__awCollpurCoupon(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.prototype.__destroy__awCollpurCoupon = function awCollpurDealPurchases.prototype.__destroy__awCollpurCoupon (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.prototype.__destroy__awCollpurCoupon(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.prototype.__get__awCollpurCoupon = function awCollpurDealPurchases.prototype.__get__awCollpurCoupon (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.prototype.__get__awCollpurCoupon(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.prototype.__update__awCollpurCoupon = function awCollpurDealPurchases.prototype.__update__awCollpurCoupon (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.prototype.__update__awCollpurCoupon(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.prototype.updateAttributes__patch_AwCollpurDealPurchases_{id} = function awCollpurDealPurchases.prototype.updateAttributes__patch_AwCollpurDealPurchases_{id} (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.prototype.updateAttributes__patch_AwCollpurDealPurchases_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.prototype.updateAttributes__put_AwCollpurDealPurchases_{id} = function awCollpurDealPurchases.prototype.updateAttributes__put_AwCollpurDealPurchases_{id} (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.prototype.updateAttributes__put_AwCollpurDealPurchases_{id}(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.replaceById = function awCollpurDealPurchases.replaceById (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.replaceById(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.replaceOrCreate = function awCollpurDealPurchases.replaceOrCreate (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.updateAll = function awCollpurDealPurchases.updateAll (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.updateAll(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.upsertWithWhere = function awCollpurDealPurchases.upsertWithWhere (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.upsert__patch_AwCollpurDealPurchases = function awCollpurDealPurchases.upsert__patch_AwCollpurDealPurchases (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.upsert__patch_AwCollpurDealPurchases(req.swagger.params, res, next);
};

module.exports.awCollpurDealPurchases.upsert__put_AwCollpurDealPurchases = function awCollpurDealPurchases.upsert__put_AwCollpurDealPurchases (req, res, next) {
  AwCollpurDealPurchases.awCollpurDealPurchases.upsert__put_AwCollpurDealPurchases(req.swagger.params, res, next);
};
