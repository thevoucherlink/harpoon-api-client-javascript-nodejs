'use strict';

exports.app.analyticTrack = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AnalyticRequest)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.createChangeStream__get_Apps_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.createChangeStream__post_Apps_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.createWithAuthentication = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.exists__get_Apps_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.exists__head_Apps_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.findByIdForVersion = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * appOs (String)
  * appVersion (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__count__appConfigs = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__count__apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__count__playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__count__radioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__count__rssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__count__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__create__appConfigs = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AppConfig)
  **/
    var examples = {};
  examples['application/json'] = {
  "featureCompetitionsAttendeeList" : true,
  "iOSTeamId" : "aeiou",
  "iOSBuild" : "aeiou",
  "imgBrandLogo" : "aeiou",
  "googleAppId" : "aeiou",
  "featurePhoneVerification" : true,
  "iosUriScheme" : "aeiou",
  "imgNavBarLogo" : "aeiou",
  "facebookConfig" : {
    "appToken" : "aeiou",
    "appId" : "aeiou",
    "scopes" : "aeiou"
  },
  "featureOffers" : true,
  "featureEventsCategorized" : true,
  "imgAppLogo" : "aeiou",
  "featureLogin" : true,
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "appId" : "aeiou",
  "playerMainButtonTitle" : "aeiou",
  "androidGoogleServices" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "featureRadioAutoPlay" : true,
  "twitterConfig" : {
    "apiKey" : "aeiou",
    "apiSecret" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "iOSGoogleServices" : "aeiou",
  "typeNewsFocusSource" : "aeiou",
  "app" : "{}",
  "urbanAirshipConfig" : {
    "developmentAppKey" : "aeiou",
    "developmentAppSecret" : "aeiou",
    "productionAppKey" : "aeiou",
    "productionLogLevel" : "aeiou",
    "productionAppSecret" : "aeiou",
    "developmentLogLevel" : "aeiou",
    "inProduction" : true
  },
  "featureGoogleAnalytics" : true,
  "featureEvents" : true,
  "imgMenuBackground" : "aeiou",
  "isChildConfig" : true,
  "pushNotificationProvider" : "aeiou",
  "featureRadioSession" : true,
  "ads" : [ {
    "unitType" : "aeiou",
    "unitName" : "aeiou",
    "unitId" : "aeiou"
  } ],
  "rssTags" : {
    "imgTumbnail" : "aeiou",
    "imgMain" : "aeiou",
    "link" : "aeiou",
    "description" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "postDateTime" : "aeiou",
    "category" : "aeiou",
    "title" : "aeiou"
  },
  "iOSReleaseNotes" : "aeiou",
  "name" : "aeiou",
  "imgSplashScreen" : "aeiou",
  "imgBrandPlaceholder" : "aeiou",
  "androidApk" : "aeiou",
  "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
  "androidUriScheme" : "aeiou",
  "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "batchConfig" : {
    "apiKey" : "aeiou",
    "androidApiKey" : "aeiou",
    "iOSApiKey" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "featureCompetitions" : true,
  "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
  "featureCommunity" : true,
  "iOSPushNotificationPrivateKey" : "aeiou",
  "color" : "aeiou",
  "featureTritonPlayer" : true,
  "featureOffersCategorized" : true,
  "featureEventsAttendeeList" : true,
  "featurePlayer" : true,
  "iOSPushNotificationPassword" : "aeiou",
  "rssWebViewCss" : "aeiou",
  "featureCommunityCategorized" : true,
  "androidGoogleApiKey" : "aeiou",
  "colors" : {
    "textSecondary" : "aeiou",
    "radioPlayerItem" : "aeiou",
    "radioPlayer" : "aeiou",
    "appSecondary" : "aeiou",
    "menuBackground" : "aeiou",
    "menuItem" : "aeiou",
    "appGradientPrimary" : "aeiou",
    "radioTextPrimary" : "aeiou",
    "radioPlayButtonBackground" : "aeiou",
    "radioProgressBar" : "aeiou",
    "radioTextSecondary" : "aeiou",
    "radioPlayButton" : "aeiou",
    "radioNavBar" : "aeiou",
    "feedSegmentIndicator" : "aeiou",
    "radioPauseButton" : "aeiou",
    "textPrimary" : "aeiou",
    "appPrimary" : "aeiou",
    "radioPauseButtonBackground" : "aeiou"
  },
  "iOSAds" : [ "" ],
  "branchConfig" : {
    "branchTestUrl" : "aeiou",
    "branchTestKey" : "aeiou",
    "branchLiveUrl" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "branchLiveKey" : "aeiou"
  },
  "imgSponsorSplash" : "aeiou",
  "androidKeystoreConfig" : {
    "storePassword" : "aeiou",
    "hashKey" : "aeiou",
    "keyAlias" : "aeiou",
    "keyPassword" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "androidAds" : [ "" ],
  "wordpressShareUrlStub" : "aeiou",
  "featureBrandFollowerList" : true,
  "iOSAppId" : "aeiou",
  "iOSPushNotificationCertificate" : "aeiou",
  "androidGoogleCloudMessagingSenderId" : "aeiou",
  "androidKeystore" : "aeiou",
  "playerConfig" : {
    "displayDescription" : true,
    "controls" : true,
    "hlshtml" : true,
    "playerCaptionConfig" : "{}",
    "skinName" : "aeiou",
    "mute" : true,
    "flashPlayer" : "aeiou",
    "autostart" : true,
    "stretching" : "aeiou",
    "playerAdConfig" : "{}",
    "visualPlaylist" : true,
    "preload" : "aeiou",
    "baseSkinPath" : "aeiou",
    "displayTitle" : true,
    "skinInactive" : "aeiou",
    "skinActive" : "aeiou",
    "repeat" : true,
    "skinBackground" : "aeiou",
    "skinCss" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "primary" : "aeiou"
  },
  "googleAnalyticsTracking" : {
    "mobileTrackingId" : "aeiou",
    "webTrackingId" : "aeiou"
  },
  "audioSponsorPreRollAd" : "aeiou",
  "pushWooshConfig" : {
    "appName" : "aeiou",
    "appToken" : "aeiou",
    "appId" : "aeiou"
  },
  "iOSVersion" : "aeiou",
  "iOSAppleId" : "aeiou",
  "androidKey" : "aeiou",
  "imgSponsorMenu" : "aeiou",
  "featurePlayerMainButton" : true,
  "featureWordpressConnect" : true,
  "featureNews" : true,
  "menu" : [ {
    "image" : "aeiou",
    "pageActionIcon" : "aeiou",
    "visibility" : true,
    "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
    "pageAction" : true,
    "styleCss" : "aeiou",
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioDontShowOnCamera" : true,
    "url" : "aeiou",
    "pageActionLink" : "aeiou"
  } ],
  "featureFacebookLogin" : true,
  "featureBrandFollow" : true,
  "androidBuild" : "aeiou",
  "featureRadio" : true,
  "androidVersion" : "aeiou",
  "loginExternal" : {
    "authorizeUrl" : "aeiou",
    "apiKey" : "aeiou",
    "accessUrl" : "aeiou",
    "requestUrl" : "aeiou",
    "version" : "aeiou"
  },
  "customerDetailsForm" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "title" : "aeiou",
    "rows" : [ {
      "metadataKey" : "aeiou",
      "hidden" : true,
      "defaultValue" : "aeiou",
      "validationMessage" : "aeiou",
      "selectorOptions" : [ {
        "format" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "title" : "aeiou",
        "value" : "aeiou"
      } ],
      "placeholder" : "aeiou",
      "type" : "aeiou",
      "title" : "aeiou",
      "validationRegExp" : "aeiou",
      "required" : true
    } ]
  } ],
  "connectTo" : {
    "heading" : "aeiou",
    "link" : "aeiou",
    "description" : "aeiou",
    "token" : "aeiou"
  },
  "featureHarpoonLogin" : true,
  "iOSPushNotificationPem" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__create__apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__create__customers = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Customer)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__create__playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__create__radioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__create__rssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__create__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__delete__appConfigs = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__delete__apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__delete__playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__delete__radioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__delete__rssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__delete__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__destroyById__appConfigs = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__destroyById__apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__destroyById__playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__destroyById__radioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__destroyById__rssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__destroyById__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.app.prototype.__findById__appConfigs = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "featureCompetitionsAttendeeList" : true,
  "iOSTeamId" : "aeiou",
  "iOSBuild" : "aeiou",
  "imgBrandLogo" : "aeiou",
  "googleAppId" : "aeiou",
  "featurePhoneVerification" : true,
  "iosUriScheme" : "aeiou",
  "imgNavBarLogo" : "aeiou",
  "facebookConfig" : {
    "appToken" : "aeiou",
    "appId" : "aeiou",
    "scopes" : "aeiou"
  },
  "featureOffers" : true,
  "featureEventsCategorized" : true,
  "imgAppLogo" : "aeiou",
  "featureLogin" : true,
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "appId" : "aeiou",
  "playerMainButtonTitle" : "aeiou",
  "androidGoogleServices" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "featureRadioAutoPlay" : true,
  "twitterConfig" : {
    "apiKey" : "aeiou",
    "apiSecret" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "iOSGoogleServices" : "aeiou",
  "typeNewsFocusSource" : "aeiou",
  "app" : "{}",
  "urbanAirshipConfig" : {
    "developmentAppKey" : "aeiou",
    "developmentAppSecret" : "aeiou",
    "productionAppKey" : "aeiou",
    "productionLogLevel" : "aeiou",
    "productionAppSecret" : "aeiou",
    "developmentLogLevel" : "aeiou",
    "inProduction" : true
  },
  "featureGoogleAnalytics" : true,
  "featureEvents" : true,
  "imgMenuBackground" : "aeiou",
  "isChildConfig" : true,
  "pushNotificationProvider" : "aeiou",
  "featureRadioSession" : true,
  "ads" : [ {
    "unitType" : "aeiou",
    "unitName" : "aeiou",
    "unitId" : "aeiou"
  } ],
  "rssTags" : {
    "imgTumbnail" : "aeiou",
    "imgMain" : "aeiou",
    "link" : "aeiou",
    "description" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "postDateTime" : "aeiou",
    "category" : "aeiou",
    "title" : "aeiou"
  },
  "iOSReleaseNotes" : "aeiou",
  "name" : "aeiou",
  "imgSplashScreen" : "aeiou",
  "imgBrandPlaceholder" : "aeiou",
  "androidApk" : "aeiou",
  "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
  "androidUriScheme" : "aeiou",
  "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "batchConfig" : {
    "apiKey" : "aeiou",
    "androidApiKey" : "aeiou",
    "iOSApiKey" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "featureCompetitions" : true,
  "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
  "featureCommunity" : true,
  "iOSPushNotificationPrivateKey" : "aeiou",
  "color" : "aeiou",
  "featureTritonPlayer" : true,
  "featureOffersCategorized" : true,
  "featureEventsAttendeeList" : true,
  "featurePlayer" : true,
  "iOSPushNotificationPassword" : "aeiou",
  "rssWebViewCss" : "aeiou",
  "featureCommunityCategorized" : true,
  "androidGoogleApiKey" : "aeiou",
  "colors" : {
    "textSecondary" : "aeiou",
    "radioPlayerItem" : "aeiou",
    "radioPlayer" : "aeiou",
    "appSecondary" : "aeiou",
    "menuBackground" : "aeiou",
    "menuItem" : "aeiou",
    "appGradientPrimary" : "aeiou",
    "radioTextPrimary" : "aeiou",
    "radioPlayButtonBackground" : "aeiou",
    "radioProgressBar" : "aeiou",
    "radioTextSecondary" : "aeiou",
    "radioPlayButton" : "aeiou",
    "radioNavBar" : "aeiou",
    "feedSegmentIndicator" : "aeiou",
    "radioPauseButton" : "aeiou",
    "textPrimary" : "aeiou",
    "appPrimary" : "aeiou",
    "radioPauseButtonBackground" : "aeiou"
  },
  "iOSAds" : [ "" ],
  "branchConfig" : {
    "branchTestUrl" : "aeiou",
    "branchTestKey" : "aeiou",
    "branchLiveUrl" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "branchLiveKey" : "aeiou"
  },
  "imgSponsorSplash" : "aeiou",
  "androidKeystoreConfig" : {
    "storePassword" : "aeiou",
    "hashKey" : "aeiou",
    "keyAlias" : "aeiou",
    "keyPassword" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "androidAds" : [ "" ],
  "wordpressShareUrlStub" : "aeiou",
  "featureBrandFollowerList" : true,
  "iOSAppId" : "aeiou",
  "iOSPushNotificationCertificate" : "aeiou",
  "androidGoogleCloudMessagingSenderId" : "aeiou",
  "androidKeystore" : "aeiou",
  "playerConfig" : {
    "displayDescription" : true,
    "controls" : true,
    "hlshtml" : true,
    "playerCaptionConfig" : "{}",
    "skinName" : "aeiou",
    "mute" : true,
    "flashPlayer" : "aeiou",
    "autostart" : true,
    "stretching" : "aeiou",
    "playerAdConfig" : "{}",
    "visualPlaylist" : true,
    "preload" : "aeiou",
    "baseSkinPath" : "aeiou",
    "displayTitle" : true,
    "skinInactive" : "aeiou",
    "skinActive" : "aeiou",
    "repeat" : true,
    "skinBackground" : "aeiou",
    "skinCss" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "primary" : "aeiou"
  },
  "googleAnalyticsTracking" : {
    "mobileTrackingId" : "aeiou",
    "webTrackingId" : "aeiou"
  },
  "audioSponsorPreRollAd" : "aeiou",
  "pushWooshConfig" : {
    "appName" : "aeiou",
    "appToken" : "aeiou",
    "appId" : "aeiou"
  },
  "iOSVersion" : "aeiou",
  "iOSAppleId" : "aeiou",
  "androidKey" : "aeiou",
  "imgSponsorMenu" : "aeiou",
  "featurePlayerMainButton" : true,
  "featureWordpressConnect" : true,
  "featureNews" : true,
  "menu" : [ {
    "image" : "aeiou",
    "pageActionIcon" : "aeiou",
    "visibility" : true,
    "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
    "pageAction" : true,
    "styleCss" : "aeiou",
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioDontShowOnCamera" : true,
    "url" : "aeiou",
    "pageActionLink" : "aeiou"
  } ],
  "featureFacebookLogin" : true,
  "featureBrandFollow" : true,
  "androidBuild" : "aeiou",
  "featureRadio" : true,
  "androidVersion" : "aeiou",
  "loginExternal" : {
    "authorizeUrl" : "aeiou",
    "apiKey" : "aeiou",
    "accessUrl" : "aeiou",
    "requestUrl" : "aeiou",
    "version" : "aeiou"
  },
  "customerDetailsForm" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "title" : "aeiou",
    "rows" : [ {
      "metadataKey" : "aeiou",
      "hidden" : true,
      "defaultValue" : "aeiou",
      "validationMessage" : "aeiou",
      "selectorOptions" : [ {
        "format" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "title" : "aeiou",
        "value" : "aeiou"
      } ],
      "placeholder" : "aeiou",
      "type" : "aeiou",
      "title" : "aeiou",
      "validationRegExp" : "aeiou",
      "required" : true
    } ]
  } ],
  "connectTo" : {
    "heading" : "aeiou",
    "link" : "aeiou",
    "description" : "aeiou",
    "token" : "aeiou"
  },
  "featureHarpoonLogin" : true,
  "iOSPushNotificationPem" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__findById__apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__findById__customers = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__findById__playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__findById__radioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__findById__rssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__findById__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__get__appConfigs = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "featureCompetitionsAttendeeList" : true,
  "iOSTeamId" : "aeiou",
  "iOSBuild" : "aeiou",
  "imgBrandLogo" : "aeiou",
  "googleAppId" : "aeiou",
  "featurePhoneVerification" : true,
  "iosUriScheme" : "aeiou",
  "imgNavBarLogo" : "aeiou",
  "facebookConfig" : {
    "appToken" : "aeiou",
    "appId" : "aeiou",
    "scopes" : "aeiou"
  },
  "featureOffers" : true,
  "featureEventsCategorized" : true,
  "imgAppLogo" : "aeiou",
  "featureLogin" : true,
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "appId" : "aeiou",
  "playerMainButtonTitle" : "aeiou",
  "androidGoogleServices" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "featureRadioAutoPlay" : true,
  "twitterConfig" : {
    "apiKey" : "aeiou",
    "apiSecret" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "iOSGoogleServices" : "aeiou",
  "typeNewsFocusSource" : "aeiou",
  "app" : "{}",
  "urbanAirshipConfig" : {
    "developmentAppKey" : "aeiou",
    "developmentAppSecret" : "aeiou",
    "productionAppKey" : "aeiou",
    "productionLogLevel" : "aeiou",
    "productionAppSecret" : "aeiou",
    "developmentLogLevel" : "aeiou",
    "inProduction" : true
  },
  "featureGoogleAnalytics" : true,
  "featureEvents" : true,
  "imgMenuBackground" : "aeiou",
  "isChildConfig" : true,
  "pushNotificationProvider" : "aeiou",
  "featureRadioSession" : true,
  "ads" : [ {
    "unitType" : "aeiou",
    "unitName" : "aeiou",
    "unitId" : "aeiou"
  } ],
  "rssTags" : {
    "imgTumbnail" : "aeiou",
    "imgMain" : "aeiou",
    "link" : "aeiou",
    "description" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "postDateTime" : "aeiou",
    "category" : "aeiou",
    "title" : "aeiou"
  },
  "iOSReleaseNotes" : "aeiou",
  "name" : "aeiou",
  "imgSplashScreen" : "aeiou",
  "imgBrandPlaceholder" : "aeiou",
  "androidApk" : "aeiou",
  "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
  "androidUriScheme" : "aeiou",
  "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "batchConfig" : {
    "apiKey" : "aeiou",
    "androidApiKey" : "aeiou",
    "iOSApiKey" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "featureCompetitions" : true,
  "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
  "featureCommunity" : true,
  "iOSPushNotificationPrivateKey" : "aeiou",
  "color" : "aeiou",
  "featureTritonPlayer" : true,
  "featureOffersCategorized" : true,
  "featureEventsAttendeeList" : true,
  "featurePlayer" : true,
  "iOSPushNotificationPassword" : "aeiou",
  "rssWebViewCss" : "aeiou",
  "featureCommunityCategorized" : true,
  "androidGoogleApiKey" : "aeiou",
  "colors" : {
    "textSecondary" : "aeiou",
    "radioPlayerItem" : "aeiou",
    "radioPlayer" : "aeiou",
    "appSecondary" : "aeiou",
    "menuBackground" : "aeiou",
    "menuItem" : "aeiou",
    "appGradientPrimary" : "aeiou",
    "radioTextPrimary" : "aeiou",
    "radioPlayButtonBackground" : "aeiou",
    "radioProgressBar" : "aeiou",
    "radioTextSecondary" : "aeiou",
    "radioPlayButton" : "aeiou",
    "radioNavBar" : "aeiou",
    "feedSegmentIndicator" : "aeiou",
    "radioPauseButton" : "aeiou",
    "textPrimary" : "aeiou",
    "appPrimary" : "aeiou",
    "radioPauseButtonBackground" : "aeiou"
  },
  "iOSAds" : [ "" ],
  "branchConfig" : {
    "branchTestUrl" : "aeiou",
    "branchTestKey" : "aeiou",
    "branchLiveUrl" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "branchLiveKey" : "aeiou"
  },
  "imgSponsorSplash" : "aeiou",
  "androidKeystoreConfig" : {
    "storePassword" : "aeiou",
    "hashKey" : "aeiou",
    "keyAlias" : "aeiou",
    "keyPassword" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "androidAds" : [ "" ],
  "wordpressShareUrlStub" : "aeiou",
  "featureBrandFollowerList" : true,
  "iOSAppId" : "aeiou",
  "iOSPushNotificationCertificate" : "aeiou",
  "androidGoogleCloudMessagingSenderId" : "aeiou",
  "androidKeystore" : "aeiou",
  "playerConfig" : {
    "displayDescription" : true,
    "controls" : true,
    "hlshtml" : true,
    "playerCaptionConfig" : "{}",
    "skinName" : "aeiou",
    "mute" : true,
    "flashPlayer" : "aeiou",
    "autostart" : true,
    "stretching" : "aeiou",
    "playerAdConfig" : "{}",
    "visualPlaylist" : true,
    "preload" : "aeiou",
    "baseSkinPath" : "aeiou",
    "displayTitle" : true,
    "skinInactive" : "aeiou",
    "skinActive" : "aeiou",
    "repeat" : true,
    "skinBackground" : "aeiou",
    "skinCss" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "primary" : "aeiou"
  },
  "googleAnalyticsTracking" : {
    "mobileTrackingId" : "aeiou",
    "webTrackingId" : "aeiou"
  },
  "audioSponsorPreRollAd" : "aeiou",
  "pushWooshConfig" : {
    "appName" : "aeiou",
    "appToken" : "aeiou",
    "appId" : "aeiou"
  },
  "iOSVersion" : "aeiou",
  "iOSAppleId" : "aeiou",
  "androidKey" : "aeiou",
  "imgSponsorMenu" : "aeiou",
  "featurePlayerMainButton" : true,
  "featureWordpressConnect" : true,
  "featureNews" : true,
  "menu" : [ {
    "image" : "aeiou",
    "pageActionIcon" : "aeiou",
    "visibility" : true,
    "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
    "pageAction" : true,
    "styleCss" : "aeiou",
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioDontShowOnCamera" : true,
    "url" : "aeiou",
    "pageActionLink" : "aeiou"
  } ],
  "featureFacebookLogin" : true,
  "featureBrandFollow" : true,
  "androidBuild" : "aeiou",
  "featureRadio" : true,
  "androidVersion" : "aeiou",
  "loginExternal" : {
    "authorizeUrl" : "aeiou",
    "apiKey" : "aeiou",
    "accessUrl" : "aeiou",
    "requestUrl" : "aeiou",
    "version" : "aeiou"
  },
  "customerDetailsForm" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "title" : "aeiou",
    "rows" : [ {
      "metadataKey" : "aeiou",
      "hidden" : true,
      "defaultValue" : "aeiou",
      "validationMessage" : "aeiou",
      "selectorOptions" : [ {
        "format" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "title" : "aeiou",
        "value" : "aeiou"
      } ],
      "placeholder" : "aeiou",
      "type" : "aeiou",
      "title" : "aeiou",
      "validationRegExp" : "aeiou",
      "required" : true
    } ]
  } ],
  "connectTo" : {
    "heading" : "aeiou",
    "link" : "aeiou",
    "description" : "aeiou",
    "token" : "aeiou"
  },
  "featureHarpoonLogin" : true,
  "iOSPushNotificationPem" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__get__apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__get__customers = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__get__parent = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__get__playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__get__radioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__get__rssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__get__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__updateById__appConfigs = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AppConfig)
  **/
    var examples = {};
  examples['application/json'] = {
  "featureCompetitionsAttendeeList" : true,
  "iOSTeamId" : "aeiou",
  "iOSBuild" : "aeiou",
  "imgBrandLogo" : "aeiou",
  "googleAppId" : "aeiou",
  "featurePhoneVerification" : true,
  "iosUriScheme" : "aeiou",
  "imgNavBarLogo" : "aeiou",
  "facebookConfig" : {
    "appToken" : "aeiou",
    "appId" : "aeiou",
    "scopes" : "aeiou"
  },
  "featureOffers" : true,
  "featureEventsCategorized" : true,
  "imgAppLogo" : "aeiou",
  "featureLogin" : true,
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "appId" : "aeiou",
  "playerMainButtonTitle" : "aeiou",
  "androidGoogleServices" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "featureRadioAutoPlay" : true,
  "twitterConfig" : {
    "apiKey" : "aeiou",
    "apiSecret" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "iOSGoogleServices" : "aeiou",
  "typeNewsFocusSource" : "aeiou",
  "app" : "{}",
  "urbanAirshipConfig" : {
    "developmentAppKey" : "aeiou",
    "developmentAppSecret" : "aeiou",
    "productionAppKey" : "aeiou",
    "productionLogLevel" : "aeiou",
    "productionAppSecret" : "aeiou",
    "developmentLogLevel" : "aeiou",
    "inProduction" : true
  },
  "featureGoogleAnalytics" : true,
  "featureEvents" : true,
  "imgMenuBackground" : "aeiou",
  "isChildConfig" : true,
  "pushNotificationProvider" : "aeiou",
  "featureRadioSession" : true,
  "ads" : [ {
    "unitType" : "aeiou",
    "unitName" : "aeiou",
    "unitId" : "aeiou"
  } ],
  "rssTags" : {
    "imgTumbnail" : "aeiou",
    "imgMain" : "aeiou",
    "link" : "aeiou",
    "description" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "postDateTime" : "aeiou",
    "category" : "aeiou",
    "title" : "aeiou"
  },
  "iOSReleaseNotes" : "aeiou",
  "name" : "aeiou",
  "imgSplashScreen" : "aeiou",
  "imgBrandPlaceholder" : "aeiou",
  "androidApk" : "aeiou",
  "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
  "androidUriScheme" : "aeiou",
  "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "batchConfig" : {
    "apiKey" : "aeiou",
    "androidApiKey" : "aeiou",
    "iOSApiKey" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "featureCompetitions" : true,
  "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
  "featureCommunity" : true,
  "iOSPushNotificationPrivateKey" : "aeiou",
  "color" : "aeiou",
  "featureTritonPlayer" : true,
  "featureOffersCategorized" : true,
  "featureEventsAttendeeList" : true,
  "featurePlayer" : true,
  "iOSPushNotificationPassword" : "aeiou",
  "rssWebViewCss" : "aeiou",
  "featureCommunityCategorized" : true,
  "androidGoogleApiKey" : "aeiou",
  "colors" : {
    "textSecondary" : "aeiou",
    "radioPlayerItem" : "aeiou",
    "radioPlayer" : "aeiou",
    "appSecondary" : "aeiou",
    "menuBackground" : "aeiou",
    "menuItem" : "aeiou",
    "appGradientPrimary" : "aeiou",
    "radioTextPrimary" : "aeiou",
    "radioPlayButtonBackground" : "aeiou",
    "radioProgressBar" : "aeiou",
    "radioTextSecondary" : "aeiou",
    "radioPlayButton" : "aeiou",
    "radioNavBar" : "aeiou",
    "feedSegmentIndicator" : "aeiou",
    "radioPauseButton" : "aeiou",
    "textPrimary" : "aeiou",
    "appPrimary" : "aeiou",
    "radioPauseButtonBackground" : "aeiou"
  },
  "iOSAds" : [ "" ],
  "branchConfig" : {
    "branchTestUrl" : "aeiou",
    "branchTestKey" : "aeiou",
    "branchLiveUrl" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "branchLiveKey" : "aeiou"
  },
  "imgSponsorSplash" : "aeiou",
  "androidKeystoreConfig" : {
    "storePassword" : "aeiou",
    "hashKey" : "aeiou",
    "keyAlias" : "aeiou",
    "keyPassword" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "androidAds" : [ "" ],
  "wordpressShareUrlStub" : "aeiou",
  "featureBrandFollowerList" : true,
  "iOSAppId" : "aeiou",
  "iOSPushNotificationCertificate" : "aeiou",
  "androidGoogleCloudMessagingSenderId" : "aeiou",
  "androidKeystore" : "aeiou",
  "playerConfig" : {
    "displayDescription" : true,
    "controls" : true,
    "hlshtml" : true,
    "playerCaptionConfig" : "{}",
    "skinName" : "aeiou",
    "mute" : true,
    "flashPlayer" : "aeiou",
    "autostart" : true,
    "stretching" : "aeiou",
    "playerAdConfig" : "{}",
    "visualPlaylist" : true,
    "preload" : "aeiou",
    "baseSkinPath" : "aeiou",
    "displayTitle" : true,
    "skinInactive" : "aeiou",
    "skinActive" : "aeiou",
    "repeat" : true,
    "skinBackground" : "aeiou",
    "skinCss" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "primary" : "aeiou"
  },
  "googleAnalyticsTracking" : {
    "mobileTrackingId" : "aeiou",
    "webTrackingId" : "aeiou"
  },
  "audioSponsorPreRollAd" : "aeiou",
  "pushWooshConfig" : {
    "appName" : "aeiou",
    "appToken" : "aeiou",
    "appId" : "aeiou"
  },
  "iOSVersion" : "aeiou",
  "iOSAppleId" : "aeiou",
  "androidKey" : "aeiou",
  "imgSponsorMenu" : "aeiou",
  "featurePlayerMainButton" : true,
  "featureWordpressConnect" : true,
  "featureNews" : true,
  "menu" : [ {
    "image" : "aeiou",
    "pageActionIcon" : "aeiou",
    "visibility" : true,
    "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
    "pageAction" : true,
    "styleCss" : "aeiou",
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioDontShowOnCamera" : true,
    "url" : "aeiou",
    "pageActionLink" : "aeiou"
  } ],
  "featureFacebookLogin" : true,
  "featureBrandFollow" : true,
  "androidBuild" : "aeiou",
  "featureRadio" : true,
  "androidVersion" : "aeiou",
  "loginExternal" : {
    "authorizeUrl" : "aeiou",
    "apiKey" : "aeiou",
    "accessUrl" : "aeiou",
    "requestUrl" : "aeiou",
    "version" : "aeiou"
  },
  "customerDetailsForm" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "title" : "aeiou",
    "rows" : [ {
      "metadataKey" : "aeiou",
      "hidden" : true,
      "defaultValue" : "aeiou",
      "validationMessage" : "aeiou",
      "selectorOptions" : [ {
        "format" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "title" : "aeiou",
        "value" : "aeiou"
      } ],
      "placeholder" : "aeiou",
      "type" : "aeiou",
      "title" : "aeiou",
      "validationRegExp" : "aeiou",
      "required" : true
    } ]
  } ],
  "connectTo" : {
    "heading" : "aeiou",
    "link" : "aeiou",
    "description" : "aeiou",
    "token" : "aeiou"
  },
  "featureHarpoonLogin" : true,
  "iOSPushNotificationPem" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__updateById__apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__updateById__customers = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (Customer)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__updateById__playlists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (Playlist)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "isMain" : true,
  "appId" : "aeiou",
  "name" : "aeiou",
  "playlistItems" : [ "{}" ],
  "shortDescription" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__updateById__radioStreams = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RadioStream)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__updateById__rssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.__updateById__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.updateAttributes__patch_Apps_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.prototype.updateAttributes__put_Apps_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.uploadFile = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * type (String)
  * data (MagentoFileUpload)
  **/
    var examples = {};
  examples['application/json'] = {
  "file" : "aeiou",
  "name" : "aeiou",
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.upsert__patch_Apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.upsert__put_Apps = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (App)
  **/
    var examples = {};
  examples['application/json'] = {
  "privacyUrl" : "aeiou",
  "parent" : "{}",
  "copyright" : "aeiou",
  "keywords" : "aeiou",
  "urlScheme" : "aeiou",
  "vendorId" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "radioStreams" : [ "{}" ],
  "storeIosUrl" : "aeiou",
  "internalId" : 1.3579000000000001069366817318950779736042022705078125,
  "appConfig" : {
    "featureCompetitionsAttendeeList" : true,
    "iOSTeamId" : "aeiou",
    "iOSBuild" : "aeiou",
    "imgBrandLogo" : "aeiou",
    "googleAppId" : "aeiou",
    "featurePhoneVerification" : true,
    "iosUriScheme" : "aeiou",
    "imgNavBarLogo" : "aeiou",
    "facebookConfig" : {
      "appToken" : "aeiou",
      "appId" : "aeiou",
      "scopes" : "aeiou"
    },
    "featureOffers" : true,
    "featureEventsCategorized" : true,
    "imgAppLogo" : "aeiou",
    "featureLogin" : true,
    "contact" : {
      "whatsapp" : "aeiou",
      "website" : "aeiou",
      "address" : {
        "attnLastName" : "aeiou",
        "streetAddress" : "aeiou",
        "city" : "aeiou",
        "countryCode" : "aeiou",
        "streetAddressComp" : "aeiou",
        "postcode" : "aeiou",
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "region" : "aeiou",
        "attnFirstName" : "aeiou"
      },
      "facebook" : "aeiou",
      "linkedIn" : "aeiou",
      "googlePlus" : "aeiou",
      "snapchat" : "aeiou",
      "twitter" : "aeiou",
      "phone" : "aeiou",
      "text" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou",
      "hashtag" : "aeiou"
    },
    "appId" : "aeiou",
    "playerMainButtonTitle" : "aeiou",
    "androidGoogleServices" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "featureRadioAutoPlay" : true,
    "twitterConfig" : {
      "apiKey" : "aeiou",
      "apiSecret" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "iOSGoogleServices" : "aeiou",
    "typeNewsFocusSource" : "aeiou",
    "app" : "{}",
    "urbanAirshipConfig" : {
      "developmentAppKey" : "aeiou",
      "developmentAppSecret" : "aeiou",
      "productionAppKey" : "aeiou",
      "productionLogLevel" : "aeiou",
      "productionAppSecret" : "aeiou",
      "developmentLogLevel" : "aeiou",
      "inProduction" : true
    },
    "featureGoogleAnalytics" : true,
    "featureEvents" : true,
    "imgMenuBackground" : "aeiou",
    "isChildConfig" : true,
    "pushNotificationProvider" : "aeiou",
    "featureRadioSession" : true,
    "ads" : [ {
      "unitType" : "aeiou",
      "unitName" : "aeiou",
      "unitId" : "aeiou"
    } ],
    "rssTags" : {
      "imgTumbnail" : "aeiou",
      "imgMain" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "postDateTime" : "aeiou",
      "category" : "aeiou",
      "title" : "aeiou"
    },
    "iOSReleaseNotes" : "aeiou",
    "name" : "aeiou",
    "imgSplashScreen" : "aeiou",
    "imgBrandPlaceholder" : "aeiou",
    "androidApk" : "aeiou",
    "radioSessionInterval" : 1.3579000000000001069366817318950779736042022705078125,
    "androidUriScheme" : "aeiou",
    "defaultRadioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
    "batchConfig" : {
      "apiKey" : "aeiou",
      "androidApiKey" : "aeiou",
      "iOSApiKey" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "featureCompetitions" : true,
    "sponsorSplashImgDisplayTime" : 1.3579000000000001069366817318950779736042022705078125,
    "featureCommunity" : true,
    "iOSPushNotificationPrivateKey" : "aeiou",
    "color" : "aeiou",
    "featureTritonPlayer" : true,
    "featureOffersCategorized" : true,
    "featureEventsAttendeeList" : true,
    "featurePlayer" : true,
    "iOSPushNotificationPassword" : "aeiou",
    "rssWebViewCss" : "aeiou",
    "featureCommunityCategorized" : true,
    "androidGoogleApiKey" : "aeiou",
    "colors" : {
      "textSecondary" : "aeiou",
      "radioPlayerItem" : "aeiou",
      "radioPlayer" : "aeiou",
      "appSecondary" : "aeiou",
      "menuBackground" : "aeiou",
      "menuItem" : "aeiou",
      "appGradientPrimary" : "aeiou",
      "radioTextPrimary" : "aeiou",
      "radioPlayButtonBackground" : "aeiou",
      "radioProgressBar" : "aeiou",
      "radioTextSecondary" : "aeiou",
      "radioPlayButton" : "aeiou",
      "radioNavBar" : "aeiou",
      "feedSegmentIndicator" : "aeiou",
      "radioPauseButton" : "aeiou",
      "textPrimary" : "aeiou",
      "appPrimary" : "aeiou",
      "radioPauseButtonBackground" : "aeiou"
    },
    "iOSAds" : [ "" ],
    "branchConfig" : {
      "branchTestUrl" : "aeiou",
      "branchTestKey" : "aeiou",
      "branchLiveUrl" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "branchLiveKey" : "aeiou"
    },
    "imgSponsorSplash" : "aeiou",
    "androidKeystoreConfig" : {
      "storePassword" : "aeiou",
      "hashKey" : "aeiou",
      "keyAlias" : "aeiou",
      "keyPassword" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "androidAds" : [ "" ],
    "wordpressShareUrlStub" : "aeiou",
    "featureBrandFollowerList" : true,
    "iOSAppId" : "aeiou",
    "iOSPushNotificationCertificate" : "aeiou",
    "androidGoogleCloudMessagingSenderId" : "aeiou",
    "androidKeystore" : "aeiou",
    "playerConfig" : {
      "displayDescription" : true,
      "controls" : true,
      "hlshtml" : true,
      "playerCaptionConfig" : "{}",
      "skinName" : "aeiou",
      "mute" : true,
      "flashPlayer" : "aeiou",
      "autostart" : true,
      "stretching" : "aeiou",
      "playerAdConfig" : "{}",
      "visualPlaylist" : true,
      "preload" : "aeiou",
      "baseSkinPath" : "aeiou",
      "displayTitle" : true,
      "skinInactive" : "aeiou",
      "skinActive" : "aeiou",
      "repeat" : true,
      "skinBackground" : "aeiou",
      "skinCss" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "primary" : "aeiou"
    },
    "googleAnalyticsTracking" : {
      "mobileTrackingId" : "aeiou",
      "webTrackingId" : "aeiou"
    },
    "audioSponsorPreRollAd" : "aeiou",
    "pushWooshConfig" : {
      "appName" : "aeiou",
      "appToken" : "aeiou",
      "appId" : "aeiou"
    },
    "iOSVersion" : "aeiou",
    "iOSAppleId" : "aeiou",
    "androidKey" : "aeiou",
    "imgSponsorMenu" : "aeiou",
    "featurePlayerMainButton" : true,
    "featureWordpressConnect" : true,
    "featureNews" : true,
    "menu" : [ {
      "image" : "aeiou",
      "pageActionIcon" : "aeiou",
      "visibility" : true,
      "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
      "pageAction" : true,
      "styleCss" : "aeiou",
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "featureRadioDontShowOnCamera" : true,
      "url" : "aeiou",
      "pageActionLink" : "aeiou"
    } ],
    "featureFacebookLogin" : true,
    "featureBrandFollow" : true,
    "androidBuild" : "aeiou",
    "featureRadio" : true,
    "androidVersion" : "aeiou",
    "loginExternal" : {
      "authorizeUrl" : "aeiou",
      "apiKey" : "aeiou",
      "accessUrl" : "aeiou",
      "requestUrl" : "aeiou",
      "version" : "aeiou"
    },
    "customerDetailsForm" : [ {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "title" : "aeiou",
      "rows" : [ {
        "metadataKey" : "aeiou",
        "hidden" : true,
        "defaultValue" : "aeiou",
        "validationMessage" : "aeiou",
        "selectorOptions" : [ {
          "format" : "aeiou",
          "id" : 1.3579000000000001069366817318950779736042022705078125,
          "title" : "aeiou",
          "value" : "aeiou"
        } ],
        "placeholder" : "aeiou",
        "type" : "aeiou",
        "title" : "aeiou",
        "validationRegExp" : "aeiou",
        "required" : true
      } ]
    } ],
    "connectTo" : {
      "heading" : "aeiou",
      "link" : "aeiou",
      "description" : "aeiou",
      "token" : "aeiou"
    },
    "featureHarpoonLogin" : true,
    "iOSPushNotificationPem" : "aeiou"
  },
  "brandFollowOffer" : true,
  "appId" : "aeiou",
  "modified" : "2000-01-23T04:56:07.000+00:00",
  "clientSecret" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : "aeiou",
  "typeNewsFeed" : "aeiou",
  "rssFeedGroups" : [ "{}" ],
  "customers" : [ "{}" ],
  "bundle" : "aeiou",
  "storeAndroidUrl" : "aeiou",
  "restrictionAge" : 1.3579000000000001069366817318950779736042022705078125,
  "apps" : [ "{}" ],
  "appConfigs" : [ "{}" ],
  "termsOfServiceUrl" : "aeiou",
  "created" : "2000-01-23T04:56:07.000+00:00",
  "clientToken" : "aeiou",
  "playlists" : [ "{}" ],
  "styleOfferList" : "aeiou",
  "parentId" : "aeiou",
  "multiConfig" : [ "" ],
  "grantTypes" : [ "aeiou" ],
  "name" : "aeiou",
  "multiConfigTitle" : "aeiou",
  "realm" : "aeiou",
  "scopes" : [ "aeiou" ],
  "category" : "aeiou",
  "status" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.app.verify = function(args, res, next) {
  /**
   * parameters expected in the args:
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

