'use strict';

var url = require('url');


var RssFeedGroup = require('./RssFeedGroupService');


module.exports.rssFeedGroup.count = function rssFeedGroup.count (req, res, next) {
  RssFeedGroup.rssFeedGroup.count(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.create = function rssFeedGroup.create (req, res, next) {
  RssFeedGroup.rssFeedGroup.create(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.createChangeStream__get_RssFeedGroups_changeStream = function rssFeedGroup.createChangeStream__get_RssFeedGroups_changeStream (req, res, next) {
  RssFeedGroup.rssFeedGroup.createChangeStream__get_RssFeedGroups_changeStream(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.createChangeStream__post_RssFeedGroups_changeStream = function rssFeedGroup.createChangeStream__post_RssFeedGroups_changeStream (req, res, next) {
  RssFeedGroup.rssFeedGroup.createChangeStream__post_RssFeedGroups_changeStream(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.deleteById = function rssFeedGroup.deleteById (req, res, next) {
  RssFeedGroup.rssFeedGroup.deleteById(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.exists__get_RssFeedGroups_{id}_exists = function rssFeedGroup.exists__get_RssFeedGroups_{id}_exists (req, res, next) {
  RssFeedGroup.rssFeedGroup.exists__get_RssFeedGroups_{id}_exists(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.exists__head_RssFeedGroups_{id} = function rssFeedGroup.exists__head_RssFeedGroups_{id} (req, res, next) {
  RssFeedGroup.rssFeedGroup.exists__head_RssFeedGroups_{id}(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.find = function rssFeedGroup.find (req, res, next) {
  RssFeedGroup.rssFeedGroup.find(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.findById = function rssFeedGroup.findById (req, res, next) {
  RssFeedGroup.rssFeedGroup.findById(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.findOne = function rssFeedGroup.findOne (req, res, next) {
  RssFeedGroup.rssFeedGroup.findOne(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.__count__rssFeeds = function rssFeedGroup.prototype.__count__rssFeeds (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.__count__rssFeeds(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.__create__rssFeeds = function rssFeedGroup.prototype.__create__rssFeeds (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.__create__rssFeeds(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.__delete__rssFeeds = function rssFeedGroup.prototype.__delete__rssFeeds (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.__delete__rssFeeds(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.__destroyById__rssFeeds = function rssFeedGroup.prototype.__destroyById__rssFeeds (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.__destroyById__rssFeeds(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.__findById__rssFeeds = function rssFeedGroup.prototype.__findById__rssFeeds (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.__findById__rssFeeds(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.__get__rssFeeds = function rssFeedGroup.prototype.__get__rssFeeds (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.__get__rssFeeds(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.__updateById__rssFeeds = function rssFeedGroup.prototype.__updateById__rssFeeds (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.__updateById__rssFeeds(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.updateAttributes__patch_RssFeedGroups_{id} = function rssFeedGroup.prototype.updateAttributes__patch_RssFeedGroups_{id} (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.updateAttributes__patch_RssFeedGroups_{id}(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.prototype.updateAttributes__put_RssFeedGroups_{id} = function rssFeedGroup.prototype.updateAttributes__put_RssFeedGroups_{id} (req, res, next) {
  RssFeedGroup.rssFeedGroup.prototype.updateAttributes__put_RssFeedGroups_{id}(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.replaceById = function rssFeedGroup.replaceById (req, res, next) {
  RssFeedGroup.rssFeedGroup.replaceById(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.replaceOrCreate = function rssFeedGroup.replaceOrCreate (req, res, next) {
  RssFeedGroup.rssFeedGroup.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.updateAll = function rssFeedGroup.updateAll (req, res, next) {
  RssFeedGroup.rssFeedGroup.updateAll(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.upsertWithWhere = function rssFeedGroup.upsertWithWhere (req, res, next) {
  RssFeedGroup.rssFeedGroup.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.upsert__patch_RssFeedGroups = function rssFeedGroup.upsert__patch_RssFeedGroups (req, res, next) {
  RssFeedGroup.rssFeedGroup.upsert__patch_RssFeedGroups(req.swagger.params, res, next);
};

module.exports.rssFeedGroup.upsert__put_RssFeedGroups = function rssFeedGroup.upsert__put_RssFeedGroups (req, res, next) {
  RssFeedGroup.rssFeedGroup.upsert__put_RssFeedGroups(req.swagger.params, res, next);
};
