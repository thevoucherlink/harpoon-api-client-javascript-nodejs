'use strict';

var url = require('url');


var StripeSubscription = require('./StripeSubscriptionService');


module.exports.stripeSubscription.count = function stripeSubscription.count (req, res, next) {
  StripeSubscription.stripeSubscription.count(req.swagger.params, res, next);
};

module.exports.stripeSubscription.create = function stripeSubscription.create (req, res, next) {
  StripeSubscription.stripeSubscription.create(req.swagger.params, res, next);
};

module.exports.stripeSubscription.createChangeStream__get_StripeSubscriptions_changeStream = function stripeSubscription.createChangeStream__get_StripeSubscriptions_changeStream (req, res, next) {
  StripeSubscription.stripeSubscription.createChangeStream__get_StripeSubscriptions_changeStream(req.swagger.params, res, next);
};

module.exports.stripeSubscription.createChangeStream__post_StripeSubscriptions_changeStream = function stripeSubscription.createChangeStream__post_StripeSubscriptions_changeStream (req, res, next) {
  StripeSubscription.stripeSubscription.createChangeStream__post_StripeSubscriptions_changeStream(req.swagger.params, res, next);
};

module.exports.stripeSubscription.deleteById = function stripeSubscription.deleteById (req, res, next) {
  StripeSubscription.stripeSubscription.deleteById(req.swagger.params, res, next);
};

module.exports.stripeSubscription.exists__get_StripeSubscriptions_{id}_exists = function stripeSubscription.exists__get_StripeSubscriptions_{id}_exists (req, res, next) {
  StripeSubscription.stripeSubscription.exists__get_StripeSubscriptions_{id}_exists(req.swagger.params, res, next);
};

module.exports.stripeSubscription.exists__head_StripeSubscriptions_{id} = function stripeSubscription.exists__head_StripeSubscriptions_{id} (req, res, next) {
  StripeSubscription.stripeSubscription.exists__head_StripeSubscriptions_{id}(req.swagger.params, res, next);
};

module.exports.stripeSubscription.find = function stripeSubscription.find (req, res, next) {
  StripeSubscription.stripeSubscription.find(req.swagger.params, res, next);
};

module.exports.stripeSubscription.findById = function stripeSubscription.findById (req, res, next) {
  StripeSubscription.stripeSubscription.findById(req.swagger.params, res, next);
};

module.exports.stripeSubscription.findOne = function stripeSubscription.findOne (req, res, next) {
  StripeSubscription.stripeSubscription.findOne(req.swagger.params, res, next);
};

module.exports.stripeSubscription.prototype.updateAttributes__patch_StripeSubscriptions_{id} = function stripeSubscription.prototype.updateAttributes__patch_StripeSubscriptions_{id} (req, res, next) {
  StripeSubscription.stripeSubscription.prototype.updateAttributes__patch_StripeSubscriptions_{id}(req.swagger.params, res, next);
};

module.exports.stripeSubscription.prototype.updateAttributes__put_StripeSubscriptions_{id} = function stripeSubscription.prototype.updateAttributes__put_StripeSubscriptions_{id} (req, res, next) {
  StripeSubscription.stripeSubscription.prototype.updateAttributes__put_StripeSubscriptions_{id}(req.swagger.params, res, next);
};

module.exports.stripeSubscription.replaceById = function stripeSubscription.replaceById (req, res, next) {
  StripeSubscription.stripeSubscription.replaceById(req.swagger.params, res, next);
};

module.exports.stripeSubscription.replaceOrCreate = function stripeSubscription.replaceOrCreate (req, res, next) {
  StripeSubscription.stripeSubscription.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.stripeSubscription.updateAll = function stripeSubscription.updateAll (req, res, next) {
  StripeSubscription.stripeSubscription.updateAll(req.swagger.params, res, next);
};

module.exports.stripeSubscription.upsertWithWhere = function stripeSubscription.upsertWithWhere (req, res, next) {
  StripeSubscription.stripeSubscription.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.stripeSubscription.upsert__patch_StripeSubscriptions = function stripeSubscription.upsert__patch_StripeSubscriptions (req, res, next) {
  StripeSubscription.stripeSubscription.upsert__patch_StripeSubscriptions(req.swagger.params, res, next);
};

module.exports.stripeSubscription.upsert__put_StripeSubscriptions = function stripeSubscription.upsert__put_StripeSubscriptions (req, res, next) {
  StripeSubscription.stripeSubscription.upsert__put_StripeSubscriptions(req.swagger.params, res, next);
};
