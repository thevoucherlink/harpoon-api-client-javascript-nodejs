'use strict';

exports.playerSource.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.createChangeStream__get_PlayerSources_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.createChangeStream__post_PlayerSources_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.exists__get_PlayerSources_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.exists__head_PlayerSources_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.prototype.__get__playlistItem = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.prototype.updateAttributes__patch_PlayerSources_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.prototype.updateAttributes__put_PlayerSources_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.upsert__patch_PlayerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.playerSource.upsert__put_PlayerSources = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (PlayerSource)
  **/
    var examples = {};
  examples['application/json'] = {
  "default" : true,
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlistItem" : "{}",
  "label" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "type" : "aeiou",
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

