'use strict';

exports.harpoonHpublicApplicationpartner.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.createChangeStream__get_HarpoonHpublicApplicationpartners_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.createChangeStream__post_HarpoonHpublicApplicationpartners_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.exists__get_HarpoonHpublicApplicationpartners_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.exists__head_HarpoonHpublicApplicationpartners_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.prototype.__get__udropshipVendor = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "billingRegionId" : 1.3579000000000001069366817318950779736042022705078125,
  "useHandlingFee" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberAllowMicrosite" : 1.3579000000000001069366817318950779736042022705078125,
  "billingUseShipping" : 1.3579000000000001069366817318950779736042022705078125,
  "labelType" : "aeiou",
  "udprodTemplateSku" : "aeiou",
  "tiershipUseV2Rates" : 1.3579000000000001069366817318950779736042022705078125,
  "passwordHash" : "aeiou",
  "vendorAttn" : "aeiou",
  "billingVendorAttn" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "password" : "aeiou",
  "vacationMessage" : "aeiou",
  "rejectReason" : "aeiou",
  "passwordEnc" : "aeiou",
  "defaultShippingId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "billingFax" : "aeiou",
  "fax" : "aeiou",
  "billingZip" : "aeiou",
  "zip" : "aeiou",
  "emailTemplate" : 1.3579000000000001069366817318950779736042022705078125,
  "backorderByAvailability" : 1.3579000000000001069366817318950779736042022705078125,
  "telephone" : "aeiou",
  "confirmation" : "aeiou",
  "vendorName" : "aeiou",
  "tiercomRates" : "aeiou",
  "tiercomFixedRates" : "aeiou",
  "harpoonHpublicv12VendorAppCategories" : [ "{}" ],
  "randomHash" : "aeiou",
  "defaultShippingExtraCharge" : "aeiou",
  "harpoonHpublicApplicationpartners" : [ "{}" ],
  "tiercomFixedCalcType" : "aeiou",
  "tiershipRates" : "aeiou",
  "defaultShippingExtraChargeSuffix" : "aeiou",
  "billingTelephone" : "aeiou",
  "updateStoreBaseUrl" : 1.3579000000000001069366817318950779736042022705078125,
  "carrierCode" : "aeiou",
  "customDataCombined" : "aeiou",
  "udmemberMembershipCode" : "aeiou",
  "region" : "aeiou",
  "status" : true,
  "tiershipSimpleRates" : "aeiou",
  "vendorLocations" : [ "{}" ],
  "udropshipVendorProducts" : [ "{}" ],
  "handlingFee" : "aeiou",
  "city" : "aeiou",
  "allowShippingExtraCharge" : 1.3579000000000001069366817318950779736042022705078125,
  "vendorTaxClass" : "aeiou",
  "subdomainLevel" : 1.3579000000000001069366817318950779736042022705078125,
  "isExtraChargeShippingDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "countryId" : "aeiou",
  "billingRegion" : "aeiou",
  "vacationMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingEmail" : "aeiou",
  "udmemberLimitProducts" : 1.3579000000000001069366817318950779736042022705078125,
  "udmemberProfileRefid" : "aeiou",
  "street" : "aeiou",
  "udmemberProfileSyncOff" : 1.3579000000000001069366817318950779736042022705078125,
  "upsShipperNumber" : "aeiou",
  "defaultShippingExtraChargeType" : "aeiou",
  "useReservedQty" : 1.3579000000000001069366817318950779736042022705078125,
  "useRatesFallback" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou",
  "tiercomFixedRule" : "aeiou",
  "notifyNewOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "notifyLowstock" : 1.3579000000000001069366817318950779736042022705078125,
  "confirmationSent" : 1.3579000000000001069366817318950779736042022705078125,
  "vacationEnd" : "2000-01-23T04:56:07.000+00:00",
  "udmemberBillingType" : "aeiou",
  "catalogProducts" : [ "{}" ],
  "notifyLowstockQty" : "aeiou",
  "udmemberHistory" : "aeiou",
  "urlKey" : "aeiou",
  "udmemberProfileId" : 1.3579000000000001069366817318950779736042022705078125,
  "regionId" : 1.3579000000000001069366817318950779736042022705078125,
  "billingStreet" : "aeiou",
  "partners" : [ "{}" ],
  "customVarsCombined" : "aeiou",
  "udmemberMembershipTitle" : "aeiou",
  "testMode" : 1.3579000000000001069366817318950779736042022705078125,
  "billingCountryId" : "aeiou",
  "config" : "{}",
  "billingCity" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.prototype.updateAttributes__patch_HarpoonHpublicApplicationpartners_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.prototype.updateAttributes__put_HarpoonHpublicApplicationpartners_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.upsert__patch_HarpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.harpoonHpublicApplicationpartner.upsert__put_HarpoonHpublicApplicationpartners = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (HarpoonHpublicApplicationpartner)
  **/
    var examples = {};
  examples['application/json'] = {
  "configAcceptNotificationpush" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configFeed" : 1.3579000000000001069366817318950779736042022705078125,
  "configList" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealsimple" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveDealgroup" : 1.3579000000000001069366817318950779736042022705078125,
  "invitedEmail" : "aeiou",
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "configAcceptNotificationbeacon" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveEvent" : 1.3579000000000001069366817318950779736042022705078125,
  "udropshipVendor" : "{}",
  "isOwner" : 1.3579000000000001069366817318950779736042022705078125,
  "brandId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "configAcceptCoupon" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationId" : 1.3579000000000001069366817318950779736042022705078125,
  "status" : "aeiou",
  "invitedAt" : "2000-01-23T04:56:07.000+00:00",
  "acceptedAt" : "2000-01-23T04:56:07.000+00:00",
  "updatedAt" : "2000-01-23T04:56:07.000+00:00",
  "configNeedFollow" : 1.3579000000000001069366817318950779736042022705078125,
  "configApproveCoupon" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

