'use strict';

var url = require('url');


var StripeCoupon = require('./StripeCouponService');


module.exports.stripeCoupon.count = function stripeCoupon.count (req, res, next) {
  StripeCoupon.stripeCoupon.count(req.swagger.params, res, next);
};

module.exports.stripeCoupon.create = function stripeCoupon.create (req, res, next) {
  StripeCoupon.stripeCoupon.create(req.swagger.params, res, next);
};

module.exports.stripeCoupon.createChangeStream__get_StripeCoupons_changeStream = function stripeCoupon.createChangeStream__get_StripeCoupons_changeStream (req, res, next) {
  StripeCoupon.stripeCoupon.createChangeStream__get_StripeCoupons_changeStream(req.swagger.params, res, next);
};

module.exports.stripeCoupon.createChangeStream__post_StripeCoupons_changeStream = function stripeCoupon.createChangeStream__post_StripeCoupons_changeStream (req, res, next) {
  StripeCoupon.stripeCoupon.createChangeStream__post_StripeCoupons_changeStream(req.swagger.params, res, next);
};

module.exports.stripeCoupon.deleteById = function stripeCoupon.deleteById (req, res, next) {
  StripeCoupon.stripeCoupon.deleteById(req.swagger.params, res, next);
};

module.exports.stripeCoupon.exists__get_StripeCoupons_{id}_exists = function stripeCoupon.exists__get_StripeCoupons_{id}_exists (req, res, next) {
  StripeCoupon.stripeCoupon.exists__get_StripeCoupons_{id}_exists(req.swagger.params, res, next);
};

module.exports.stripeCoupon.exists__head_StripeCoupons_{id} = function stripeCoupon.exists__head_StripeCoupons_{id} (req, res, next) {
  StripeCoupon.stripeCoupon.exists__head_StripeCoupons_{id}(req.swagger.params, res, next);
};

module.exports.stripeCoupon.find = function stripeCoupon.find (req, res, next) {
  StripeCoupon.stripeCoupon.find(req.swagger.params, res, next);
};

module.exports.stripeCoupon.findById = function stripeCoupon.findById (req, res, next) {
  StripeCoupon.stripeCoupon.findById(req.swagger.params, res, next);
};

module.exports.stripeCoupon.findOne = function stripeCoupon.findOne (req, res, next) {
  StripeCoupon.stripeCoupon.findOne(req.swagger.params, res, next);
};

module.exports.stripeCoupon.prototype.updateAttributes__patch_StripeCoupons_{id} = function stripeCoupon.prototype.updateAttributes__patch_StripeCoupons_{id} (req, res, next) {
  StripeCoupon.stripeCoupon.prototype.updateAttributes__patch_StripeCoupons_{id}(req.swagger.params, res, next);
};

module.exports.stripeCoupon.prototype.updateAttributes__put_StripeCoupons_{id} = function stripeCoupon.prototype.updateAttributes__put_StripeCoupons_{id} (req, res, next) {
  StripeCoupon.stripeCoupon.prototype.updateAttributes__put_StripeCoupons_{id}(req.swagger.params, res, next);
};

module.exports.stripeCoupon.replaceById = function stripeCoupon.replaceById (req, res, next) {
  StripeCoupon.stripeCoupon.replaceById(req.swagger.params, res, next);
};

module.exports.stripeCoupon.replaceOrCreate = function stripeCoupon.replaceOrCreate (req, res, next) {
  StripeCoupon.stripeCoupon.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.stripeCoupon.updateAll = function stripeCoupon.updateAll (req, res, next) {
  StripeCoupon.stripeCoupon.updateAll(req.swagger.params, res, next);
};

module.exports.stripeCoupon.upsertWithWhere = function stripeCoupon.upsertWithWhere (req, res, next) {
  StripeCoupon.stripeCoupon.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.stripeCoupon.upsert__patch_StripeCoupons = function stripeCoupon.upsert__patch_StripeCoupons (req, res, next) {
  StripeCoupon.stripeCoupon.upsert__patch_StripeCoupons(req.swagger.params, res, next);
};

module.exports.stripeCoupon.upsert__put_StripeCoupons = function stripeCoupon.upsert__put_StripeCoupons (req, res, next) {
  StripeCoupon.stripeCoupon.upsert__put_StripeCoupons(req.swagger.params, res, next);
};
