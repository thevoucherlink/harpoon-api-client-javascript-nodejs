'use strict';

exports.stripeCoupon.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.createChangeStream__get_StripeCoupons_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.createChangeStream__post_StripeCoupons_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.exists__get_StripeCoupons_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.exists__head_StripeCoupons_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.prototype.updateAttributes__patch_StripeCoupons_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.prototype.updateAttributes__put_StripeCoupons_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.upsert__patch_StripeCoupons = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeCoupon.upsert__put_StripeCoupons = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeCoupon)
  **/
    var examples = {};
  examples['application/json'] = {
  "metadata" : "aeiou",
  "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
  "livemode" : true,
  "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
  "created" : 1.3579000000000001069366817318950779736042022705078125,
  "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
  "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "valid" : true,
  "duration" : "aeiou",
  "currency" : "aeiou",
  "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "object" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

