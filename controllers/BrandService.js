'use strict';

exports.brand.allFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "cover" : "aeiou",
  "privacyCode" : "aeiou",
  "related" : {
    "dealPaid" : 1.3579000000000001069366817318950779736042022705078125,
    "eventId" : 1.3579000000000001069366817318950779736042022705078125,
    "competitionId" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "brandId" : 1.3579000000000001069366817318950779736042022705078125,
    "customerId" : 1.3579000000000001069366817318950779736042022705078125,
    "couponId" : 1.3579000000000001069366817318950779736042022705078125
  },
  "postedAt" : "2000-01-23T04:56:07.000+00:00",
  "link" : "aeiou",
  "actionCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "message" : "aeiou",
  "brand" : {
    "website" : "aeiou",
    "address" : "aeiou",
    "description" : "aeiou",
    "managerName" : "aeiou",
    "isFollowed" : true,
    "cover" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "logo" : "aeiou",
    "categories" : [ {
      "parent" : "",
      "children" : [ "" ],
      "hasChildren" : true,
      "isPrimary" : true,
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "productCount" : 1.3579000000000001069366817318950779736042022705078125
    } ],
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  }
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.competitions = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "competitionQuestion" : "aeiou",
  "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "earnMoreChances" : true,
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "hasJoined" : true,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "earnMoreChancesURL" : "aeiou",
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "competitionAnswer" : "aeiou",
  "isWinner" : true,
  "chances" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "chanceCount" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.coupons = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "price" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "hasClaimed" : true,
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyClaimed" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.dealGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "price" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "hasClaimed" : true,
  "product" : {
    "cover" : "aeiou",
    "base_currency" : "aeiou",
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "description" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyClaimed" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.dealPaids = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "price" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "hasClaimed" : true,
  "product" : {
    "cover" : "aeiou",
    "base_currency" : "aeiou",
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "description" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyClaimed" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.deals = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "type" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "price" : 1.3579000000000001069366817318950779736042022705078125,
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "hasClaimed" : true,
  "actionText" : "aeiou",
  "campaignType" : "",
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyClaimed" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.events = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "tickets" : [ {
    "qtyLeft" : 1.3579000000000001069366817318950779736042022705078125,
    "qtyBought" : 1.3579000000000001069366817318950779736042022705078125,
    "price" : 1.3579000000000001069366817318950779736042022705078125,
    "name" : "aeiou",
    "qtyTotal" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "connectFacebookId" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "attendeeCount" : 1.3579000000000001069366817318950779736042022705078125,
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "basePrice" : 1.3579000000000001069366817318950779736042022705078125,
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "attendees" : [ {
    "lastName" : "aeiou",
    "profilePictureUpload" : {
      "file" : "aeiou",
      "contentType" : "aeiou"
    },
    "coverUpload" : "",
    "metadata" : "{}",
    "gender" : "aeiou",
    "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
    "authorizationCode" : "aeiou",
    "joinedAt" : "2000-01-23T04:56:07.000+00:00",
    "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollowed" : true,
    "cover" : "aeiou",
    "firstName" : "aeiou",
    "profilePicture" : "aeiou",
    "password" : "aeiou",
    "dob" : "aeiou",
    "connection" : {
      "twitter" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "facebook" : {
        "id" : 1.3579000000000001069366817318950779736042022705078125,
        "userId" : "aeiou"
      },
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "isFollower" : true,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  } ],
  "facebook" : {
    "noreplyCount" : 1.3579000000000001069366817318950779736042022705078125,
    "node" : "aeiou",
    "updatedTime" : "aeiou",
    "maybeCount" : 1.3579000000000001069366817318950779736042022705078125,
    "attendingCount" : 1.3579000000000001069366817318950779736042022705078125,
    "place" : "{}",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "category" : "aeiou"
  },
  "isGoing" : true,
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.feeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "cover" : "aeiou",
  "privacyCode" : "aeiou",
  "related" : {
    "dealPaid" : 1.3579000000000001069366817318950779736042022705078125,
    "eventId" : 1.3579000000000001069366817318950779736042022705078125,
    "competitionId" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "brandId" : 1.3579000000000001069366817318950779736042022705078125,
    "customerId" : 1.3579000000000001069366817318950779736042022705078125,
    "couponId" : 1.3579000000000001069366817318950779736042022705078125
  },
  "postedAt" : "2000-01-23T04:56:07.000+00:00",
  "link" : "aeiou",
  "actionCode" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "message" : "aeiou",
  "brand" : {
    "website" : "aeiou",
    "address" : "aeiou",
    "description" : "aeiou",
    "managerName" : "aeiou",
    "isFollowed" : true,
    "cover" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "logo" : "aeiou",
    "categories" : [ {
      "parent" : "",
      "children" : [ "" ],
      "hasChildren" : true,
      "isPrimary" : true,
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "productCount" : 1.3579000000000001069366817318950779736042022705078125
    } ],
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  }
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "website" : "aeiou",
  "address" : "aeiou",
  "description" : "aeiou",
  "managerName" : "aeiou",
  "isFollowed" : true,
  "cover" : "aeiou",
  "phone" : "aeiou",
  "name" : "aeiou",
  "logo" : "aeiou",
  "categories" : [ {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "website" : "aeiou",
  "address" : "aeiou",
  "description" : "aeiou",
  "managerName" : "aeiou",
  "isFollowed" : true,
  "cover" : "aeiou",
  "phone" : "aeiou",
  "name" : "aeiou",
  "logo" : "aeiou",
  "categories" : [ {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "website" : "aeiou",
  "address" : "aeiou",
  "description" : "aeiou",
  "managerName" : "aeiou",
  "isFollowed" : true,
  "cover" : "aeiou",
  "phone" : "aeiou",
  "name" : "aeiou",
  "logo" : "aeiou",
  "categories" : [ {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.followers = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "password" : "aeiou",
  "dob" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.followersCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "lastName" : "aeiou",
  "profilePictureUpload" : {
    "file" : "aeiou",
    "contentType" : "aeiou"
  },
  "coverUpload" : "",
  "metadata" : "{}",
  "gender" : "aeiou",
  "notificationCount" : 1.3579000000000001069366817318950779736042022705078125,
  "authorizationCode" : "aeiou",
  "privacy" : {
    "activity" : "aeiou",
    "notificationBeacon" : true,
    "notificationPush" : true,
    "notificationLocation" : true
  },
  "followingCount" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollowed" : true,
  "cover" : "aeiou",
  "firstName" : "aeiou",
  "profilePicture" : "aeiou",
  "badge" : {
    "walletOfferExpiring" : 1.3579000000000001069366817318950779736042022705078125,
    "all" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferAvailable" : 1.3579000000000001069366817318950779736042022705078125,
    "dealAll" : 1.3579000000000001069366817318950779736042022705078125,
    "dealGroup" : 1.3579000000000001069366817318950779736042022705078125,
    "notificationUnread" : 1.3579000000000001069366817318950779736042022705078125,
    "brandFeed" : 1.3579000000000001069366817318950779736042022705078125,
    "competition" : 1.3579000000000001069366817318950779736042022705078125,
    "brandActivity" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferNew" : 1.3579000000000001069366817318950779736042022705078125,
    "offer" : 1.3579000000000001069366817318950779736042022705078125,
    "walletOfferExpired" : 1.3579000000000001069366817318950779736042022705078125,
    "dealCoupon" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "event" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : 1.3579000000000001069366817318950779736042022705078125,
    "dealSimple" : 1.3579000000000001069366817318950779736042022705078125
  },
  "password" : "aeiou",
  "dob" : "aeiou",
  "appId" : "aeiou",
  "connection" : {
    "twitter" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "facebook" : {
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "userId" : "aeiou"
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFollower" : true,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.followersDelete = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.offers = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "collectionNotes" : "aeiou",
  "description" : "aeiou",
  "shareLink" : "aeiou",
  "checkoutLink" : "aeiou",
  "nearestVenue" : {
    "country" : "aeiou",
    "address" : "aeiou",
    "city" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "coordinates" : {
      "latitude" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "longitude" : 1.3579000000000001069366817318950779736042022705078125
    },
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "brand" : {
      "website" : "aeiou",
      "address" : "aeiou",
      "description" : "aeiou",
      "managerName" : "aeiou",
      "isFollowed" : true,
      "cover" : "aeiou",
      "phone" : "aeiou",
      "name" : "aeiou",
      "logo" : "aeiou",
      "categories" : [ "" ],
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
      "email" : "aeiou"
    },
    "email" : "aeiou"
  },
  "locationLink" : "aeiou",
  "altLink" : "aeiou",
  "baseCurrency" : "aeiou",
  "cover" : "aeiou",
  "alias" : "aeiou",
  "from" : "2000-01-23T04:56:07.000+00:00",
  "qtyPerOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : true,
  "priceText" : "aeiou",
  "brand" : "",
  "actionText" : "aeiou",
  "campaignType" : {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  },
  "termsConditions" : "aeiou",
  "redemptionType" : "aeiou",
  "name" : "aeiou",
  "bannerText" : "aeiou",
  "topic" : "",
  "to" : "2000-01-23T04:56:07.000+00:00",
  "category" : "",
  "closestPurchase" : {
    "expiredAt" : "2000-01-23T04:56:07.000+00:00",
    "purchasedAt" : "2000-01-23T04:56:07.000+00:00",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "status" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Brand)
  **/
    var examples = {};
  examples['application/json'] = {
  "website" : "aeiou",
  "address" : "aeiou",
  "description" : "aeiou",
  "managerName" : "aeiou",
  "isFollowed" : true,
  "cover" : "aeiou",
  "phone" : "aeiou",
  "name" : "aeiou",
  "logo" : "aeiou",
  "categories" : [ {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Brand)
  **/
    var examples = {};
  examples['application/json'] = {
  "website" : "aeiou",
  "address" : "aeiou",
  "description" : "aeiou",
  "managerName" : "aeiou",
  "isFollowed" : true,
  "cover" : "aeiou",
  "phone" : "aeiou",
  "name" : "aeiou",
  "logo" : "aeiou",
  "categories" : [ {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (Brand)
  **/
    var examples = {};
  examples['application/json'] = {
  "website" : "aeiou",
  "address" : "aeiou",
  "description" : "aeiou",
  "managerName" : "aeiou",
  "isFollowed" : true,
  "cover" : "aeiou",
  "phone" : "aeiou",
  "name" : "aeiou",
  "logo" : "aeiou",
  "categories" : [ {
    "parent" : "",
    "children" : [ "" ],
    "hasChildren" : true,
    "isPrimary" : true,
    "name" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "productCount" : 1.3579000000000001069366817318950779736042022705078125
  } ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
  "email" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.brand.venues = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "country" : "aeiou",
  "address" : "aeiou",
  "city" : "aeiou",
  "phone" : "aeiou",
  "name" : "aeiou",
  "coordinates" : {
    "latitude" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "longitude" : 1.3579000000000001069366817318950779736042022705078125
  },
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "brand" : {
    "website" : "aeiou",
    "address" : "aeiou",
    "description" : "aeiou",
    "managerName" : "aeiou",
    "isFollowed" : true,
    "cover" : "aeiou",
    "phone" : "aeiou",
    "name" : "aeiou",
    "logo" : "aeiou",
    "categories" : [ {
      "parent" : "",
      "children" : [ "" ],
      "hasChildren" : true,
      "isPrimary" : true,
      "name" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "productCount" : 1.3579000000000001069366817318950779736042022705078125
    } ],
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "followerCount" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou"
  },
  "email" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

