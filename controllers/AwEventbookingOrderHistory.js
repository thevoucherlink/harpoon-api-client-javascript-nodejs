'use strict';

var url = require('url');


var AwEventbookingOrderHistory = require('./AwEventbookingOrderHistoryService');


module.exports.awEventbookingOrderHistory.count = function awEventbookingOrderHistory.count (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.count(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.create = function awEventbookingOrderHistory.create (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.create(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.createChangeStream__get_AwEventbookingOrderHistories_changeStream = function awEventbookingOrderHistory.createChangeStream__get_AwEventbookingOrderHistories_changeStream (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.createChangeStream__get_AwEventbookingOrderHistories_changeStream(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.createChangeStream__post_AwEventbookingOrderHistories_changeStream = function awEventbookingOrderHistory.createChangeStream__post_AwEventbookingOrderHistories_changeStream (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.createChangeStream__post_AwEventbookingOrderHistories_changeStream(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.deleteById = function awEventbookingOrderHistory.deleteById (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.deleteById(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.exists__get_AwEventbookingOrderHistories_{id}_exists = function awEventbookingOrderHistory.exists__get_AwEventbookingOrderHistories_{id}_exists (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.exists__get_AwEventbookingOrderHistories_{id}_exists(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.exists__head_AwEventbookingOrderHistories_{id} = function awEventbookingOrderHistory.exists__head_AwEventbookingOrderHistories_{id} (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.exists__head_AwEventbookingOrderHistories_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.find = function awEventbookingOrderHistory.find (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.find(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.findById = function awEventbookingOrderHistory.findById (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.findById(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.findOne = function awEventbookingOrderHistory.findOne (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.findOne(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.prototype.updateAttributes__patch_AwEventbookingOrderHistories_{id} = function awEventbookingOrderHistory.prototype.updateAttributes__patch_AwEventbookingOrderHistories_{id} (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.prototype.updateAttributes__patch_AwEventbookingOrderHistories_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.prototype.updateAttributes__put_AwEventbookingOrderHistories_{id} = function awEventbookingOrderHistory.prototype.updateAttributes__put_AwEventbookingOrderHistories_{id} (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.prototype.updateAttributes__put_AwEventbookingOrderHistories_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.replaceById = function awEventbookingOrderHistory.replaceById (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.replaceById(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.replaceOrCreate = function awEventbookingOrderHistory.replaceOrCreate (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.updateAll = function awEventbookingOrderHistory.updateAll (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.updateAll(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.upsertWithWhere = function awEventbookingOrderHistory.upsertWithWhere (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.upsert__patch_AwEventbookingOrderHistories = function awEventbookingOrderHistory.upsert__patch_AwEventbookingOrderHistories (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.upsert__patch_AwEventbookingOrderHistories(req.swagger.params, res, next);
};

module.exports.awEventbookingOrderHistory.upsert__put_AwEventbookingOrderHistories = function awEventbookingOrderHistory.upsert__put_AwEventbookingOrderHistories (req, res, next) {
  AwEventbookingOrderHistory.awEventbookingOrderHistory.upsert__put_AwEventbookingOrderHistories(req.swagger.params, res, next);
};
