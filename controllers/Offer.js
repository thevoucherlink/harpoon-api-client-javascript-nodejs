'use strict';

var url = require('url');


var Offer = require('./OfferService');


module.exports.offer.find = function offer.find (req, res, next) {
  Offer.offer.find(req.swagger.params, res, next);
};

module.exports.offer.findById = function offer.findById (req, res, next) {
  Offer.offer.findById(req.swagger.params, res, next);
};

module.exports.offer.replaceById = function offer.replaceById (req, res, next) {
  Offer.offer.replaceById(req.swagger.params, res, next);
};

module.exports.offer.replaceOrCreate = function offer.replaceOrCreate (req, res, next) {
  Offer.offer.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.offer.upsertWithWhere = function offer.upsertWithWhere (req, res, next) {
  Offer.offer.upsertWithWhere(req.swagger.params, res, next);
};
