'use strict';

var url = require('url');


var RssFeed = require('./RssFeedService');


module.exports.rssFeed.count = function rssFeed.count (req, res, next) {
  RssFeed.rssFeed.count(req.swagger.params, res, next);
};

module.exports.rssFeed.create = function rssFeed.create (req, res, next) {
  RssFeed.rssFeed.create(req.swagger.params, res, next);
};

module.exports.rssFeed.createChangeStream__get_RssFeeds_changeStream = function rssFeed.createChangeStream__get_RssFeeds_changeStream (req, res, next) {
  RssFeed.rssFeed.createChangeStream__get_RssFeeds_changeStream(req.swagger.params, res, next);
};

module.exports.rssFeed.createChangeStream__post_RssFeeds_changeStream = function rssFeed.createChangeStream__post_RssFeeds_changeStream (req, res, next) {
  RssFeed.rssFeed.createChangeStream__post_RssFeeds_changeStream(req.swagger.params, res, next);
};

module.exports.rssFeed.deleteById = function rssFeed.deleteById (req, res, next) {
  RssFeed.rssFeed.deleteById(req.swagger.params, res, next);
};

module.exports.rssFeed.exists__get_RssFeeds_{id}_exists = function rssFeed.exists__get_RssFeeds_{id}_exists (req, res, next) {
  RssFeed.rssFeed.exists__get_RssFeeds_{id}_exists(req.swagger.params, res, next);
};

module.exports.rssFeed.exists__head_RssFeeds_{id} = function rssFeed.exists__head_RssFeeds_{id} (req, res, next) {
  RssFeed.rssFeed.exists__head_RssFeeds_{id}(req.swagger.params, res, next);
};

module.exports.rssFeed.find = function rssFeed.find (req, res, next) {
  RssFeed.rssFeed.find(req.swagger.params, res, next);
};

module.exports.rssFeed.findById = function rssFeed.findById (req, res, next) {
  RssFeed.rssFeed.findById(req.swagger.params, res, next);
};

module.exports.rssFeed.findOne = function rssFeed.findOne (req, res, next) {
  RssFeed.rssFeed.findOne(req.swagger.params, res, next);
};

module.exports.rssFeed.prototype.__get__rssFeedGroup = function rssFeed.prototype.__get__rssFeedGroup (req, res, next) {
  RssFeed.rssFeed.prototype.__get__rssFeedGroup(req.swagger.params, res, next);
};

module.exports.rssFeed.prototype.updateAttributes__patch_RssFeeds_{id} = function rssFeed.prototype.updateAttributes__patch_RssFeeds_{id} (req, res, next) {
  RssFeed.rssFeed.prototype.updateAttributes__patch_RssFeeds_{id}(req.swagger.params, res, next);
};

module.exports.rssFeed.prototype.updateAttributes__put_RssFeeds_{id} = function rssFeed.prototype.updateAttributes__put_RssFeeds_{id} (req, res, next) {
  RssFeed.rssFeed.prototype.updateAttributes__put_RssFeeds_{id}(req.swagger.params, res, next);
};

module.exports.rssFeed.replaceById = function rssFeed.replaceById (req, res, next) {
  RssFeed.rssFeed.replaceById(req.swagger.params, res, next);
};

module.exports.rssFeed.replaceOrCreate = function rssFeed.replaceOrCreate (req, res, next) {
  RssFeed.rssFeed.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.rssFeed.updateAll = function rssFeed.updateAll (req, res, next) {
  RssFeed.rssFeed.updateAll(req.swagger.params, res, next);
};

module.exports.rssFeed.upsertWithWhere = function rssFeed.upsertWithWhere (req, res, next) {
  RssFeed.rssFeed.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.rssFeed.upsert__patch_RssFeeds = function rssFeed.upsert__patch_RssFeeds (req, res, next) {
  RssFeed.rssFeed.upsert__patch_RssFeeds(req.swagger.params, res, next);
};

module.exports.rssFeed.upsert__put_RssFeeds = function rssFeed.upsert__put_RssFeeds (req, res, next) {
  RssFeed.rssFeed.upsert__put_RssFeeds(req.swagger.params, res, next);
};
