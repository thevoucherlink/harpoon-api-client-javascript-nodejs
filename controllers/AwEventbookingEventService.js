'use strict';

exports.awEventbookingEvent.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.createChangeStream__get_AwEventbookingEvents_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.createChangeStream__post_AwEventbookingEvents_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.exists__get_AwEventbookingEvents_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.exists__head_AwEventbookingEvents_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__count__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__count__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__count__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__create__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEventAttribute)
  **/
    var examples = {};
  examples['application/json'] = {
  "attributeCode" : "aeiou",
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "value" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__create__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__create__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__delete__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEvent.prototype.__delete__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEvent.prototype.__delete__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEvent.prototype.__destroyById__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEvent.prototype.__destroyById__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEvent.prototype.__destroyById__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEvent.prototype.__exists__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = true;
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__findById__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "attributeCode" : "aeiou",
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "value" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__findById__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__findById__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__get__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "attributeCode" : "aeiou",
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "value" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__get__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__get__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__link__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__unlink__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awEventbookingEvent.prototype.__updateById__attributes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AwEventbookingEventAttribute)
  **/
    var examples = {};
  examples['application/json'] = {
  "attributeCode" : "aeiou",
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "storeId" : 1.3579000000000001069366817318950779736042022705078125,
  "value" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__updateById__purchasedTickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AwEventbookingTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "createdAt" : "2000-01-23T04:56:07.000+00:00",
  "eventTicketId" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "redeemed" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "fee" : "aeiou",
  "redeemedByTerminal" : "aeiou",
  "redeemedAt" : "2000-01-23T04:56:07.000+00:00",
  "competitionwinnerId" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "paymentStatus" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.__updateById__tickets = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AwEventbookingEventTicket)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventId" : 1.3579000000000001069366817318950779736042022705078125,
  "chance" : 1.3579000000000001069366817318950779736042022705078125,
  "tickets" : [ "{}" ],
  "awEventbookingTicket" : [ "{}" ],
  "fee" : "aeiou",
  "priceType" : "aeiou",
  "codeprefix" : "aeiou",
  "price" : "aeiou",
  "qty" : 1.3579000000000001069366817318950779736042022705078125,
  "isPassOnFee" : 1.3579000000000001069366817318950779736042022705078125,
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "sku" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.updateAttributes__patch_AwEventbookingEvents_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.prototype.updateAttributes__put_AwEventbookingEvents_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.upsert__patch_AwEventbookingEvents = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awEventbookingEvent.upsert__put_AwEventbookingEvents = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwEventbookingEvent)
  **/
    var examples = {};
  examples['application/json'] = {
  "eventEndDate" : "2000-01-23T04:56:07.000+00:00",
  "tickets" : [ "{}" ],
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "isReminderSend" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasedTickets" : [ "{}" ],
  "generatePdfTickets" : 1.3579000000000001069366817318950779736042022705078125,
  "redeemRoles" : "aeiou",
  "dayCountBeforeSendReminderLetter" : 1.3579000000000001069366817318950779736042022705078125,
  "isEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "location" : "aeiou",
  "attributes" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "isTermsEnabled" : 1.3579000000000001069366817318950779736042022705078125,
  "eventStartDate" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

