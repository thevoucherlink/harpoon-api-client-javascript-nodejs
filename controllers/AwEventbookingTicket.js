'use strict';

var url = require('url');


var AwEventbookingTicket = require('./AwEventbookingTicketService');


module.exports.awEventbookingTicket.count = function awEventbookingTicket.count (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.count(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.create = function awEventbookingTicket.create (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.create(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.createChangeStream__get_AwEventbookingTickets_changeStream = function awEventbookingTicket.createChangeStream__get_AwEventbookingTickets_changeStream (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.createChangeStream__get_AwEventbookingTickets_changeStream(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.createChangeStream__post_AwEventbookingTickets_changeStream = function awEventbookingTicket.createChangeStream__post_AwEventbookingTickets_changeStream (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.createChangeStream__post_AwEventbookingTickets_changeStream(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.deleteById = function awEventbookingTicket.deleteById (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.deleteById(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.exists__get_AwEventbookingTickets_{id}_exists = function awEventbookingTicket.exists__get_AwEventbookingTickets_{id}_exists (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.exists__get_AwEventbookingTickets_{id}_exists(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.exists__head_AwEventbookingTickets_{id} = function awEventbookingTicket.exists__head_AwEventbookingTickets_{id} (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.exists__head_AwEventbookingTickets_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.find = function awEventbookingTicket.find (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.find(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.findById = function awEventbookingTicket.findById (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.findById(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.findOne = function awEventbookingTicket.findOne (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.findOne(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.prototype.updateAttributes__patch_AwEventbookingTickets_{id} = function awEventbookingTicket.prototype.updateAttributes__patch_AwEventbookingTickets_{id} (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.prototype.updateAttributes__patch_AwEventbookingTickets_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.prototype.updateAttributes__put_AwEventbookingTickets_{id} = function awEventbookingTicket.prototype.updateAttributes__put_AwEventbookingTickets_{id} (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.prototype.updateAttributes__put_AwEventbookingTickets_{id}(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.replaceById = function awEventbookingTicket.replaceById (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.replaceById(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.replaceOrCreate = function awEventbookingTicket.replaceOrCreate (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.updateAll = function awEventbookingTicket.updateAll (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.updateAll(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.upsertWithWhere = function awEventbookingTicket.upsertWithWhere (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.upsert__patch_AwEventbookingTickets = function awEventbookingTicket.upsert__patch_AwEventbookingTickets (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.upsert__patch_AwEventbookingTickets(req.swagger.params, res, next);
};

module.exports.awEventbookingTicket.upsert__put_AwEventbookingTickets = function awEventbookingTicket.upsert__put_AwEventbookingTickets (req, res, next) {
  AwEventbookingTicket.awEventbookingTicket.upsert__put_AwEventbookingTickets(req.swagger.params, res, next);
};
