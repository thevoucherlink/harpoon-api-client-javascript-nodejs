'use strict';

var url = require('url');


var Category = require('./CategoryService');


module.exports.category.findBrandCategories = function category.findBrandCategories (req, res, next) {
  Category.category.findBrandCategories(req.swagger.params, res, next);
};

module.exports.category.findCampaignTypeCategories = function category.findCampaignTypeCategories (req, res, next) {
  Category.category.findCampaignTypeCategories(req.swagger.params, res, next);
};

module.exports.category.findOfferCategories = function category.findOfferCategories (req, res, next) {
  Category.category.findOfferCategories(req.swagger.params, res, next);
};

module.exports.category.replaceById = function category.replaceById (req, res, next) {
  Category.category.replaceById(req.swagger.params, res, next);
};

module.exports.category.replaceOrCreate = function category.replaceOrCreate (req, res, next) {
  Category.category.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.category.upsertWithWhere = function category.upsertWithWhere (req, res, next) {
  Category.category.upsertWithWhere(req.swagger.params, res, next);
};
