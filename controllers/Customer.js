'use strict';

var url = require('url');


var Customer = require('./CustomerService');


module.exports.customer.activities = function customer.activities (req, res, next) {
  Customer.customer.activities(req.swagger.params, res, next);
};

module.exports.customer.badge = function customer.badge (req, res, next) {
  Customer.customer.badge(req.swagger.params, res, next);
};

module.exports.customer.cardCreate = function customer.cardCreate (req, res, next) {
  Customer.customer.cardCreate(req.swagger.params, res, next);
};

module.exports.customer.cardDelete = function customer.cardDelete (req, res, next) {
  Customer.customer.cardDelete(req.swagger.params, res, next);
};

module.exports.customer.cardFindById = function customer.cardFindById (req, res, next) {
  Customer.customer.cardFindById(req.swagger.params, res, next);
};

module.exports.customer.cardUpdate = function customer.cardUpdate (req, res, next) {
  Customer.customer.cardUpdate(req.swagger.params, res, next);
};

module.exports.customer.cards = function customer.cards (req, res, next) {
  Customer.customer.cards(req.swagger.params, res, next);
};

module.exports.customer.competitionOrders = function customer.competitionOrders (req, res, next) {
  Customer.customer.competitionOrders(req.swagger.params, res, next);
};

module.exports.customer.couponOrders = function customer.couponOrders (req, res, next) {
  Customer.customer.couponOrders(req.swagger.params, res, next);
};

module.exports.customer.create = function customer.create (req, res, next) {
  Customer.customer.create(req.swagger.params, res, next);
};

module.exports.customer.dealGroupOrders = function customer.dealGroupOrders (req, res, next) {
  Customer.customer.dealGroupOrders(req.swagger.params, res, next);
};

module.exports.customer.dealPaidOrders = function customer.dealPaidOrders (req, res, next) {
  Customer.customer.dealPaidOrders(req.swagger.params, res, next);
};

module.exports.customer.eventAttending = function customer.eventAttending (req, res, next) {
  Customer.customer.eventAttending(req.swagger.params, res, next);
};

module.exports.customer.eventOrders = function customer.eventOrders (req, res, next) {
  Customer.customer.eventOrders(req.swagger.params, res, next);
};

module.exports.customer.find = function customer.find (req, res, next) {
  Customer.customer.find(req.swagger.params, res, next);
};

module.exports.customer.findById = function customer.findById (req, res, next) {
  Customer.customer.findById(req.swagger.params, res, next);
};

module.exports.customer.findOne = function customer.findOne (req, res, next) {
  Customer.customer.findOne(req.swagger.params, res, next);
};

module.exports.customer.follow = function customer.follow (req, res, next) {
  Customer.customer.follow(req.swagger.params, res, next);
};

module.exports.customer.followers = function customer.followers (req, res, next) {
  Customer.customer.followers(req.swagger.params, res, next);
};

module.exports.customer.followings = function customer.followings (req, res, next) {
  Customer.customer.followings(req.swagger.params, res, next);
};

module.exports.customer.login = function customer.login (req, res, next) {
  Customer.customer.login(req.swagger.params, res, next);
};

module.exports.customer.notifications = function customer.notifications (req, res, next) {
  Customer.customer.notifications(req.swagger.params, res, next);
};

module.exports.customer.passwordResetAlt = function customer.passwordResetAlt (req, res, next) {
  Customer.customer.passwordResetAlt(req.swagger.params, res, next);
};

module.exports.customer.passwordUpdate = function customer.passwordUpdate (req, res, next) {
  Customer.customer.passwordUpdate(req.swagger.params, res, next);
};

module.exports.customer.refreshToken = function customer.refreshToken (req, res, next) {
  Customer.customer.refreshToken(req.swagger.params, res, next);
};

module.exports.customer.replaceById = function customer.replaceById (req, res, next) {
  Customer.customer.replaceById(req.swagger.params, res, next);
};

module.exports.customer.replaceOrCreate = function customer.replaceOrCreate (req, res, next) {
  Customer.customer.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.customer.tokenFromAuthorizationCode = function customer.tokenFromAuthorizationCode (req, res, next) {
  Customer.customer.tokenFromAuthorizationCode(req.swagger.params, res, next);
};

module.exports.customer.unfollow = function customer.unfollow (req, res, next) {
  Customer.customer.unfollow(req.swagger.params, res, next);
};

module.exports.customer.update = function customer.update (req, res, next) {
  Customer.customer.update(req.swagger.params, res, next);
};

module.exports.customer.upsertWithWhere = function customer.upsertWithWhere (req, res, next) {
  Customer.customer.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.customer.verify = function customer.verify (req, res, next) {
  Customer.customer.verify(req.swagger.params, res, next);
};

module.exports.customer.walletCompetition = function customer.walletCompetition (req, res, next) {
  Customer.customer.walletCompetition(req.swagger.params, res, next);
};

module.exports.customer.walletCoupon = function customer.walletCoupon (req, res, next) {
  Customer.customer.walletCoupon(req.swagger.params, res, next);
};

module.exports.customer.walletDeal = function customer.walletDeal (req, res, next) {
  Customer.customer.walletDeal(req.swagger.params, res, next);
};

module.exports.customer.walletDealGroup = function customer.walletDealGroup (req, res, next) {
  Customer.customer.walletDealGroup(req.swagger.params, res, next);
};

module.exports.customer.walletDealPaid = function customer.walletDealPaid (req, res, next) {
  Customer.customer.walletDealPaid(req.swagger.params, res, next);
};

module.exports.customer.walletEvent = function customer.walletEvent (req, res, next) {
  Customer.customer.walletEvent(req.swagger.params, res, next);
};

module.exports.customer.walletOffer = function customer.walletOffer (req, res, next) {
  Customer.customer.walletOffer(req.swagger.params, res, next);
};
