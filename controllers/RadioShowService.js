'use strict';

exports.radioShow.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.createChangeStream__get_RadioShows_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.createChangeStream__post_RadioShows_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.exists__get_RadioShows_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.exists__head_RadioShows_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__count__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__count__radioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__create__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioPresenter)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "name" : "aeiou",
  "radioShows" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__create__radioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__delete__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.radioShow.prototype.__delete__radioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.radioShow.prototype.__destroyById__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.radioShow.prototype.__destroyById__radioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.radioShow.prototype.__exists__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = true;
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__findById__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "name" : "aeiou",
  "radioShows" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__findById__radioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__get__playlistItem = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "playlistId" : 1.3579000000000001069366817318950779736042022705078125,
  "image" : "aeiou",
  "playerTracks" : [ "{}" ],
  "radioShows" : [ "{}" ],
  "mediaType" : "aeiou",
  "playerSources" : [ "{}" ],
  "shortDescription" : "aeiou",
  "title" : "aeiou",
  "type" : "aeiou",
  "mediaId" : 1.3579000000000001069366817318950779736042022705078125,
  "file" : "aeiou",
  "playlist" : "{}",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "order" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__get__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "image" : "aeiou",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "name" : "aeiou",
  "radioShows" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__get__radioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__get__radioStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "sponsorTrack" : "aeiou",
  "tritonConfig" : {
    "aacMount" : "aeiou",
    "stationName" : "aeiou",
    "mp3Mount" : "aeiou",
    "stationId" : "aeiou"
  },
  "description" : "aeiou",
  "radioShows" : [ "{}" ],
  "metaDataUrl" : "aeiou",
  "imgStream" : "aeiou",
  "urlLQ" : "aeiou",
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "urlHQ" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__link__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RadioPresenterRadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenterId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "radioPresenter" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__unlink__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.radioShow.prototype.__updateById__radioPresenters = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RadioPresenter)
  **/
    var examples = {};
  examples['application/json'] = {
  "image" : "aeiou",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "name" : "aeiou",
  "radioShows" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.__updateById__radioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.updateAttributes__patch_RadioShows_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.prototype.updateAttributes__put_RadioShows_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.uploadFile = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (MagentoFileUpload)
  **/
    var examples = {};
  examples['application/json'] = {
  "file" : "aeiou",
  "name" : "aeiou",
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.upsert__patch_RadioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShow.upsert__put_RadioShows = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioShow)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

