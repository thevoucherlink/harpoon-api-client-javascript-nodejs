'use strict';

exports.awCollpurDeal.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.createChangeStream__get_AwCollpurDeals_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.createChangeStream__post_AwCollpurDeals_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.exists__get_AwCollpurDeals_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.exists__head_AwCollpurDeals_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.prototype.__count__awCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.prototype.__create__awCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.prototype.__delete__awCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awCollpurDeal.prototype.__destroyById__awCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.awCollpurDeal.prototype.__findById__awCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.prototype.__get__awCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.prototype.__updateById__awCollpurDealPurchases = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (AwCollpurDealPurchases)
  **/
    var examples = {};
  examples['application/json'] = {
  "refundState" : 1.3579000000000001069366817318950779736042022705078125,
  "orderId" : 1.3579000000000001069366817318950779736042022705078125,
  "dealId" : 1.3579000000000001069366817318950779736042022705078125,
  "orderItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyPurchased" : 1.3579000000000001069366817318950779736042022705078125,
  "customerName" : "aeiou",
  "qtyWithCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "purchaseDateTime" : "2000-01-23T04:56:07.000+00:00",
  "qtyOrdered" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "shippingAmount" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "awCollpurCoupon" : "{}"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.prototype.updateAttributes__patch_AwCollpurDeals_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.prototype.updateAttributes__put_AwCollpurDeals_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.upsert__patch_AwCollpurDeals = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.awCollpurDeal.upsert__put_AwCollpurDeals = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (AwCollpurDeal)
  **/
    var examples = {};
  examples['application/json'] = {
  "maximumAllowedPurchases" : 1.3579000000000001069366817318950779736042022705078125,
  "expiredFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "qtyToReachDeal" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "isActive" : 1.3579000000000001069366817318950779736042022705078125,
  "closeState" : 1.3579000000000001069366817318950779736042022705078125,
  "fullDescription" : "aeiou",
  "availableFrom" : "2000-01-23T04:56:07.000+00:00",
  "productName" : "aeiou",
  "storeIds" : "aeiou",
  "price" : "aeiou",
  "isSuccessedFlag" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "enableCoupons" : 1.3579000000000001069366817318950779736042022705078125,
  "isFeatured" : 1.3579000000000001069366817318950779736042022705078125,
  "dealImage" : "aeiou",
  "isSuccess" : 1.3579000000000001069366817318950779736042022705078125,
  "productId" : 1.3579000000000001069366817318950779736042022705078125,
  "purchasesLeft" : 1.3579000000000001069366817318950779736042022705078125,
  "availableTo" : "2000-01-23T04:56:07.000+00:00",
  "couponExpireAfterDays" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "awCollpurDealPurchases" : [ "{}" ],
  "autoClose" : 1.3579000000000001069366817318950779736042022705078125,
  "couponPrefix" : "aeiou",
  "sentBeforeFlag" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

