'use strict';

var url = require('url');


var RadioPresenter = require('./RadioPresenterService');


module.exports.radioPresenter.count = function radioPresenter.count (req, res, next) {
  RadioPresenter.radioPresenter.count(req.swagger.params, res, next);
};

module.exports.radioPresenter.create = function radioPresenter.create (req, res, next) {
  RadioPresenter.radioPresenter.create(req.swagger.params, res, next);
};

module.exports.radioPresenter.createChangeStream__get_RadioPresenters_changeStream = function radioPresenter.createChangeStream__get_RadioPresenters_changeStream (req, res, next) {
  RadioPresenter.radioPresenter.createChangeStream__get_RadioPresenters_changeStream(req.swagger.params, res, next);
};

module.exports.radioPresenter.createChangeStream__post_RadioPresenters_changeStream = function radioPresenter.createChangeStream__post_RadioPresenters_changeStream (req, res, next) {
  RadioPresenter.radioPresenter.createChangeStream__post_RadioPresenters_changeStream(req.swagger.params, res, next);
};

module.exports.radioPresenter.deleteById = function radioPresenter.deleteById (req, res, next) {
  RadioPresenter.radioPresenter.deleteById(req.swagger.params, res, next);
};

module.exports.radioPresenter.exists__get_RadioPresenters_{id}_exists = function radioPresenter.exists__get_RadioPresenters_{id}_exists (req, res, next) {
  RadioPresenter.radioPresenter.exists__get_RadioPresenters_{id}_exists(req.swagger.params, res, next);
};

module.exports.radioPresenter.exists__head_RadioPresenters_{id} = function radioPresenter.exists__head_RadioPresenters_{id} (req, res, next) {
  RadioPresenter.radioPresenter.exists__head_RadioPresenters_{id}(req.swagger.params, res, next);
};

module.exports.radioPresenter.find = function radioPresenter.find (req, res, next) {
  RadioPresenter.radioPresenter.find(req.swagger.params, res, next);
};

module.exports.radioPresenter.findById = function radioPresenter.findById (req, res, next) {
  RadioPresenter.radioPresenter.findById(req.swagger.params, res, next);
};

module.exports.radioPresenter.findOne = function radioPresenter.findOne (req, res, next) {
  RadioPresenter.radioPresenter.findOne(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__count__radioShows = function radioPresenter.prototype.__count__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__count__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__create__radioShows = function radioPresenter.prototype.__create__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__create__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__delete__radioShows = function radioPresenter.prototype.__delete__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__delete__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__destroyById__radioShows = function radioPresenter.prototype.__destroyById__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__destroyById__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__exists__radioShows = function radioPresenter.prototype.__exists__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__exists__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__findById__radioShows = function radioPresenter.prototype.__findById__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__findById__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__get__radioShows = function radioPresenter.prototype.__get__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__get__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__link__radioShows = function radioPresenter.prototype.__link__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__link__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__unlink__radioShows = function radioPresenter.prototype.__unlink__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__unlink__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.__updateById__radioShows = function radioPresenter.prototype.__updateById__radioShows (req, res, next) {
  RadioPresenter.radioPresenter.prototype.__updateById__radioShows(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.updateAttributes__patch_RadioPresenters_{id} = function radioPresenter.prototype.updateAttributes__patch_RadioPresenters_{id} (req, res, next) {
  RadioPresenter.radioPresenter.prototype.updateAttributes__patch_RadioPresenters_{id}(req.swagger.params, res, next);
};

module.exports.radioPresenter.prototype.updateAttributes__put_RadioPresenters_{id} = function radioPresenter.prototype.updateAttributes__put_RadioPresenters_{id} (req, res, next) {
  RadioPresenter.radioPresenter.prototype.updateAttributes__put_RadioPresenters_{id}(req.swagger.params, res, next);
};

module.exports.radioPresenter.replaceById = function radioPresenter.replaceById (req, res, next) {
  RadioPresenter.radioPresenter.replaceById(req.swagger.params, res, next);
};

module.exports.radioPresenter.replaceOrCreate = function radioPresenter.replaceOrCreate (req, res, next) {
  RadioPresenter.radioPresenter.replaceOrCreate(req.swagger.params, res, next);
};

module.exports.radioPresenter.updateAll = function radioPresenter.updateAll (req, res, next) {
  RadioPresenter.radioPresenter.updateAll(req.swagger.params, res, next);
};

module.exports.radioPresenter.uploadImage = function radioPresenter.uploadImage (req, res, next) {
  RadioPresenter.radioPresenter.uploadImage(req.swagger.params, res, next);
};

module.exports.radioPresenter.upsertWithWhere = function radioPresenter.upsertWithWhere (req, res, next) {
  RadioPresenter.radioPresenter.upsertWithWhere(req.swagger.params, res, next);
};

module.exports.radioPresenter.upsert__patch_RadioPresenters = function radioPresenter.upsert__patch_RadioPresenters (req, res, next) {
  RadioPresenter.radioPresenter.upsert__patch_RadioPresenters(req.swagger.params, res, next);
};

module.exports.radioPresenter.upsert__put_RadioPresenters = function radioPresenter.upsert__put_RadioPresenters (req, res, next) {
  RadioPresenter.radioPresenter.upsert__put_RadioPresenters(req.swagger.params, res, next);
};
