'use strict';

exports.rssFeedGroup.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.createChangeStream__get_RssFeedGroups_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.createChangeStream__post_RssFeedGroups_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.exists__get_RssFeedGroups_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.exists__head_RssFeedGroups_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.prototype.__count__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.prototype.__create__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.prototype.__delete__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.rssFeedGroup.prototype.__destroyById__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
  // no response value expected for this operation
  res.end();
}

exports.rssFeedGroup.prototype.__findById__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.prototype.__get__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.prototype.__updateById__rssFeeds = function(args, res, next) {
  /**
   * parameters expected in the args:
  * fk (String)
  * id (String)
  * data (RssFeed)
  **/
    var examples = {};
  examples['application/json'] = {
  "subCategory" : "aeiou",
  "styleCss" : "aeiou",
  "url" : "aeiou",
  "noAds" : true,
  "adMRectVisible" : true,
  "rssFeedGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "adMRectPosition" : 1.3579000000000001069366817318950779736042022705078125,
  "rssFeedGroup" : "{}",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "category" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.prototype.updateAttributes__patch_RssFeedGroups_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.prototype.updateAttributes__put_RssFeedGroups_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.upsert__patch_RssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.rssFeedGroup.upsert__put_RssFeedGroups = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RssFeedGroup)
  **/
    var examples = {};
  examples['application/json'] = {
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "appId" : "aeiou",
  "name" : "aeiou",
  "rssFeeds" : [ "{}" ],
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

