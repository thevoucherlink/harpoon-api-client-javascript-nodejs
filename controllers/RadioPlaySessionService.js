'use strict';

exports.radioPlaySession.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.createChangeStream__get_RadioPlaySessions_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.createChangeStream__post_RadioPlaySessions_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.exists__get_RadioPlaySessions_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.exists__head_RadioPlaySessions_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.prototype.updateAttributes__patch_RadioPlaySessions_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.prototype.updateAttributes__put_RadioPlaySessions_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.upsert__patch_RadioPlaySessions = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioPlaySession.upsert__put_RadioPlaySessions = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioPlaySession)
  **/
    var examples = {};
  examples['application/json'] = {
  "playStartTime" : "2000-01-23T04:56:07.000+00:00",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "playEndTime" : "2000-01-23T04:56:07.000+00:00",
  "customerId" : 1.3579000000000001069366817318950779736042022705078125,
  "sessionTime" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "playUpdateTime" : "2000-01-23T04:56:07.000+00:00"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

