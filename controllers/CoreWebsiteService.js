'use strict';

exports.coreWebsite.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.createChangeStream__get_CoreWebsites_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.createChangeStream__post_CoreWebsites_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.exists__get_CoreWebsites_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.exists__head_CoreWebsites_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.prototype.updateAttributes__patch_CoreWebsites_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.prototype.updateAttributes__put_CoreWebsites_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.upsert__patch_CoreWebsites = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.coreWebsite.upsert__put_CoreWebsites = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (CoreWebsite)
  **/
    var examples = {};
  examples['application/json'] = {
  "isDefault" : 1.3579000000000001069366817318950779736042022705078125,
  "code" : "aeiou",
  "sortOrder" : 1.3579000000000001069366817318950779736042022705078125,
  "name" : "aeiou",
  "defaultGroupId" : 1.3579000000000001069366817318950779736042022705078125,
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

