'use strict';

exports.category.findBrandCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "parent" : "",
  "children" : [ "" ],
  "hasChildren" : true,
  "isPrimary" : true,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "productCount" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.category.findCampaignTypeCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "parent" : "",
  "children" : [ "" ],
  "hasChildren" : true,
  "isPrimary" : true,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "productCount" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.category.findOfferCategories = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "parent" : "",
  "children" : [ "" ],
  "hasChildren" : true,
  "isPrimary" : true,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "productCount" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.category.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (Category)
  **/
    var examples = {};
  examples['application/json'] = {
  "parent" : "",
  "children" : [ "" ],
  "hasChildren" : true,
  "isPrimary" : true,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "productCount" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.category.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (Category)
  **/
    var examples = {};
  examples['application/json'] = {
  "parent" : "",
  "children" : [ "" ],
  "hasChildren" : true,
  "isPrimary" : true,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "productCount" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.category.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (Category)
  **/
    var examples = {};
  examples['application/json'] = {
  "parent" : "",
  "children" : [ "" ],
  "hasChildren" : true,
  "isPrimary" : true,
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "productCount" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

