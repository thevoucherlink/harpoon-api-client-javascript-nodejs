'use strict';

exports.radioShowTime.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.createChangeStream__get_RadioShowTimes_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.createChangeStream__post_RadioShowTimes_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.exists__get_RadioShowTimes_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.exists__head_RadioShowTimes_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.prototype.__get__radioShow = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * refresh (Boolean)
  **/
    var examples = {};
  examples['application/json'] = {
  "imgShow" : "aeiou",
  "sponsorTrack" : "aeiou",
  "playlistItemId" : 1.3579000000000001069366817318950779736042022705078125,
  "radioShowTimeCurrent" : {
    "radioShow" : "{}",
    "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
    "weekday" : 1.3579000000000001069366817318950779736042022705078125,
    "startTime" : "aeiou",
    "endTime" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125
  },
  "radioPresenters" : [ "{}" ],
  "description" : "aeiou",
  "radioStream" : "{}",
  "type" : "aeiou",
  "content" : "aeiou",
  "radioStreamId" : 1.3579000000000001069366817318950779736042022705078125,
  "playlistItem" : "{}",
  "contact" : {
    "whatsapp" : "aeiou",
    "website" : "aeiou",
    "address" : {
      "attnLastName" : "aeiou",
      "streetAddress" : "aeiou",
      "city" : "aeiou",
      "countryCode" : "aeiou",
      "streetAddressComp" : "aeiou",
      "postcode" : "aeiou",
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "region" : "aeiou",
      "attnFirstName" : "aeiou"
    },
    "facebook" : "aeiou",
    "linkedIn" : "aeiou",
    "googlePlus" : "aeiou",
    "snapchat" : "aeiou",
    "twitter" : "aeiou",
    "phone" : "aeiou",
    "text" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "email" : "aeiou",
    "hashtag" : "aeiou"
  },
  "ends" : "2000-01-23T04:56:07.000+00:00",
  "name" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "starts" : "2000-01-23T04:56:07.000+00:00",
  "radioShowTimes" : [ "{}" ],
  "contentType" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.prototype.updateAttributes__patch_RadioShowTimes_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.prototype.updateAttributes__put_RadioShowTimes_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.upsert__patch_RadioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.radioShowTime.upsert__put_RadioShowTimes = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (RadioShowTime)
  **/
    var examples = {};
  examples['application/json'] = {
  "radioShow" : "{}",
  "radioShowId" : 1.3579000000000001069366817318950779736042022705078125,
  "weekday" : 1.3579000000000001069366817318950779736042022705078125,
  "startTime" : "aeiou",
  "endTime" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

