'use strict';

exports.stripeInvoice.count = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.create = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.createChangeStream__get_StripeInvoices_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.createChangeStream__post_StripeInvoices_changeStream = function(args, res, next) {
  /**
   * parameters expected in the args:
  * options (String)
  **/
    var examples = {};
  examples['application/json'] = "";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.deleteById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = "{}";
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.exists__get_StripeInvoices_{id}_exists = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.exists__head_StripeInvoices_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "exists" : true
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.find = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = [ {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
} ];
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.findById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.findOne = function(args, res, next) {
  /**
   * parameters expected in the args:
  * filter (String)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.prototype.updateAttributes__patch_StripeInvoices_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.prototype.updateAttributes__put_StripeInvoices_{id} = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.replaceById = function(args, res, next) {
  /**
   * parameters expected in the args:
  * id (String)
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.replaceOrCreate = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.updateAll = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "count" : 1.3579000000000001069366817318950779736042022705078125
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.upsertWithWhere = function(args, res, next) {
  /**
   * parameters expected in the args:
  * where (String)
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.upsert__patch_StripeInvoices = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

exports.stripeInvoice.upsert__put_StripeInvoices = function(args, res, next) {
  /**
   * parameters expected in the args:
  * data (StripeInvoice)
  **/
    var examples = {};
  examples['application/json'] = {
  "date" : 1.3579000000000001069366817318950779736042022705078125,
  "metadata" : "aeiou",
  "livemode" : true,
  "endingBalance" : 1.3579000000000001069366817318950779736042022705078125,
  "description" : "aeiou",
  "discount" : {
    "coupon" : {
      "metadata" : "aeiou",
      "durationInMonths" : 1.3579000000000001069366817318950779736042022705078125,
      "livemode" : true,
      "maxRedemptions" : 1.3579000000000001069366817318950779736042022705078125,
      "created" : 1.3579000000000001069366817318950779736042022705078125,
      "percentOff" : 1.3579000000000001069366817318950779736042022705078125,
      "redeemBy" : 1.3579000000000001069366817318950779736042022705078125,
      "timesRedeemed" : 1.3579000000000001069366817318950779736042022705078125,
      "valid" : true,
      "duration" : "aeiou",
      "currency" : "aeiou",
      "amountOff" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125,
      "object" : "aeiou"
    },
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "end" : 1.3579000000000001069366817318950779736042022705078125,
    "subscription" : "aeiou",
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou",
    "customer" : "aeiou"
  },
  "subscription" : {
    "currentPeriodStart" : 1.3579000000000001069366817318950779736042022705078125,
    "trialEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "metadata" : "aeiou",
    "quantity" : 1.3579000000000001069366817318950779736042022705078125,
    "currentPeriodEnd" : 1.3579000000000001069366817318950779736042022705078125,
    "trialStart" : 1.3579000000000001069366817318950779736042022705078125,
    "start" : 1.3579000000000001069366817318950779736042022705078125,
    "applicationFeePercent" : 1.3579000000000001069366817318950779736042022705078125,
    "discount" : "",
    "canceledAt" : 1.3579000000000001069366817318950779736042022705078125,
    "endedAt" : 1.3579000000000001069366817318950779736042022705078125,
    "cancelAtPeriodEnd" : true,
    "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "plan" : {
      "statement_descriptor" : "aeiou",
      "amount" : "aeiou",
      "metadata" : "aeiou",
      "intervalCount" : 1.3579000000000001069366817318950779736042022705078125,
      "name" : "aeiou",
      "currency" : "aeiou",
      "interval" : "aeiou",
      "trial_period_days" : 1.3579000000000001069366817318950779736042022705078125,
      "id" : 1.3579000000000001069366817318950779736042022705078125
    },
    "status" : "aeiou",
    "customer" : "aeiou",
    "object" : "aeiou"
  },
  "webhooksDeliveredAt" : 1.3579000000000001069366817318950779736042022705078125,
  "applicationFee" : 1.3579000000000001069366817318950779736042022705078125,
  "total" : 1.3579000000000001069366817318950779736042022705078125,
  "nextPaymentAttempt" : 1.3579000000000001069366817318950779736042022705078125,
  "taxPercent" : 1.3579000000000001069366817318950779736042022705078125,
  "currency" : "aeiou",
  "id" : 1.3579000000000001069366817318950779736042022705078125,
  "lines" : [ {
    "id" : 1.3579000000000001069366817318950779736042022705078125,
    "object" : "aeiou"
  } ],
  "receiptNumber" : "aeiou",
  "periodEnd" : 1.3579000000000001069366817318950779736042022705078125,
  "charge" : "aeiou",
  "forgiven" : true,
  "attemptCount" : 1.3579000000000001069366817318950779736042022705078125,
  "tax" : 1.3579000000000001069366817318950779736042022705078125,
  "subscriptionProrationDate" : 1.3579000000000001069366817318950779736042022705078125,
  "amountDue" : 1.3579000000000001069366817318950779736042022705078125,
  "subtotal" : 1.3579000000000001069366817318950779736042022705078125,
  "paid" : true,
  "closed" : true,
  "attempted" : true,
  "periodStart" : 1.3579000000000001069366817318950779736042022705078125,
  "statmentDescriptor" : "aeiou",
  "object" : "aeiou",
  "customer" : "aeiou"
};
  if(Object.keys(examples).length > 0) {
    res.setHeader('Content-Type', 'application/json');
    res.end(JSON.stringify(examples[Object.keys(examples)[0]] || {}, null, 2));
  }
  else {
    res.end();
  }
  
}

